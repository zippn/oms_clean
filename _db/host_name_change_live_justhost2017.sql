UPDATE wp_options SET option_value = replace(option_value, 'http://3dartasia.com', 'http://onemorestep.ca') WHERE option_name = 'home' OR option_name = 'siteurl';

UPDATE wp_posts SET guid = replace(guid, 'http://3dartasia.com','http://onemorestep.ca');

UPDATE wp_posts SET post_content = replace(post_content, 'http://3dartasia.com', 'http://onemorestep.ca');

#UPDATE wp_options SET option_value = '' WHERE option_name = 'permalink_structure';


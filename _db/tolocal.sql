UPDATE d23oms2011_options SET option_value = replace(option_value, 'http://onemorestep.ca', 'http://localhost/oms') WHERE option_name = 'home' OR option_name = 'siteurl';
UPDATE d23oms2011_posts SET post_content = replace(post_content, 'http://onemorestep.ca', 'http://localhost/oms');
UPDATE d23oms2011_postmeta SET meta_value = replace(meta_value,'http://onemorestep.ca','http://localhost/oms');
UPDATE d23oms2011_posts SET guid = replace(guid, 'http://onemorestep.ca', 'http://localhost/oms');

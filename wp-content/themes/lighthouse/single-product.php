


<?php
/*
 * @package WordPress
 * @subpackage oms
 */?>

<?php get_template_part('header')?>

<div class="container oms-page-content">

    <div class="row">
        <div id="primary" class="col-md-9 content-area">
            <main id="main" class="site-main" role="main">
                <div class="row">
                    <div id="content-inner-wrap">
                        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); global $_product; $_product = new jigoshop_product( $post->ID ); ?>

                            <?php do_action('jigoshop_before_single_product', $post, $_product); ?>

                            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                                <?php do_action('jigoshop_before_single_product_summary', $post, $_product); ?>

                                <div class="summary">

                                    <?php do_action( 'jigoshop_template_single_summary', $post, $_product ); ?>

                                </div>

                                <?php do_action('jigoshop_after_single_product_summary', $post, $_product); ?>

                            </div>

                            <?php do_action('jigoshop_after_single_product', $post, $_product); ?>

                        <?php endwhile; ?>

                    </div><!--cs wrap-->

                </div>



            </main><!-- #main -->
        </div><!-- #primary -->

        <?php get_sidebar();?>
    </div><!--row-->


</div><!--.container-->
<?php get_footer(); ?>




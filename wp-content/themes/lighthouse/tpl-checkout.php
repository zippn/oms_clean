<?php
/*
 * Template Name: Checkout
 * @package WordPress
 * @subpackage citi
 */?>
<?php
/*if from  cheque payment then record the transaction*/
if (!empty($_POST['custom'])){
    process_cheque_payment($_POST['custom']);
    wp_redirect(get_permalink(187)); //redirect to thank you page
    //vl_redirect(get_post_permalink(187)); //redirect to thank you page
}

/**Perform actions*/
if ( !empty($_POST) && wp_verify_nonce($_POST['pr_nonce'],'clear') )
{
    empty_gift_bag();
}


?>
<?php get_template_part('header')?>
    <div class="container">
        <div class="row">
            <div id="primary" class="col-md-9 content-area">
                <main id="main" class="site-main" role="main">
                    <div class="row">

                    <div id="content-inner-wrap">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <h1><?php the_title(); ?></h1>
                            <?php if (count($_SESSION['basket']) > 0) {
                                get_template_part('basket-summary');
                            } else { ?>
                                <p class="regular_content"><?php _e("[:en]You gift bag is currently empty. Please select an item from our online catalogue above.[:vi]Giỏ quà của quý vị hiện đang trống. Xin vui lòng chọn một món quà."); ?></p>
                            <?php } ?>
                        <?php endwhile; endif;?>
                    </div><!--cs wrap-->
                    </div>

                </main><!-- #main -->
            </div><!-- #primary -->

            <?php get_sidebar();?>

        </div> <!--.row-->
    </div><!--.container-->

<script type="text/javascript">
    var $j = jQuery.noConflict();
    $j(document).ready(function($) {
        $("#radCheque").click(function(){
               $("#ByChequeInfo").show();
        });

        $("#btnClear").click(function(){
            $("#frmCancel").submit();
			/*$("#btnClear").removeGiftFromBasket({
				//divToChange:"#content-inner-wrap",
			});*/
        });
		$("#btnPurchase").click(function(){

            $("#btnPurchase").purchase();


		});
        $("#cbConsent").change(function(){
            if($("#ppPublishName").val()=="publish name")
            {   $("#ppPublishName").val("do not publish name");
                $("#cbPublishName").val("do not publish name");
            }else{
                $("#ppPublishName").val("publish name");
                $("#cbPublishName").val("publish name");

            }
        });
		
    });
</script>
 
<?php get_template_part('footer')?>
<div id="side_menu_wrapper" class="col-md-3">

    <!--<span class="top_left_corner_green"></span>-->
    <!--<span class="top_right_corner_green"></span>-->
    <div id="side_menu_wrapper_inner" >
        <!--<div class="row">
            <article id="gift-drive_panel" class="side-wraps side-panel">
                <header>
                    <h3><?php /*_e("[:en]Gift Drive[:vi]Quà Giáng Sinh");*/?></h3>
                    <a href="/current-gift-drive/" class="btn call-to-action">View Gifts</a>
                    <div class="side-panel-info-wrap">
                        <div class="side-add-info"><span class="side-label"><?php /*_e("[:en]Goal[:vi]Mục Tiêu");*/?></span> <span class="side-emphasized">$<?php /*echo GIFT_DRIVE_FUNDRAISING_TARGET;*/?></span></div>
                        <div class="side-add-info"><span class="side-label"><?php /*_e("[:en]End Date[:vi]Ngày Cuối");*/?></span> <span class="side-emphasized"><?php /*echo GIFT_DRIVE_END_DATE*/?></span></div>
                    </div>
                </header>
                <section>
                    <div class="statusbars">

                        <div class="progress">
                            <div id="progressbar" class="progress-bar" role="progressbar"  aria-valuemin="0" aria-valuemax="100" style="width:0">
                                <?php /*echo round(100*(intval(GIFT_DRIVE_RAISED)/intval(GIFT_DRIVE_FUNDRAISING_TARGET)),0);*/?> %
                            </div>
                        </div>
                    </div>
                </section>
            </article>
        </div>-->
        <div class="row">
            <article id="scholarship_panel" class="side-wraps side-panel">
                <header>
                    <h3><?php _e("[:en]Scholarship Fund[:vi]Quỹ học bổng");?></h3>
                    <a href="/gift/?gift=1112" class="btn call-to-action">Donate</a>
                    <div><a class="btn btn-default call-to-action" href="<?php echo get_permalink(SCHOLARSHIP_PAGE_ID);?>">Learn More</a></div>
                    <div class="side-panel-info-wrap">
                        <div class="side-add-info"><span class="side-label"><?php _e("[:en]Goal[:vi]Mục Tiêu");?></span> <span class="side-emphasized">$<?php echo SCHOLARSHIP_FUNDRAISING_TARGET;?></span></div>
                        <div class="side-add-info"><span class="side-label"><?php _e("[:en]End Date[:vi]Ngày Cuối");?></span> <span class="side-emphasized"><?php echo SCHOLARSHIP_END_DATE?></span></div>
                    </div>
                </header>
                <section>
                    <div class="statusbars">
                        <div class="progress">
                            <div id="education_progressbar" class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0">
                                <?php echo round(100*(intval(SCHOLARSHIP_RAISED)/intval(SCHOLARSHIP_FUNDRAISING_TARGET)),0);?> %
                            </div>
                        </div>
                    </div>
                </section>
            </article>

        <!--<div id="social-wrap" class="col-md-4 float_right">
                    <span id="facebook-like">
                        <iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fonemorestep.ca&amp;width=60&amp;layout=box_count&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=65&amp;appId=171453632872821" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:60px; height:65px;" allowTransparency="true"></iframe>
                     </span>
                    <span id="twitter">
                         <a class="twitter-share-button"
                            href="https://twitter.com/share"
                            data-url="http://onemorestep.ca"
                            data-via="takeonemorestep"
                            data-text="Great Xmas project - Gift Drive for orphans in Vietnam #onemorestep"
                            data-count="vertical">
                             Tweet
                         </a>
                        <script type="text/javascript">
                            window.twttr=(function(d,s,id){var t,js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return}js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);return window.twttr||(t={_e:[],ready:function(f){t._e.push(f)}})}(document,"script","twitter-wjs"));
                        </script>
                     </span>
            </div>
        </div>-->
     </div>
</div>

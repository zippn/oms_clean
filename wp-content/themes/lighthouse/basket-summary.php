 <div id="GiftBagSummary">

    <table width="100%" class="GiftBagSummary">
        <tr>
            <td  class="ColumnHeading" >
                <?php _e("[:en]Item[:vi]Quà"); ?>
            </td>
            <td width="18%" class="ColumnHeading" align="center">
                <?php _e("[:en]Qty[:vi]Số Lượng"); ?>

            </td>
            <td width="18%" class="ColumnHeading" align="center">
                <?php _e("[:en]Price[:vi]Giá"); ?>

            </td>
            <td width="14%" class="ColumnHeading" align="right">
                <?php _e("[:en]Total Price[:vi]Tổng Giá"); ?>
            </td>

         </tr>
    </table>

    <hr color="#999999" size="1px" />
    <table width="100%" class="GiftBagSummary">

    <?php 	$total = 0;

    foreach($_SESSION['basket'] as $k => $v):
       $current_item = get_post($k, OBJECT);

        if($current_item):
            $item_price = get_post_meta($k,'item_price',true);
    ?>
        <tr>
            <td >
                <?php echo $v['ItemName']; ?>
            </td >
            <td  width="18%" align="center">
                   <?php echo $v['ItemQty']; ?>
            </td>
            <td  width="18%" align="center">
                    <?php echo '$'.$item_price; ?>
            </td>
            <td  width="14%" align="right">
                    <?php $item_total = intval($v['ItemQty']) * intval($item_price);
                        echo '$' . number_format($item_total, 2, '.', ',');
                        $total += $item_total;
                    ?>
            </td>
        </tr>

    <?php endif; endforeach; ?>

       </table>
       <hr color="#999999" size="1px" />
    <table width="100%" class="GiftBagSummary">
     <tr>
        <td class="ColumnHeading"  align="right">
             <?php _e("[:en]Total[:vi]Tổng Số"); ?>
        </td>
        <td align="right" width="20%" class="ColumnHeading" >
            <?php echo '$' . number_format($total, 2, '.', ','); ?>
        </td>
     </tr>
    </table>
 </div>

 <br />

    <div id="SummaryControl">
        <h5> <?php _e("[:en]Purchase Gifts[:vi]Mua Quà"); ?> </h5>

           <?php echo get_the_content($post->ID);?>

            <div id="purchaseForm" class="float_left">
            <form id="frmCheque" method="POST" action="#">
                <input name="sid" value="<?php echo session_id();  ?>" type="hidden">
                <p class="regular_content"><label><?php _e("[:en]Name[:vi]Tên"); ?>: <input name="Name" id="txtName" type="text" size="30" maxlength="30" /></label></p>
                <p class="regular_content"><label>Email: <input name="Email" id="txtEmail" type="text" size="30" maxlength="30" />(<?php _e("[:en]Optional[:vi]Không bắt buộc"); ?>)</label></p>
                <br>
                <p class="regular_content">
                    <label>
                        <input name="Consent" id="cbConsent" type="checkbox" value="yes" checked/>
                            <?php if (qtranxf_getLanguage() == 'en'):?>
                                Yes, my name can be listed on the <a href="<?php echo get_post_permalink(23);?>">Donors</a> page
                            <?php else:?>
                                Tôi đồng ý để tên tôi được ghi trên trang <a href="<?php echo get_post_permalink(23);?>">Ân Nhân</a>
                            <?php endif;?>
                        </label>
                </p>
                <input name="custom" value="" type="hidden" id="cbPurchaseID"/>

            </form>
            <p><label><b><?php _e("[:en]Method of payment[:vi]Cách trả tiền"); ?>:</b></label></p>

                 <p class="regular_content" id="paymentOption">
                     <label>
                        <input type="radio" name="MethodOfPayment" value="radio" id="radVisa" />
                        Visa</label>

                      <label style="position:relative;left:30px">
                        <input type="radio" name="MethodOfPayment" value="radio" id="radPayPal" />
                        Pay Pal</label>

                      <label style="position:relative;left:50px">
                        <input type="radio" name="MethodOfPayment" value="radio" id="radCheque" />
                        Cheque</label>
                </p>


        <!--        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="frmPaypal">
                    <input name="cmd" value="_xclick" type="hidden">
                    <input name="business" id="paypal_email" value="" type="hidden">
                    <input type="hidden" name="currency_code" value="CAD">

                    <input name="quantity" value="1" type="hidden">
                    <input name="item_name" value="Orphanage Donation" type="hidden">
                    <input name="item_number" value="<?php /*$sid; */?>" type="hidden">
                    <input name="amount" value="<?php /*$total; */?>" type="hidden">
                    <input name="return" id="paypal_return" value="" type="hidden">
                    <input name="cancel_return" id="paypal_cancel_return" value="" type="hidden">
                    <input name="notify_url" id="paypal_notify_url" value="" type="hidden">
                    <input name="custom" value="publish name" type="hidden" id="ppPublishName"/>

                </form>-->



                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="frmPaypal">
                    <input name="cmd" value="_xclick" type="hidden">
                    <input name="business" id="paypal_email" value="" type="hidden">
                    <input type="hidden" name="currency_code" value="CAD">
                    <input name="quantity" value="1" type="hidden">
                    <input name="item_name" value="Orphanage Donation" type="hidden">
                    <input name="item_number" value="<?php echo session_id();  ?>" type="hidden">
                    <input name="amount" value="<?php echo $total; ?>" type="hidden">
                    <input name="return" id="paypal_return" value="" type="hidden">
                    <input name="cancel_return" id="paypal_cancel_return" value="" type="hidden">
                    <input name="notify_url" id="paypal_notify_url" value="" type="hidden">
                    <input name="custom" value="" type="hidden" id="purchaseID"/>

                </form>
                
                 <form method="post" id="frmCancel">
                    <?php wp_nonce_field( "clear", "pr_nonce") ?>
                     <div class="btn" id="btnClear">
                        <div class="btn-left-edge"></div>
                        <div class="btn-center"><span><?php _e("[:en]Clear Gift Bag[:vi]Bãi Bỏ");?></span></div>
                        <div class="btn-right-edge"></div>
                    </div>
                </form>

                    <div class="btn" id="btnPurchase">
                        <div class="btn-left-edge"></div>
                        <div class="btn-center"><span><?php _e("[:en]Purchase[:vi]Mua Quà");?></span></div>
                        <div class="btn-right-edge"></div>
                    </div>

                      <br />
                     <br />
        </div>
        <div id="ByChequeInfo" class="float_right">
                 <p class="regular_content">
                     <?php if (qtranxf_getLanguage() == 'en'):?>
                        Please print this page, make the cheque payable to <b>ONEMORESTEP International Relief Org</b> and mail it to the address below:
                    <?php else:?>
                       Xin quý vị in trang này ra, gởi kèm với cheque đến <b>ONEMORESTEP International Relief Org</b> tại địa chỉ sau:
                    <?php endif;?>
                 </p>

                        <p class="regular_content">609 - 25 Leith Hill Rd.<br />
                        Toronto, ON<br />
                        M2J 1Z1
                        </p>
        </div>
    </div>
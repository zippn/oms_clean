<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Please browse readme.txt for credits and forking information
 * @package Lighthouse
 */

get_header(); ?>

	<div class="container home-page">
            <div class="row">
                <div id="primary" class=" content-area">
						<main id="main" class="site-main" role="main">

                            <div class="row">
                                <div class="col float_left" >

                                    <?php
                                    query_posts('cat='.HOME2011_CAT_ID);
                                    if (have_posts()) : while (have_posts()) : the_post(); ?>

                                        <h1><?php the_title(); ?></h1>
                                        <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                                            the_post_thumbnail('home-page');
                                        } ?>
                                        <div class="post" id="post-<?php the_ID(); ?>">
                                            <div class="entry">
                                                <?php the_content(); ?>
                                            </div><!--entry-->
                                        </div><!--post-->
                                    <?php endwhile; endif; ?>
                                </div>
                                <?php //do_shortcode('[oms-shopping-list]'); ?>


                            </div>



                        </main><!-- #main -->
				</div><!-- #primary -->


            </div><!--row-->


		</div><!--.container-->
		<?php get_footer(); ?>

			
    

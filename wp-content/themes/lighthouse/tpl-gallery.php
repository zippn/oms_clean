<?php
/*
 * Template Name: Gallery
 * @package WordPress
 * @subpackage citi
 */?>


<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div id="primary" class="col-md-9 content-area">
            <main id="main" class="site-main" role="main">
                <div class="row">
                    <div id="content-inner-wrap">

                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <div id="breadcrums">
                                <a href="<?php bloginfo("url");?>">Home</a> &rsaquo;
                                <?php $ancestor_IDs = get_post_ancestors(get_the_ID());
                                $num_of_ancestors = sizeof($ancestor_IDs);?>

                                <?php for($i = 0;$i<$num_of_ancestors;$i++ ):?>
                                    <a href="<?php echo get_post_permalink($ancestor_IDs[$num_of_ancestors-1-$i]);?>"><?php echo get_the_title($ancestor_IDs[$num_of_ancestors-1-$i]);?></a> &rsaquo;
                                <?php endfor;?>

                                <?php the_title(); ?>
                            </div>
                            <?php get_template_part( 'template-parts/content', 'page' ); ?>

                        <?php endwhile; endif; ?>


                    </div>
                </div>

            </main><!-- #main -->
        </div><!-- #primary -->

        <?php get_sidebar(); ?>

    </div> <!--.row-->
</div><!--.container-->
<?php get_footer(); ?>



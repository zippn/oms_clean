<?php
/*
 * Template Name: Gift
 * @package WordPress
 * @subpackage oms
 */?>
<?php get_template_part('header')?>

<div class="container oms-page-content">

    <div class="row">
        <div id="primary" class="col-md-9 content-area">
            <main id="main" class="site-main" role="main">
                <div class="row">
                    <div id="content-inner-wrap">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <div id="breadcrums">
                                <a href="<?php bloginfo("url");?>">Home</a> &rsaquo;
                                <?php $ancestor_ID = CURRENT_DRIVE_ID;?>

                                    <a href="<?php echo get_post_permalink($ancestor_ID);?>"><?php echo get_the_title($ancestor_ID);?></a> &rsaquo;

                            </div>
                            <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                                the_post_thumbnail('sectionpage');
                            } ?>
                            <div class="post" id="post-<?php the_ID(); ?>">
                                <div class="entry">
                                    <?php the_content(); ?>
                                </div><!--entry-->
                            </div><!--post-->
                        <?php endwhile; endif; ?>
                    </div><!--cs wrap-->

                </div>



            </main><!-- #main -->
        </div><!-- #primary -->

        <?php get_sidebar();?>
    </div><!--row-->


</div><!--.container-->
<script>


    var $j = jQuery.noConflict();

    $j(document).ready(function($) {

//item title for breadcrum
        var item = $('#item-title').text();
        $('<a id="item-breadcrum">'+item+'</a>').appendTo('#breadcrums');



    });

</script>
<?php get_footer(); ?>


<?php
/*
 * Template Name: Scholarships
 * @package WordPress
 * @subpackage citi
 */?>

<?php get_header(); ?>

<div class="container">

    <div class="row">
        <div id="primary" class="col-md-9 content-area">
            <main id="main" class="site-main" role="main">
                <div class="row">
                    <div id="content-inner-wrap">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <?php get_template_part( 'template-parts/content', 'scholarship' ); ?>
                        <?php endwhile; endif; ?>
<!--                        <div id="scholarship-slider"><?php /*echo do_shortcode('[smartslider3 slider=12]'); */?></div>
-->
                        <div id="student-profiles">
                            <?php
                            $current_page_id = $post->ID;
                            $students = new WP_Query(array('post_type'=>'student','posts_per_page'=>-1 ));
                            if($students->have_posts()):?>
                                <h2>Here are the scholarship recipients of the 2018-2019 school year</h2>
                                <ul id="student-list">
                                    <?php while($students->have_posts()):$students->the_post();?>
                                        <? $grad_year = get_field('graduation_year');
                                            $status = get_field_object('status');
                                        ?>
                                        <li class="student-wrap <? echo $status['value'];?>">
                                            <h4><?php  the_title();?></h4>
                                            <?php the_content()?>
                                            <div class="student-info">

                                                <p><label>Field of Study </label><?php echo get_post_meta(get_the_id(),'field_of_study', true);?></p>
                                                <p ><label >Home Region </label><?php the_field('region');?></p>
                                                <p ><label>Years Received Scholarship </label><?php the_field('years_received_scholarship');?></p>
                                                <p ><label>Current Status </label><? echo $status['choices'][$status['value']];?></p>
                                                <? if($grad_year):?>
                                                        <p ><label>Graduation Year </label><?php echo $grad_year;?></p>
                                                <? endif;?>
                                            </div>

                                        </li>

                                    <?php endwhile; wp_reset_query();?>
                                </ul>
                            <?php endif;?>
                        </div>
                    </div><!--cs wrap-->

                </div>




            </main><!-- #main -->
        </div><!-- #primary -->

        <?php get_sidebar();?>
    </div><!--row-->


</div><!--.container-->
<?php get_footer(); ?>




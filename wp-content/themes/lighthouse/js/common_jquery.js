

var $j = jQuery.noConflict();

$j(document).ready(function($) {

//tag content area
    $('#content .container:first').addClass('oms-page-content');


//move translate opton into menu
   // jQuery("#small_menu").detach().appendTo('#menu-main');

//checkout
    $('.form-row .toggle-on').html('YES');

//gift
    $('#gift-list a').click(function() {
        //location.reload(true);
        $('.modal-footer .btn').click();
    });

    //sidebar mobile
    $(window).click(function() {
        $('.side-panel').removeClass('active-panel');
    });
    $('#gift-list-mobile-wrapper a').click(function(event){
        event.stopPropagation();

        var btnSelect = $(this).attr('data-menu');
       // console.log(btnSelect);
        if($(btnSelect).hasClass('active-panel')){
            $(btnSelect).removeClass('active-panel');

        }else{
            $('.side-panel').removeClass('active-panel');
            $(btnSelect).addClass('active-panel');

        }

    });


    $(window).scroll(function (event) {
        var scrollPos = $(window).scrollTop();
        // Do something
        if(scrollPos>=160){
            $('.side-panel').removeClass('active-panel');
        }
//        console.log(scrollPos);
    });




});

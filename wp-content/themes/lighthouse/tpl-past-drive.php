<?php
/*
 * Template Name: Past Drive
 * @package WordPress
 * @subpackage citi
 */?>

<?php get_header(); ?>

	<div class="container past-campaign">

        <div class="row">
            <div id="primary" class="col-md-9 content-area">
                <main id="main" class="site-main" role="main">

                    <div class="row">
                        <div id="content-inner-wrap">
                            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                <div id="breadcrums">
                                    <a href="<?php bloginfo("url");?>">Home</a> &rsaquo;
                                    <?php $ancestor_IDs = get_post_ancestors(get_the_ID());
                                    $num_of_ancestors = sizeof($ancestor_IDs);?>

                                    <?php for($i = 0;$i<$num_of_ancestors;$i++ ):?>
                                        <a href="<?php echo get_post_permalink($ancestor_IDs[$num_of_ancestors-1-$i]);?>"><?php echo get_the_title($ancestor_IDs[$num_of_ancestors-1-$i]);?></a> &rsaquo;
                                    <?php endfor;?>

                                    <?php the_title(); ?>
                                </div>
                               <!-- <div class="float_right">
                                    <a href="<?php /*$parentLink = get_permalink($post->post_parent); echo $parentLink; */?>">
                                        <button class="parent-page-btn" type="button" class="btn btn-primary" >
                                            Back to Campaigns
                                        </button>
                                    </a>
                                </div>-->

                                <h1><?php the_title(); ?></h1>
                                <div class="past-drive-slider row">

                                    <?php
                                    $sliderCode= get_field('slider_code');
                                    echo do_shortcode("[smartslider3 slider=$sliderCode]");

                                    ?>



                                </div>

                                <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                                    the_post_thumbnail('sectionpage');
                                } ?>
                                <div class="post" id="post-<?php the_ID(); ?>">
                                    <div class="entry">
                                        <div class="row">
                                            <div class="drive-meta col-md-5">
                                                <h3>
                                                    <span>Gift Drive: </span><span class="green-text">$<?php echo get_field('drive_amount');?></span>
                                                </h3>
                                                <h3>
                                                    <span>Scholarships: </span><span class="green-text">$<?php echo get_field('scholarship_fund');?></span>
                                                </h3>
                                                <h3>
                                                    <span>Orphanages: </span><span class="green-text"><?php echo get_field('number_of_orphanages');?></span>
                                                </h3>
                                                <h3>
                                                    <span>Children: </span><span class="green-text"><?php echo get_field('number_of_children');?></span>
                                                </h3>
                                            </div>
                                            <div class="drive-content col-md-7">
                                                <?php the_content(); ?>
                                            </div>
                                        </div>
                                        <div>
                                            <h3>The Items</h3>
                                            <?php
                                            $year = get_field('year');
                                            $post_id = get_the_ID();

                                            /* this short codes changes the post ID, TO DO: Need to reset the query in the shortcode */
                                            do_shortcode("[oms-shopping-list year=$year]");
                                            /*****************/

                                            ?>

                                        </div>
                                        <div class="drive-team row">
                                            <h3>Donors</h3>
                                            <?php
                                                $donors = get_post_meta($post_id, 'drive_donors', true);
                                                echo $donors;
                                            ?>

                                        </div>
                                        <div class="drive-team row">
                                            <h3>OMS Team</h3>
                                            <?php
                                                $team_members = get_post_meta($post_id, 'team_members', true);
                                                echo $team_members;

                                            ?>
                                        </div>
                                    </div><!--entry-->
                                </div><!--post-->
                            <?php endwhile; endif; ?>
                            <!--<div class="float_right">
                                <a href="<?php /*$parentLink = get_permalink($post->post_parent); echo $parentLink; */?>">
                                    <button class="parent-page-btn" type="button" class="btn btn-primary" >
                                        Back to Campaigns
                                    </button>
                                </a>
                            </div>-->
                        </div><!--cs wrap-->

                    </div>




                </main><!-- #main -->
            </div><!-- #primary -->

            <?php get_sidebar();?>
        </div><!--row-->


    </div><!--.container-->
<?php get_footer(); ?>

			
    

<?php
/*
 * Template Name: Who We Help
 * @package WordPress
 * @subpackage citi
 */?>

<?php get_header(); ?>

    <div class="container">
    <div class="row">
        <div id="primary" class="col-md-9 content-area">
            <main id="main" class="site-main" role="main">
                <div class="row">
                    <div id="content-section-wrap">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <?php get_template_part( 'template-parts/content', 'page' ); ?>


                        <?php endwhile; endif; ?>
                        <?php
                        wp_reset_postdata();
                        $query = new WP_Query(array(
                                'post_type' => 'page',
                                'meta_key' => '_wp_page_template',
                                'meta_value' => 'tpl-orphanage-profile.php')
                        );
                        $current_page_id = $post->ID;

                      //   var_dump($query);
                        ?>
                        <?php if($query->have_posts()):?>
                            <ul id="orphanage-list">
                                <?php while($query->have_posts()):$query->the_post();?>
                                    <li class="orphanage-wrap row">
                                        <div class="image-wrap col-md-6">
                                            <?php
                                            $sliderCode= get_field('slider_code');
                                            echo do_shortcode("[smartslider3 slider=$sliderCode]");
                                            ?>
                                        </div>
                                        <article class="article-wrap col-md-6">
                                            <h2><?php the_title();?></h2>
                                            <?php the_content();?>
                                        </article>
                                    </li>

                                <?php endwhile; wp_reset_query();?>
                            </ul>
                        <?php endif;?>



                    </div><!--content-section-wrap-->
                </div>

            </main><!-- #main -->
        </div><!-- #primary -->

        <?php get_sidebar(); ?>

    </div> <!--.row-->
</div><!--.container-->

<?php get_footer(); ?>

			
    

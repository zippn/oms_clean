<?php
/*
 * Template Name: Current Drive
 * @package WordPress
 * @subpackage citi
 */?>

<?php get_header(); ?>

	<div class="container">

        <div class="row">
            <div id="primary" class="col-md-9 content-area">
                <main id="main" class="site-main" role="main">
                    <div class="row">
                        <div id="content-inner-wrap">
                            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                <h1><?php the_title(); ?></h1>
                                <!--slider-->
                                <div class="current-drive-slider row">

                                    <?php
                                        $sliderCode= get_field('slider_code');
                                    echo do_shortcode("[smartslider3 slider=$sliderCode]");

                                    ?>



                                </div>
                                <div class="post" id="post-<?php the_ID(); ?>">
                                    <div class="entry">
                                        <div class="row">
                                            <div class="drive-meta col-md-5">
                                                <h3>
                                                    <span>Gift Drive: </span><span class="green-text">$<?php echo GIFT_DRIVE_FUNDRAISING_TARGET;?></span>
                                                </h3>
                                                <h3>
                                                    <span>Scholarships: </span><span class="green-text">$<?php echo SCHOLARSHIP_FUNDRAISING_TARGET;?></span>
                                                </h3>
                                                <h3>
                                                    <span>Orphanages: </span><span class="green-text"><?php echo get_field('number_of_orphanages');?></span>
                                                </h3>
                                                <h3>
                                                    <span>Children: </span><span class="green-text"><?php echo get_field('number_of_children');?></span>
                                                </h3>
                                            </div>
                                            <div class="drive-content col-md-7">
                                                <?php the_content(); ?>
                                            </div>


                                        </div>
                                        <?php //do_shortcode('[oms-shopping-list]'); ?>

                                    </div><!--entry-->
                                </div><!--post-->
                            <?php endwhile; endif; ?>
                        </div><!--cs wrap-->

                    </div>




                </main><!-- #main -->
            </div><!-- #primary -->

            <?php get_sidebar();?>
        </div><!--row-->


    </div><!--.container-->
<?php get_footer(); ?>

			
    

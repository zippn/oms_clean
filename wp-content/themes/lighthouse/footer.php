<?php
/**
 * The template for displaying the footer.
 *
 * Please browse readme.txt for credits and forking information
 * Contains the closing of the #content div and all content after
 *
 * @package Lighthouse
 */

?>






</div><!-- #content -->
<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="row site-info">
		<?php echo '&copy; '.date_i18n(__('Y','lighthouse')); ?> <?php bloginfo( 'name' ); ?> | onemorestepvn@gmail.com
	</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script>


    var $j = jQuery.noConflict();

    $j(document).ready(function($) {

//checkout
        $('.form-row .toggle').css('width','90%');
        $('.form-row .toggle-on').html('YES');
        $('.form-row .toggle-off').html('NO');



    });

</script>


</body>
</html>

<?php
/*
 * Template Name: Gift
 * @package WordPress
 * @subpackage oms
 */?>
<?php get_template_part('header')?>

<div class="container">

    <div class="row">
        <div id="primary" class="col-md-9 content-area">
            <main id="main" class="site-main" role="main">
                <div class="row">
                    <div id="content-inner-wrap">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                                the_post_thumbnail('sectionpage');
                            } ?>
                            <div class="post" id="post-<?php the_ID(); ?>">
                                <div class="entry">
                                    <?php the_content(); ?>
                                </div><!--entry-->
                            </div><!--post-->
                        <?php endwhile; endif; ?>
                    </div><!--cs wrap-->

                </div>



            </main><!-- #main -->
        </div><!-- #primary -->

        <?php get_sidebar();?>
    </div><!--row-->


</div><!--.container-->
<?php get_footer(); ?>


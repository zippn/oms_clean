<?php
/**
 * The template used for displaying page content in page.php
 *
 * Please browse readme.txt for credits and forking information
 * @package Lighthouse
 */

?>

<article id="post-<?php the_ID(); ?>" >

	<?php lighthouse_featured_image_disaplay(); ?>

	<header >
		<span class="screen-reader-text"><?php the_title();?> </span>
		<h1><?php the_title();?> <a class="btn" href="#student-profiles">View Student Profiles</a></h1>

		<div class="entry-meta"></div><!-- .entry-meta -->
        <?php
        echo do_shortcode('[smartslider3 slider=12]');
        ?>
	</header><!-- .entry-header -->
	
	<div >

		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'lighthouse' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php edit_post_link( esc_html__( 'Edit', 'lighthouse' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->


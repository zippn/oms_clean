

<?php
/**
 * The template for displaying student pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please browse readme.txt for credits and forking information
 * @package Lighthouse
 */

get_header(); ?>

<div class="container">
    <div class="row">
        <div id="primary" class="col-md-9 content-area">
            <main id="main" class="site-main" role="main">
                <div class="row">
                    <div id="content-inner-wrap">

                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <div id="breadcrums">
                                <a href="<?php bloginfo("url");?>">Home</a> &rsaquo;
                                <?php $ancestor_ID = 625;?>

                                <a href="<?php echo get_post_permalink($ancestor_ID);?>"><?php echo get_the_title($ancestor_ID);?></a> &rsaquo;
                                <span><?php the_title()?></span>

                            </div>
                            <div class="post" id="post-<?php the_ID(); ?>">
                                <header class="entry-header">
                                    <span class="screen-reader-text"><?php the_title(); ?></span>
                                    <h1 class="entry-title"><?php the_title(); ?></h1>
                                </header>
                                <div class="entry">
                                    <div class="content-left-side row">
                                        <div class="row">
                                            <div class="featured"><?php the_post_thumbnail('large');?></div>
                                            <ul class="student-profile">
                                                <li><span class="label"><?php _e("[:en]Ethnicity[:vi]Dân tộc");?></span> : <?php the_field('tribe');?></li>
                                                <!--<li><span class="label"><?php /*_e("[:en]Region[:vi]Tỉnh");*/?></span> : <?php /*the_field('region');*/?></li>-->
                                                <li><span class="label"><?php _e("[:en]Program Duration[:vi]Đang học năm");?></span> : <?php the_field('year_of_study');?></li>
                                                <li><span class="label"><?php _e("[:en]Field of Study[:vi]Ngành Học");?></span> : <?php echo get_post_meta(get_the_ID(),'field_of_study',true);?></li>
                                                <!--<span class="label"><?php /*_e("[:en]Left[:vi]Còn");*/?></span> : --><?php /*the_field('status');*/?>
                                                <li><span class="label"><?php _e("[:en]School Years Receiving Scholarship[:vi]Những năm được học bổng");?></span> : <?php the_field('years_received_scholarship');?></li>
                                                <?php $field = get_field_object('status');
                                                $value = get_field('status');
                                                $label = $field['choices'][ $value ];
                                                ?>
                                                <li><span class="label"><?php _e("[:en]Current Status[:vi]Tình Trạng Hiện Tại");?></span> : <?php echo $label;?></li>
                                            </ul>
                                        </div>

                                        <div class="questionnaire row">
                                            <?php the_content(); ?>

                                        </div>
                                    </div>
                                    <header class="entry-header">
                                        <span class="screen-reader-text">Student Profiles</span>
                                        <h1 class="entry-title">Student Profiles</h1>
                                    </header>
                                    <?php
                                    $current_page_id = $post->ID;
                                    $students = new WP_Query(array('post_type'=>'student' ));
                                    if($students->have_posts()):?>
                                        <ul id="student-list">
                                            <?php while($students->have_posts()):$students->the_post();?>
                                                <li class="student-wrap">
                                                    <a href="<?php the_permalink();?>" title="<?php the_title();?>" <?php echo $current_page_id==$post->ID?'class="active"':''?>>

                                                        <h3>
                                                            <?php  echo get_the_title();?>
                                                        </h3>
                                                    </a>
                                                </li>

                                            <?php endwhile; wp_reset_query();?>
                                        </ul>
                                    <?php endif;?>
                                </div><!--entry-->
                            </div><!--post-->
                        <?php endwhile; endif; ?>
                    </div>
                </div>

            </main><!-- #main -->
        </div><!-- #primary -->

        <?php get_sidebar(); ?>

    </div> <!--.row-->
</div><!--.container-->
<?php get_footer(); ?>



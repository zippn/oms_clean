<?php
/*
 * Template Name: Home
 * @package WordPress
 * @subpackage citi
 */?>

<?php get_template_part('header')?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
<div class="left_column float_left" >
    <?php
        //query_posts('cat=3');
        if (have_posts()) : while (have_posts()) : the_post(); ?>

            <h1><?php the_title(); ?></h1>
            <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                  the_post_thumbnail('home-page');
                } ?>
            <div class="post" id="post-<?php the_ID(); ?>">
                <div class="entry">
                    <?php the_content(); ?>
                </div><!--entry-->
            </div><!--post-->
   <?php endwhile; endif; ?>
</div>
 <div class="right_column float_right ">
     <h2>
         Follow Us On Twitters
     </h2>
        <div class="twitter-wrap horizontal-divider news" id="title-twitter">

            <script src="http://widgets.twimg.com/j/2/widget.js"></script>
            <script>
            new TWTR.Widget({
              version: 2,
              type: 'profile',
              rpp: 3,
              interval: 30000,
              width: 'auto',
              height: 100,
              theme: {
                shell: {
                  background: '#ffffff',
                  color: '#6eb306'
                },
                tweets: {
                  background: '#faf7fa',
                  color: '#4d4b4d',
                  links: '#c51372'
                }
              },
              features: {
                scrollbar: false,
                loop: true,
                live: false,
                behavior: 'default'
              }
            }).render().setUser('takeonemorestep').start();
            </script>
        </div>

        <div class="facebook-wrap news">
            <div class="facebook">
                <h2>Follow Us On Facebook</h2>
                <div class="fb-like-box" data-href="http://www.facebook.com/pages/OneMoreStep/141055129331797" data-width="280" data-height="420" data-show-faces="false" data-border-color="#dadada" data-stream="true" data-header="false"></div>
                
            </div>
        </div>

</div><!--right_column-->



<?php get_template_part('footer')?>



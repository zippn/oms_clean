<?php
/*
 * Template Name: Gift List
 * @package WordPress
 * @subpackage oms
 */?>
<?php get_template_part('header')?>

<div class="container oms-page-content">

    <div class="row">
        <div id="primary" class="col-md-9 content-area">
            <main id="main" class="site-main" role="main">
                <div class="row">
                    <div id="content-inner-wrap">
                        <?php
                        $category_posts = new WP_Query(array(
                            'post_type' => 'product',
                            'posts_per_page'=>35,
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'product_cat',
                                    'field'    => 'name',
                                    'terms'    => '2016',
                                )
                            )
                        ));
                        ?>
                        <?php

                        if($category_posts->have_posts()) :
                            while($category_posts->have_posts()) :
                                $category_posts->the_post();
                                ?>

                                <h1><?php the_title() ?></h1>
                                <div class='post-content'><?php the_content() ?></div>

                                <?php
                            endwhile;
                        else:
                            ?>

                            Oops, there are no posts.

                            <?php
                        endif;
                        ?>
                    </div><!--cs wrap-->

                </div>



            </main><!-- #main -->
        </div><!-- #primary -->

        <?php get_sidebar();?>
    </div><!--row-->


</div><!--.container-->
<?php get_footer(); ?>


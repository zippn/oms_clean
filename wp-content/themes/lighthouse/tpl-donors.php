<?php
/*
 * Template Name: Donor List
 * @package WordPress
 * @subpackage citi
 */?>
<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div id="primary" class="col-md-9 content-area">
            <main id="main" class="site-main" role="main">
                <div class="row">
                    <div id="content-inner-wrap">

                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <?php get_template_part( 'template-parts/content', 'page' ); ?>

                        <?php endwhile; endif; ?>

                    </div>
                </div>

            </main><!-- #main -->
        </div><!-- #primary -->

        <?php get_sidebar(); ?>

    </div> <!--.row-->
</div><!--.container-->
<?php get_footer(); ?>


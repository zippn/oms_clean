<?php
/*
 * Template Name: Past Drive Main
 * @package WordPress
 */?>


<?php get_header(); ?>

    <div class="container">
        <div class="row">
            <div id="primary" class="col-md-9 content-area">
                <main id="main" class="site-main" role="main">
                    <div class="row">
                        <div id="content-section-wrap">
                            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                <?php get_template_part( 'template-parts/content', 'page' ); ?>


                            <?php endwhile; endif; ?>
                            <?php
                            wp_reset_postdata();
                            $query = new WP_Query(array(
                                    'post_type' => 'page',
                                    'meta_key' => '_wp_page_template',
                                    'meta_value' => 'tpl-past-drive.php')
                            );
                            $current_page_id = $post->ID;

                            // var_dump($year);
                            ?>
                            <?php if($query->have_posts()):?>
                                <ul id="student-list">
                                    <?php while($query->have_posts()):$query->the_post();?>
                                        <li class="student-wrap col-md-3">
                                            <a href="<?php the_permalink();?>" title="<?php the_title();?>" <?php echo $current_page_id==$post->ID?'class="active"':''?>>
                                                <?php if(has_post_thumbnail()):?>
                                                    <?php the_post_thumbnail('thumbnail');?>
                                                <?php else:?>
                                                    <div class="place-holder">
                                                        <?php  _e("[:en]".get_the_title()."'s photo coming soon[:vi]Hình của ".get_the_title()."sẽ được tải lên");?>
                                                    </div>
                                                <?php endif;?>
                                            </a>
                                        </li>

                                    <?php endwhile; wp_reset_query();?>
                                </ul>
                            <?php endif;?>



                        </div><!--content-section-wrap-->
                    </div>

                </main><!-- #main -->
            </div><!-- #primary -->

            <?php get_sidebar(); ?>

        </div> <!--.row-->
    </div><!--.container-->




<?php get_footer(); ?>
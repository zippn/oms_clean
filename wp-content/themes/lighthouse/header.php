<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
* Please browse readme.txt for credits and forking information
 * @package Lighthouse
 */

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta name="viewport" content="width=device-width" />
  <link rel="profile" href="http://gmpg.org/xfn/11" />
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

  <?php wp_head(); ?>

    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/customize-css.css" />


    <script src="https://www.paypalobjects.com/api/checkout.js" data-version-4></script>

    <script type="text/javascript">

        //location.reload(true);

        var $j = jQuery.noConflict();
        $j(document).ready(function($) {
            //$("#content_wrapper_outer").corner("10px");
            //$(".item").corner("8px");


            //progress bar
            var giftDriveProgress = <?php echo intval(100*intval(get_post_meta(HOME2011_PAGE_ID,'gift_amount_raised',true))/intval(GIFT_DRIVE_FUNDRAISING_TARGET));?>+'%';
            var scholarshipProgess = <?php echo intval(100*intval(get_post_meta(HOME2011_PAGE_ID,'education_amount_raised',true))/intval(SCHOLARSHIP_FUNDRAISING_TARGET));?>+'%';

            $('#progressbar').css('width',giftDriveProgress);
            $("#education_progressbar").css('width', scholarshipProgess);

            /*$("#gift-receipient #gift-receipient-list a.sidebar-level1").click(function(e){
                e.preventDefault();
                if($("#gift-receipient-list #receipient-list").hasClass('hide')){
                    $("#gift-receipient-list #receipient-list").removeClass('hide');

                }else{
                    $("#gift-receipient-list #receipient-list").addClass('hide');

                }
            });*/
        });
    </script>


</head>

<body <?php body_class(); ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=6260354563';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
  <div id="page" class="hfeed site">
    <header id="masthead"  role="banner">
      <nav class="navbar lh-nav-bg-transform navbar-default navbar-fixed-top navbar-left" role="navigation"> 
        <!-- Brand and toggle get grouped for better mobile display --> 
        <div class="container" id="navigation_menu">
            <div class="navbar-header">
                <?php if ( has_nav_menu( 'primary' ) ) { ?>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only"><?php echo esc_html('Toggle Navigation', 'lighthouse') ?></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                <?php } ?>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                    <div class="navbar-brand"><img src="<?php bloginfo('stylesheet_directory');?>/images/logo-white.png" border="0" /></div>

                </a>
            </div>
            <div id="secondary-menu">
                <ul class="seccond-menu nav navbar-nav">
                    <li class="menu-item"><a href="/cart"><i class="fa fa-shopping-bag"></i> Gift Bag </a></li>
                </ul>
            </div>
            <?php if ( has_nav_menu( 'primary' ) ) {
                lighthouse_header_menu(); // main navigation
            }
            ?>


            <div id="gift-list-mobile-wrapper">
                <div id="gl-inner-wrapper">

                    <a id="gift-drive-btn" data-menu="#gift-drive_panel">
                        <button type="button" class="btn btn-primary" >
                            Gift Drive
                        </button>
                    </a>
                    <a id="scholarship-btn" data-menu="#scholarship_panel">
                        <button id="view-scholarship-btn" type="button" class="btn btn-primary" >
                            Scholarship
                        </button>
                    </a>
                </div>

            </div>



        </div><!--#container-->
        </nav>

        <!--slider-->

    </header>
      <?php if(is_home()){?>
        <div id="home-slider">
            <div id="home-sidebar">
                <?php get_sidebar();?>
            </div>
          <?php echo do_shortcode('[smartslider3 slider=2]'); ?>
        </div>
      <?php }?>



    <div id="content" class="site-content">
<?php get_template_part('header')?>
<div id="content-inner-wrap">
     <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <h1><?php _e("[:en]Archive[:vi]Kho Lưu Trữ"); ?> | <?php the_title(); ?></h1>

            <div class="post" id="post-<?php the_ID(); ?>">
                <div class="entry">
                    <?php the_excerpt();?>


                    <?php
                        $fundraising_years = get_the_terms(get_the_ID(),'fundraising_year');
                        if( $fundraising_years && $fundraising_years[0]->name == '2015' ){
                            //get_template_part('gift-list-archive-jigproducts');
                        }else{
                            get_template_part('gift-list','archive');
                        }
                    ?>

                    <?php the_content(); ?>
                </div><!--entry-->
            </div><!--post-->
       <?php endwhile; endif; ?>
</div><!--cs wrap-->

<?php get_template_part('footer')?>
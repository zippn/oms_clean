        <a id="arrow_left" title="Next Page" href="#"></a>
      	<div id="gift_list_wrapper"> 
        	<!--<div class="gift_list_padding"></div>-->
      		
            	<?php  $items = new WP_Query('post_type=item&fundraising_year=2014');
                        $itemCount=0;
                        $total=0;
                        while($items->have_posts()):
                            $items->the_post();
                            //$post_meta = get_post_custom(get_the_ID());
                            $total += intval(get_field('item_quantity'))*intval(get_field('item_price'));
                            ?>

                            <?php if($itemCount==0):?>
                                <div class="gift_list_inner_wrapper" name="<?php echo $itemCount?>" id="gift-list-<?php echo $itemCount?>" style="display:block;">
                           <?php elseif($itemCount%6 == 0): ?>
                                </div>
                                <div class="gift_list_inner_wrapper"  name="<?php echo $itemCount?>" id="gift-list-<?php echo $itemCount?>"  style="display:none;">
                            <?php endif; ?>

                                <div class="item">
                                    <div class="item_content_wrapper" id="item-<?php the_ID(); ?>">
                                        <a href="<?php the_permalink(); ?>" id="<?php the_ID(); ?>" class="giftlist" style="display: block" rel="history">
                                            <h4><?php the_title(); ?></h4>

                                            <div class="float_left"><p class="gift_item_content">$<?php the_field('item_price') ?></p></div>
                                              <div class="float_right"><p class="gift_item_content"><?php _e("[:en]Left[:vi]Còn"); ?>: <?php the_field('item_remaining');?>/<?php the_field('item_quantity');?></p>
                                            </div>
                                            <div class="ItemImage">
                                                <?php the_post_thumbnail(); ?>
                                            </div>
                                        </a>

                                    <?php if (!empty($_SESSION['basket']) && array_key_exists(get_the_ID(), $_SESSION['basket'])){ ?>
                                        <div class="PurchasedBow" id="purchased<?php the_ID(); ?>">
                                             <a href="<?php the_permalink(); ?>"  rel="history"><img src="<?php bloginfo('stylesheet_directory');?>/images/purchased-bow.png" border="0" alt="<?=$v['name']; ?>" /></a>
                                        </div>
                                    <?php } else if (get_field('item_remaining') == 0) { ?>
                                        <div class="PurchasedSoldOut" id="soldout<?php the_ID(); ?>">
                                            <a href="<?php the_permalink(); ?>"  rel="history"><img src="<?php bloginfo('stylesheet_directory');?>/images/sold-out-<?php echo qtranxf_getLanguage();?>.png" border="0" alt="<?php the_title(); ?>" /></a>
                                        </div>

                                    <?php } ?>
                                    </div>
                                    
								
                                </div>
   						
								<?php $itemCount++;?>
                                <?php endwhile; //echo "item total: $$total"; ?>
           					</div>

        </div>

        <a id="arrow_right" title="Next Page" href="#"></a>
        <img src="<?php bloginfo('stylesheet_directory');?>/images/view_more.png" id="view-more-arrow">


        

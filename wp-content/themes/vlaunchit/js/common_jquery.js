var $j = jQuery.noConflict();

$j(document).ready(function($) {

    //2016
    var paymentSelect = $('#payment .payment_methods');

 //   $('head').append('<script src="https://www.paypalobjects.com/api/checkout.js" data-version-4></script>');
   // paymentSelect.append('<li><div id="paypal-button"></div></li>');


    var itemStrip=false;
    var pos=0;
    var interval = 6;
    var numOfPages = 3;
    var lastPageStartPos = numOfPages*interval-interval;
    var root = getBaseURL()+"wp-content/themes/vlaunchit/";

    function getBaseURL() {
		var url = location.href;  // entire url including querystring - also: window.location.href;
		var baseURL = url.substring(0, url.indexOf('/', 14));


		if (baseURL.indexOf('http://localhost') != -1) {
			// Base Url for localhost
			var url = location.href;  // window.location.href;
			var pathname = location.pathname;  // window.location.pathname;
			var index1 = url.indexOf(pathname);
			var index2 = url.indexOf("/", index1 + 1);
			var baseLocalUrl = url.substr(0, index2);

			return baseLocalUrl + "/";
		}else if (url.indexOf('/oms_2011') != -1) {
			return baseURL + "/oms_2011/";
		}
		else {
			// Root Url for domain name
			return baseURL + "/";
		}
	}

    /**2015 fix by vn**/

    //$('<a id="arrow_right" title="Next Page" href="#"></a>').insertBefore('#view-more-arrow');
    $('#gift-list-outter-2').append('<a id="arrow_right" title="Next Page" href="#"></a>');
    $("#gift-list-outter-2").prepend('<a id="arrow_left" title="Next Page" href="#"></a>');

   // $("#gift-list .list-item:lt(2)").each().wrapAll('<div class="gift_list_inner_wrapper" name="0" />');

    var divs = $(".list-item");
    var  listNum = 0;
    for(var i = 0; i <= divs.length; i+=6) {
       // console.log(i+" ");
        if(i<divs.length){
            divs.slice(i, i+6).wrapAll('<div class="gift_list_inner_wrapper"  />').parent().attr("name",i).attr("id","gift-list-"+i);
        }else{
            divs.slice(i, divs.length).wrapAll('<div class="gift_list_inner_wrapper"  />').parent().attr("name",i).attr("id","gift-list-"+i);

        }

    }

    $.fn.startUpGallery2 = function(currentPos) {
        $("#gift-list #gift-list-0").show();

    };

    var slidePos = 0;
    $( "#gift-list-outter-2 #arrow_right" ).click(function() {
        var slideCount = divs.length-6;
        $( "#gift-list-outter-2 #arrow_left").show();

        if(slidePos<slideCount){
           $("#gift-list #gift-list-"+slidePos).hide();

            slidePos +=6;

            $( "#gift-list #gift-list-"+slidePos ).fadeIn( "slow", function() {
                // Animation complete
            });
            if(slidePos>=slideCount){
                $("#arrow_right").hide();
                $("#view-more-arrow").hide();

            }
        }

        if(slidePos>=slideCount){
            console.log('here'+'slide '+slidePos+' '+slideCount);
           // $("#arrow_right").hide();

        }
    });

    $( "#gift-list-outter-2 #arrow_left" ).click(function() {
        var slideCount = divs.length-6;
       // console.log(slidePos+" ");
        $( "#gift-list-outter-2 #arrow_right").show();
        console.log('slide '+slidePos+' '+slideCount);

        if(slidePos>0){
            $("#view-more-arrow").show();

            $("#gift-list #gift-list-"+slidePos).hide();

            slidePos -=6;

            $( "#gift-list #gift-list-"+slidePos ).fadeIn( "slow", function() {
                // Animation complete
            });
        }else{
            console.log('hide slide '+slidePos+' '+slideCount);

            // console.log(this);
            $("#arrow_left").hide();

        }
    });
    /**end of fix 2015**/
   /* $.fn.startUpGallery = function(currentPos) {
        var currentPosInt = parseInt(currentPos);
        //console.log("currentpos "+currentPos+", lastPos "+lastPageStartPos);
        if(currentPosInt>lastPageStartPos)
        {
            return;
        }else if(currentPosInt==0){//first slide
            $("#arrow_left").hide();
            return;

        }else if(currentPosInt==lastPageStartPos){//if on last slide, then show left arrow
             $("#arrow_right").hide();
            $("#arrow_left").show();

            //hide view more items arrow if on last slide at startup
            $("#view-more-arrow").hide();
        }else{//show both arrows
            $("#arrow_right").show();
            $("#arrow_left").show();
        }
        pos=currentPosInt;
        var a="#gift-list-"+pos;
        $(a).show();
        $("#gift-list-0").hide();


        *//*
        $(a).fadeTo(5000,1,function(){

             $(a).fadeOut(500, function () {
                pos+=interval;
                a="#gift-list-"+pos;
                $(a).fadeIn(500, function(){
                    document.getElementById("arrow_left").src=root+"images/arrow_left.png";
                    $(a).fadeTo(5000,1,function(){
                         $(a).fadeOut(500, function () {
                            pos+=interval;
                            a="#gift-list-"+pos;
                            $(a).fadeIn(500);
                         });
                    });
                });
             });
        });*//*
    }


    function startSlide(b){

        if(document.all){b=event;}
        if(this.id=="arrow_right"||this.id=="nextItems"){
            //remove the view more items image if the right arrow is clicked:
            $("#view-more-arrow").hide();
            if(pos<lastPageStartPos){
                var a="#gift-list-"+pos;
                *//*
                $(a).hide();
                pos+=5;
                a="#gift-list-"+pos;
                $(a).fadein("fast");
                *//*
                 $(a).fadeOut(100, function () {
                    pos+=interval;
                    a="#gift-list-"+pos;
                    $(a).fadeIn(100);
                    $("#arrow_left").show();
                    //document.getElementById("arrow_left").src=root+"images/arrow_left.png";

              });

            }
        }else{
            if(pos>0)
            {
                var a="#gift-list-"+pos;

                $(a).fadeOut(100, function () {
                    pos-=interval;
                    a="#gift-list-"+pos;
                    $(a).fadeIn(100);
                     });
                $("#arrow_right").show();
                //document.getElementById("arrow_right").src=root+"images/arrow_right.png";
            }
        }
    }
    function highlightArrow(a){
        var b=this.id;

        if(b=="arrow_right")
        {
            if(pos>=lastPageStartPos)//if has reached last  page of item list (starting postion of last page is 10)
            {
                 document.getElementById("arrow_right").src=root+"images/arrow_right_bw.png";

            }
            else
            {
                this.getElementsByTagName("IMG")[0].src=root+"images/"+this.id+"_white.png";
            }
        }else{
            if(pos==0)//if has reached last  page of item list (starting postion of last page is 10)
            {
                 document.getElementById("arrow_left").src=root+"images/arrow_left_bw.png";
            }
            else
            {
                this.getElementsByTagName("IMG")[0].src=root+"images/"+this.id+"_white.png";
            }
        }
    }*/
    function highlightText(){this.style.color="#666";}
    function unhighlightText(){this.style.color="#e71849";}
/*
    function releaseSlide(){
        var a=this.id;
        if(a=="arrow_right")
        {
            if(pos>=lastPageStartPos)//if has reached last  page of item list (starting postion of last page is 10)
            {
                 //document.getElementById("arrow_right").src=root+"images/arrow_right_bw.png";
                  $("#arrow_right").hide();
            }
            else
            {
                //this.getElementsByTagName("IMG")[0].src=root+"images/"+this.id+".png";
            }
        }else{
            if(pos==0)//if has reached last  page of item list (starting postion of last page is 10)
            {
                 //document.getElementById("arrow_left").src=root+"images/arrow_left_bw.png";
                  $("#arrow_left").hide();
            }
            else
            {
                //this.getElementsByTagName("IMG")[0].src=root+"images/"+this.id+".png";

            }
        }

    }
*/

/*    $(function(){
               document.getElementById("arrow_left").onclick=startSlide;
               document.getElementById("arrow_left").onmouseout=releaseSlide;
               //document.getElementById("arrow_left").onmouseover=highlightArrow;
               document.getElementById("arrow_right").onclick=startSlide;
               document.getElementById("arrow_right").onmouseout=releaseSlide;
               //document.getElementById("arrow_right").onmouseover=highlightArrow;
             *//*  document.getElementById("nextItems").onclick=startSlide;
               document.getElementById("prevItems").onclick=startSlide;
               document.getElementById("nextItems").onmouseover=highlightText;
               document.getElementById("prevItems").onmouseover=highlightText;
               document.getElementById("nextItems").onmouseout=unhighlightText;
               document.getElementById("prevItems").onmouseout=unhighlightText;*//*
               itemStrip=document.getElementById("gift_list_inner_wrapper");
               // $("#arrow_left").hide();
               }
    );*/

    $.fn.purchase = function() {
         if($("#txtName").val().length<2){
            $("#txtName").addClass("error");
            return false;
        }else{
            $("#txtName").removeClass("error");
        }
       
        if($("#radVisa:checked").length>0||$("#radPayPal:checked").length>0||$("#radCheque:checked").length>0){
            //$("#paypal_email").val("phizzy_1281139283_biz@hotmail.com");
            $("#paypal_email").val("onemorestepvn@gmail.com");
            results = $("#btnPurchase").validateGiftBag({
                    lang:'<?php echo qtranxf_getLanguage();?>',
                    purchaserName:$("#txtName").val(),
                    purchaserEmail:$("#txtEmail").val(),
                    list_name:$("#cbConsent:checked").length>0?1:0
                  });
            if(parseInt(results)){ //if id of purchase is returned
                if($("#radCheque:checked").length>0){
                    $("#cbPurchaseID").val(results);
                    $("#frmCheque").submit();Han
                }else{//paypal payment
                    $("#paypal_return").val("http://onemorestep.ca/thank-you");
                    $("#paypal_cancel_return").val(""+window.location);
                    $("#paypal_notify_url").val("http://onemorestep.ca/?paypalListener=IPN");
                    $("#purchaseID").val(results);
                    $("#frmPaypal").submit();
                }

            }else{
                if($("#GiftBagSummary").find(".error").length){
                    $("#GiftBagSummary").find(".error").html(results);
                }else{
                    $("#GiftBagSummary").prepend('<div class="error">'+results+'</div>');
                }
                return false;
            }
             $("#paymentOption").removeClass("error");
        }else{
            $("#paymentOption").addClass("error");return false;
        }

        return true;
    }

    /*
     *Getting Tweets from Twitter.  Using a proxy file to make sure retweets show up
     */

  //  var tweetData= $('#image_real .tweets').each();
    //console.log($('#image_real').html());
    //$('#tweet0').prevAll().remove();
    //$('#image_real').text(tweetData);
    $.fn.printTweets = function(options) {
        var defaults = {
            twitterName: "mediacoAV",	//the Twitter user name
            rotationInterval: 10000,	//inverval between each tweet display
            rotateSpeed:500,
            numOfTweets:4,
            direction:"up",
            easing:"easeOutElastic",
            width:$(this).width(),
            height:$(this).height(),
            currentSlide:0
        };


        var options = $.extend(defaults, options);
        var currentVisibleTweet=0;
        var feedURL = "";
        $(this).css({overflow:'hidden', position:'relative'}); //make sure the target div acts as a viewing screen
        //$(this).append('<div id="image_real"></div>');
        $image_real=$("#image_real"); //the div that contains the strip of element
        $image_real.css({position:'absolute',top:0,left:0});


        $.get(feedURL, function(response){
           // showTweets(response);
            rotateSwitch("#image_real", "#image_real div",options.numOfTweets,options.height,options.rotationInterval, options.rotateSpeed, options.direction, options.easing, options.currentSlide); //Run function on launch

        });
        function showTweets(tweets){
            var tweetID=0;

            $(tweets).find('item').each(function() {
                var tweet = $(this);
                var pattern = new RegExp("^"+options.twitterName+": ","g");
                var status = tweet.find('description').text().replace(/((https?|s?ftp|ssh)\:\/\/[^"\s\<\>]*[^.,;'">\:\s\<\>\)\]\!])/g, function(url) {
                    //return '<a href="'+url+'">'+url+'</a>';
                    return url;
                }).replace(/\B@([_a-z0-9]+)/ig, function(reply) {
                        return reply;
                        //return  reply.charAt(0)+'<a href="http://twitter.com/'+reply.substring(1)+'">'+reply.substring(1)+'</a>';
                    }).replace(pattern,'');
                var tweetDate = relative_time(tweet.find('pubDate').text());
                $image_real.append('<div class="tweets" id="tweet'+tweetID+'"><span class="name">ASAAP: </span>&ldquo;'+status+'&rdquo; <small>- <span class="date">'+tweetDate+'</span></small></div>');
                tweetID++;
            });//end of $(tweets).find
            $image_real.find('.tweets').css({width:options.width,height:options.height,display:'block', overflow:'hidden'});
            $("#tweet0").addClass("active"); //make the first element active for the rotation
        };//end of $showTweets
    };//end printTweet function

    //Paging  and Slider Function
    rotate = function(options){
        var defaults = {
            objectName: "#image_real",
            pagingName: "#paging a",
            slideDistance:1000,
            speed:3000,
            slideNum:5,
            direction:"left",
            easing:"easeOutElastic",
            activeElement:''
        };
        var options = $.extend(defaults, options);

        var image_reelPosition = options.slideNum * options.slideDistance; //Determines the distance the image reel needs to slide

        $(options.pagingName).removeClass('active'); //Remove all active class
        if(options.activeElement!="")
        {
            $(options.activeElement).addClass('active'); //Add active class (the $active is declared in the rotateSwitch function)
        }
        //Slider Animation

        switch(options.direction)
        {
            case "up":
                $(options.objectName).animate({top: -image_reelPosition},  options.speed);
                break
            case "left":
                $(options.objectName).animate({left:-image_reelPosition}, options.speed);
                break;
            case "right":
                $(options.objectName).animate({left:+image_reelPosition}, options.speed);
                break;
            case "down":
                $(options.objectName).animate({top:+image_reelPosition}, options.speed);
                break;
            default:
                $(options.objectName).animate({left:-image_reelPosition}, options.speed);
                break;

        }


    };
    /****Rotation  and Timing Event
     *@objectName the ID of the element (image_reel) containing all the slides
     *@pagingName the ID of the element containing each slide
     *@numOfSlides the count of how many slides are in the reel
     *@slideDistance the length each slide (ie the width of the viewing window)
     *@interval the interval by which the slides are toggled
     *@speed the animation speed of the slide change
     *@direction direction of the animation
     */
    rotateSwitch =  function(objectName, pagingName, numOfSlides, slideDistance, interval, speed, direction, easing, current){
        currentSlide=current;
        var play = setInterval(function(){ //Set timer - this will repeat itself every 7 seconds
            $active = $(pagingName+'.active').next(); //Move to the next paging
            if ( $active.length === 0) { //If paging reaches the end...
                $active = $(pagingName+':first'); //go back to first
                currentSlide=0;
            }
            else
            {
                currentSlide++;
            }
            rotate({
                objectName: objectName,
                pagingName: pagingName,
                slideDistance:slideDistance,
                speed:speed,
                slideNum:currentSlide,
                direction:direction,
                easing:"easeOutElastic",
                activeElement:$active}) //Trigger the paging and slider function
        }, interval); //Timer speed in milliseconds (7 seconds)
        return play;


    };

});

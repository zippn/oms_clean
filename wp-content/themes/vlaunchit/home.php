

<?php get_template_part('header')?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=171453632872821";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<script type="text/javascript">


    var $j = jQuery.noConflict();

    $j(document).ready(function($) {


        function replaceURLWithHTMLLinks(text) {
            var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
            var exp2 = /(@takeonemorestep)/ig;
            text = text.replace(exp,"<a href='$1'>$1</a>");
            text = text.replace(exp2,"<a href='https://twitter.com/takeonemorestep' target='_blank' class='name'>$1</a>");
            return text;
        }
        //THIS IS A JQUERY STATEMENT THAT GRABS A CHUNK OF
        //TEXT AND REPLACES IT WITH THE UPDATED STRING
        $(".tweets").each(function(i){
            var text = $(this).html();
            $(this).html(replaceURLWithHTMLLinks(text));
        });

        hashtag_regexp = /#([a-zA-Z0-9]+)/g;

        function linkHashtags(text) {
            return text.replace(
                hashtag_regexp,
                '<a class="hashtag" href="https://twitter.com/search?q=%23$1&src=hash">#$1</a>'
            );
        }
        $('.tweets').each(function() {
            $(this).html(linkHashtags($(this).html()));
        });

        //  $("#sidebar-twitter").css({height:$('.active').height()+20});
        $("#sidebar-twitter").printTweets({twitterName:'takeonemorestep', numOfTweets:10, height:$("#sidebar-twitter").height(), rotationInterval:6000});
        //  $("#sidebar-twitter").rotateSwitch();
    });

</script>

<?php query_posts('cat=3&posts_per_page=1');?>
<div class="left_column float_left" >
    <?php
        //query_posts('cat=3');
        if (have_posts()) : while (have_posts()) : the_post(); ?>

            <h1><?php the_title(); ?></h1>
            <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                  the_post_thumbnail('home-page');
                } ?>
            <div class="post" id="post-<?php the_ID(); ?>">
                <div class="entry">
                    <?php the_content(); ?>
                </div><!--entry-->
            </div><!--post-->
   <?php endwhile; endif; ?>
</div>
 <div class="right_column float_right ">
<!--     <h2>-->
<!--         Follow Us On Twitter-->
<!--     </h2>-->
<!--        <div id="the-twitter">-->
<!---->
<!--            <div id="sidebar-twitter">-->
<!--                <div id="image_real" >-->
<!--                    --><?php
//                    //error_reporting(0);
//
//                    $tweet_user="takeonemorestep";
//                    $tweet_id=0;
//                    $tweets = getTweets(10, $tweet_user);
//
//                    foreach($tweets as $tweet){
//                        if($tweet_id==0){
//                            $active_class = "active";
//                        }else{
//                            $active_class = "";
//                        }
//                        ?>
<!---->
<!--                            <div class="tweets --><?php //echo $active_class;?><!--" id="tweet--><?php //echo $tweet_id;?><!--">@--><?php //echo $tweet_user;?><!--: --><?php //echo $tweet["text"];?><!--<small>- <span class="date">--><?php //echo $tweet["created_at"];?><!--</span></small></div>-->
<!--                        --><?php
//                        $tweet_id++;
//                    }
//                    //error_reporting(-1);
//
//                    ?>
<!--                </div>-->
<!---->
<!--            </div>-->
<!---->
<!--        </div>-->

        <div class="facebook-wrap news">
            <div class="facebook">
                <h2>Follow Us On Facebook</h2>
                <div class="fb-like-box" data-href="https://www.facebook.com/TakeOneMoreStep" data-width="285" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="true" data-show-border="true"></div>
            </div>
        </div>

</div><!--right_column-->



<?php get_template_part('footer')?>


<?php
/**
 * @package WordPress
 * @subpackage oobs
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php bloginfo('name'); ?> <?php wp_title('&raquo;', true, 'left'); ?> </title>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>?ver=1.5" type="text/css" media="screen" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" />
<!-- added to handle PNG transparency -->
<!--[if lte IE 8]>
<style>
</style>
<![endif]-->
<!--[if IE 6]>
<style>

</style>
<![endif]-->

    <script src="https://www.paypalobjects.com/api/checkout.js" data-version-4></script>
<?php wp_head(); ?>
    <!--<script type="text/javascript" src="<?php /*bloginfo('stylesheet_directory');*/?>/js/common_jquery.js"></script>-->
<script type="text/javascript">
    var $j = jQuery.noConflict();
    $j(document).ready(function($) {
        //$("#content_wrapper_outer").corner("10px");
        //$(".item").corner("8px");
        <?php if ( 'item' == get_post_type() ):?>

        <?php else:?>
            $("#img_left_arrow").hide();
        <?php endif;?>
        $("#gift-list").startUpGallery2(0);

        $("#progressbar").progressBar(<?php echo 100*intval(get_post_meta(HOME2011_PAGE_ID,'gift_amount_raised',true))/intval(GIFT_DRIVE_FUNDRAISING_TARGET);?>,{ width:140, boxImage:'<?php bloginfo('stylesheet_directory');?>/images/progressbar.gif', barImage: '<?php bloginfo('stylesheet_directory');?>/images/progressbar/progressbg_red.gif' });
        $("#education_progressbar").progressBar(<?php echo 100*intval(get_post_meta(HOME2011_PAGE_ID,'education_amount_raised',true))/intval(SCHOLARSHIP_FUNDRAISING_TARGET);?>,{ width:140, boxImage:'<?php bloginfo('stylesheet_directory');?>/images/progressbar.gif', barImage: '<?php bloginfo('stylesheet_directory');?>/images/progressbar/progressbg_green.gif' });

        $("#gift-receipient #gift-receipient-list a.sidebar-level1").click(function(e){
            e.preventDefault();
                if($("#gift-receipient-list #receipient-list").hasClass('hide')){
                    $("#gift-receipient-list #receipient-list").removeClass('hide');

                }else{
                    $("#gift-receipient-list #receipient-list").addClass('hide');

                }
        });
    });
</script>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-56932547-1', 'auto');
    ga('send', 'pageview');

</script>
<!--
    <script>
        paypal.Button.render({

            env: 'sandbox', // Specify 'sandbox' for the test environment

            client: {
                sandbox:    'botphong@hotmail.com',
                production: 'botphong@hotmail.com'
            },

            payment: function() {

                var env    = this.props.env;
                var client = this.props.client;

                return paypal.rest.payment.create(env, client, {
                    transactions: [
                        {
                            amount: { total: '1.00', currency: 'USD' }
                        }
                    ]
                });
            },

            commit: true, // Optional: show a 'Pay Now' button in the checkout flow

            onAuthorize: function(data, actions) {

                // Optional: display a confirmation page here

                return actions.payment.execute().then(function() {
                    // Show a success page to the buyer
                });
            }

        }, '#paypal-button');

    </script>-->
<!--[if IE]>
<![endif]-->
</head>
<body bgcolor="#FFFFFF" text="#000000">
<div id="outer_wrapper">
	<div id="inner_wrapper">
        <div id="header_wrapper">
            <div id="bow_wrapper">
                <span id="Bow"><a href="<?php bloginfo('url');?>"><img src="<?php bloginfo('stylesheet_directory');?>/images/logo-white.png" border="0" /></a></span>
            </div>
            <div id="header-right">
               
                <div id="top_menu">
                    <div class="float_left">
                        <a href="<?php bloginfo('url');?>"><?php _e("[:en]Home[:vi]TRANG CHÍNH"); ?></a>|<a href="<?php echo get_permalink(15);?>"><?php echo get_the_title(15); ?></a>|<a href="<?php echo get_permalink(17);?>"><?php echo get_the_title(17); ?></a>|<a href="<?php echo get_permalink(19);?>"><?php echo get_the_title(19); ?></a>|<a href="<?php echo get_permalink(21);?>"><?php echo get_the_title(21); ?></a>|<a href="<?php echo get_permalink(23);?>"><?php echo get_the_title(23); ?></a>|<a href="<?php echo get_permalink(25);?>"><?php echo get_the_title(25); ?></a>

                    </div>
                    <div id="small_menu" class="float_right">

                             <span class="small_menu_item" >
                                 <?php if (is_home()&&qtranxf_getLanguage() == 'vi'){
                                    echo '<a href="'.get_bloginfo('url').'?lang=en">English</a>';
                                 }elseif(is_home()&&qtranxf_getLanguage() == 'en'){
                                    echo '<a href="'.get_bloginfo('url').'?lang=vi">Tiếng Việt</a>';
                                 }elseif(qtranxf_getLanguage() == 'vi'){
                                    echo '<a href="'.get_permalink($post->ID).'?lang=en">English</a>';
                                }else {
                                    echo '<a href="'.get_permalink($post->ID).'?lang=vi">Tiếng Việt</a>';
                        
                                } ?></span>

                        </div>
                </div>
             	

		        <div id="gift-list-outter-2">
        		    <?php do_shortcode('[oms-shopping-list]'); ?>
                    <img src="<?php bloginfo('stylesheet_directory');?>/images/view_more.png" id="view-more-arrow">
                </div>


             </div>
        </div>

        <div id="body_wrapper_outer">
        	<div id="body_wrapper_inner">
                <div id="content_wrapper_outer">
                	<div id="content_wrapper_inner">



<?php
/*
 * Template Name: Orphanage Profile
 * @package WordPress
 * @subpackage citi
 */?>

<?php get_template_part('header')?>
<div id="content-inner-wrap" class="orphange-profile">
     <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <h1><?php the_title(); ?></h1>
            <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                  the_post_thumbnail('large');
                } ?>
            <div class="post" id="post-<?php the_ID(); ?>">
                <div class="entry">

                    <?php the_content(); ?>
                </div><!--entry-->
            </div><!--post-->
       <?php endwhile; endif; ?>
</div><!--cs wrap-->

<?php get_template_part('footer')?>


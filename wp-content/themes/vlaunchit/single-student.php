<?php get_template_part('header')?>
<div id="content-inner-wrap">
     <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <h1><?php the_title(); ?></h1>
        <div class="post" id="post-<?php the_ID(); ?>">
            <div class="entry">
                <div class="content-left-side">
                    <div class="featured"><?php the_post_thumbnail('large');?></div>
                    <ul class="student-profile">
                        <li><span class="label"><?php _e("[:en]Ethnicity[:vi]Dân tộc");?></span> : <?php the_field('tribe');?></li>
                        <!--<li><span class="label"><?php /*_e("[:en]Region[:vi]Tỉnh");*/?></span> : <?php /*the_field('region');*/?></li>-->
                        <li><span class="label"><?php _e("[:en]Program Duration[:vi]Đang học năm");?></span> : <?php the_field('year_of_study');?></li>
                        <li><span class="label"><?php _e("[:en]Field of Study[:vi]Ngành Học");?></span> : <?php echo get_post_meta(get_the_ID(),'field_of_study',true);?></li>
                        <!--<span class="label"><?php /*_e("[:en]Left[:vi]Còn");*/?></span> : --><?php /*the_field('status');*/?>
                        <li><span class="label"><?php _e("[:en]School Years Receiving Scholarship[:vi]Những năm được học bổng");?></span> : <?php the_field('years_received_scholarship');?></li>
                        <?php $field = get_field_object('status');
                            $value = get_field('status');
                            $label = $field['choices'][ $value ];
                        ?>
                        <li><span class="label"><?php _e("[:en]Current Status[:vi]Tình Trạng Hiện Tại");?></span> : <?php echo $label;?></li>
                    </ul>
                    <?php the_content(); ?>
                </div>
                <?php get_template_part("sidebar","students");?>
            </div><!--entry-->
        </div><!--post-->
     <?php endwhile; endif; ?>
</div><!--cs wrap-->

<?php get_template_part('footer')?>



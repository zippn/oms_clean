<?php
/**
 * @package WordPress
 * @subpackage vlaunchit
 */
/**Global vars*/
define("HOME2011_PAGE_ID",13);
define("ALBUM_PAGE_ID",19);
define("SCHOLARSHIP_PAGE_ID",625); //LIVE 625, local 553
define("SCHOLARSHIP_STUDENT_PROFILES",642);
define("SCHOLARSHIP_DONATE",163);
define("THE_TEAM_PAGE_ID",25);
define("PAST_DRIVE_PAGE_ID",261);
define("GIFT_DRIVE_FUNDRAISING_TARGET", get_post_meta(HOME2011_PAGE_ID,'gift_target',true));
define("SCHOLARSHIP_FUNDRAISING_TARGET", get_post_meta(HOME2011_PAGE_ID,'education_target',true));
define("GIFT_DRIVE_END_DATE",get_post_meta(HOME2011_PAGE_ID,'gift_end_date',true));
define("SCHOLARSHIP_END_DATE",get_post_meta(HOME2011_PAGE_ID,'education_end_date',true));

/**Process Checkout Forms*/

/*Empty basket*/
/*remove item from basket*/
function empty_gift_bag() {

	try{
		session_unset('basket');
	}catch(Exception $e){
	}
}


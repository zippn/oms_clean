<?
$album_sel = (strtolower($_GET['location']) == 'thien-binh') ? 'thien-binh' : 'nam-hung';

$album_info = array(
	'nam-hung' 		=> array(
							'album_count'	=> 54,
							'imagepath' 	=> 'Nam_Hung',
							'imagename'		=> 'nh-P1020969-',
							'bgcolor'		=> '#FF833F'
						),

	'thien-binh' 	=> array(
							'album_count'	=> 56,
							'imagepath' 	=> 'Thien_Binh',
							'imagename'		=> 'tb-P1020993-',
							'bgcolor'		=> '#67B7FB'
						)
);
$album_info = $album_info[$album_sel];


$album_desc = array(
	'nam-hung-en' 	=> "['Each of these bags contains goodies for each child.  A bag consists of a few cakes, a box of milk, and some candies.', '', 'Piles of food, cooking supplies, toys, diapers and sports balls piled up and ready to be brought to the orphanages.', 'Two new bicycles graciously purchased by One More Step donors. Fitted with baskets to help older students pick up supplies throughout the year.', '', '', 'An example of a gift kit for one child including: some toys, supplies for school, and a bag of treats.', 'Boxes of instant noodles', 'All 14 of the mosquito nets', 'Kitchen and cleaning supplies from your donations allow a consistent source of tasty meals and a hygenic environment.', 'Rice in Vietnamese is G?o.  Thanks to those who contribute to this large supply for a large group of hungry children!(Each bag is 5kg)', 'These formula and diapers were purchsed as needed supplies for infants.', 'These are pencil cases', '', '', 'Kitchen sets, infant toys, dolls and action figures for kids of different ages.', 'If you bought these board games, thanks very much! ', 'Supplies arriving at the Nam Hung orphanage.', '', '', '', 'Some existing play equipment which had been supplied to the orphanage.', '', '', 'Young children greet those bring supplies as they arrive at Nam Hung.', '', '', '', 'Giving away treats and gifts.', '', '', '', 'Group picture with orphanage children and some of their new supplies and toys.', '', '', 'One child in ethnic clothing, showing off this new toy.', 'Uyen, one of our volunteers in Vietnam, poses with one of the nuns', '', '', '', 'Playing with the new toys in the courtyard.', 'Vu, another volunteer, with one of the children', '', 'Basic sleeping area at the orphanage.', 'This is the kitchen where new the rice, meat and cooking supplies will be used with fresh vegetables from the region.', 'The Christmas spirit can be seen with this display of the manger', 'Kids enjoying their new soccer balls.', '', '', 'The children of different ages live very closely, and help each other with they toys.', '', '', '', '']",
	'nam-hung-vn' 	=> "['Mỗi bao là quà bánh kẹo cho mỗi em.  Trong mỗi bao có vài cái bánh bông lan, một hộp sữa, và kẹo.', '', 'Mì gói, nước mắm, bánh, kẹo, trái banh, gạo, tả, đồ chơi đang được chuẩn bị mang ra xe.', 'Hai chiếc xe đạp xinh xinh này sẽ được các em dùng hàng ngày để đi học và đi chợ.  ', '', 'Mọi việc đã được sắp xếp chu đáo, bây giờ chỉ có xếp vào xe và chuẩn bị lên đường thôi :)', 'Vài món đồ chơi nho nhỏ cho các em.', 'Các thùng mì gói Gấu đỏ mới được mua về.', '', 'Nước mắm, xà bông giặt đồ, gạo', 'Nhờ vào số tiền đóng góp của tất cả quỶ ân nhân, nhóm đã có đủ tiền để mua những bao gạo thơm ngon đến cho các em.', 'Mấy lon sữa này sẽ giúp cho các em có đủ chất bổ dưỡng cho cơ thể.', 'Nhóm đã mua rất nhiều dụng cụ học sinh cho các em để giúp các em có những món cần thiết để các em có thể tiếp tục đi học.', '', '', '', '', 'Xe đang bon bon chạy trên con hẻm dẫn đến Giáo Xứ Nam Hưng.', 'Các em đang phấn khởi ra tiếp đón nhóm Tiến Bước.', 'Thành viên của nhóm Tiến Bước đang khệ nệ mang quà vào cho các em.', 'Các em nhỏ đang mở rộng cửa đón chào nhóm Tiến Bước.', 'Đây là sân chơi của các em.  Trong giờ nghỉ giải lao, ngoài chiếc cầu tuộc này, các em không có các món đồ chơi nào khác để giải trị', 'Soeurs trong cô nhi viện dẫn các em ra chào đón phái đoàn.', 'Sau khi phânh phát quà cho các em thành viên trong nhóm Tiến Bước chúc các em một mùa Giáng Sinh vui vẻ.', 'Các em nhỏ đang lắng tay nghe lời chúc Giáng Sinh và đang nôn nóng được trở vào nhà để chơi với các món quà của mình.', 'Một vài em lớn đứng ra chúc Giáng Sinh và tỏ lòng cám ơn đến tất cả các quỶ ân nhân đã giúp đỡ Nhóm Tiến Bước thực hiện được nhiệm vụ này thành công một cách mỹ mãng.', 'Thành viên trong nhóm Tiến Bước chụp vài hình với các em để làm kỷ niệm.  Đây là những giây phút vui vẻ nhất.', '', '', 'Các món quà nho nhỏ nhưng chứa đầy tình thương đang được phân phát ra cho các em.', 'Mọi người tụ họp lại nói cười vui vẻ', 'Các em nhỏ đang giơ tay nhận lấy món quà của mình.', 'Mọi người đang hô to \"Cám ơn\"', '', '', '', '', 'Các em đang hớn hở mở gói quà của mình ra và khoe với các  bạn.', '', '', '', '', '', '', 'Đây là nhà bếp.', 'Nhân dịp Giáng Sinh, các em đã cùng nhau xây hang Chúa đơn sơ này dùng làm nơi cầu nguyện.', '', 'Em bé trai này đang thử món đồ chơi mới của mình.', 'Em nào cũng bận bịu với món đồ chơi mới có.', '', '', '', '', 'Thành viên nhóm Tiến Bước đang tập hát cho em bé trai nàỵ']",

	'thien-binh-en'	=> "['', 'Supplies arriving at the Thien Binh orphanage.', 'Our rental van loaded with supplies and gifts', '', 'Large bags of meat are enough to provide the children at Thien Binh hearty and delicious meals this Christmas', '', '', 'Unloading supplies', 'One of the kids gives the sign to the camera as the van arrives with supplies.', '', 'A group picture of the boys with their new soccer balls.', '', 'Kids lining up to receive their presents', '', 'Children greeting their guests.', 'And waiting in line for toys and treats.', '', '', '', '', '', 'Kids check out their new goods.', '', '', '', '', '', '', '', 'Handing out toys and school supplies, the kids inspect.', '', '', '', 'Children and youth of all ages are provided with schooling and community at Tien Binh.', '', '', 'Boxes and piles of food and kitchen supplies have been delivered to Thien Binh.', 'Children receiving gifts bought buy you!', '', 'The Sisters of Thien Binh are pictured with our volunteers', '', 'Courtyard at the Thien Bing orphanage.', '', '', 'The play area of infants, which is in poor condition', '', '', '', 'The formula provides a healthier start where infants don\'t have mothers to provide.', '', '', '', '', '', 'This orphanage has a few young infants.', '']",
	'thien-binh-vn' => "['Trên đường đi đến Cô Nhi Viện Thiên Bình.', 'Cửa đã mở sẵn sàng để chào đón phái đoàn.', 'Chiếc xe van đang chở phái đoàn của nhóm Tiến Bước đến Cô Nhi Viện Thiên Bình.', '', 'Đây là những bao thịt.  Giáng Sinh năm nay các em sẽ có những bữa ăn đầy đủ', 'Mọi người đang chuẩn bị mang quà ra.', '', '', 'Ông \"lơ xe\" tí hon đang đứng chụp hình trước xe.', 'Các em khác tụ họp lại để chụp vài tấm làm kỷ niệm nhưng vì đang vui quá, cho nên em nào cũng lo ra, đang chăm chú nhìn những món quà mang đến.', 'Nhóm Tiến Bước cũng bắt chước đến chụp ké vài tấm hình với các em.  Các em trai đang ôm lấy mấy chiếc bóng mới nhận được.', 'Các em nhỏ lễ phép xếp hàng nhận quà.  Tuy cơ hội đến trường rất ít, nhưng các em đã được các soeurs nuôi dạy rất kỹ lưỡng cho nên em nào cũng rất lễ phép.', 'Một thành viên trong nhóm đang chúc Giáng Sinh và chúc các em chóng lớn, học giỏi và hạnh phúc.', '', 'Cô nhi viện Thiên Bình ở xa thành phố hơn và có nhiều em nhỏ có khuyết tật hơn cô nhi viện ở Nam Hưng.', 'Một bé trai lớn đã đại diện các em, đứng ra cám ơn nhóm Tiến Bước và các quỶ ân nhân đã giúp đỡ các em.  Sau khi cám ơn xong, các em khác hát to một bài để tỏ lòng biết ơn.', 'Từng em một nề nếp tiến lên để nhận món quà của mình.', 'Nhìn các em cười vui vẻ đã làm nhóm rất cảm động.', 'Mọi người đều vui vẻ nhận lấy quà của mình.', 'Thấy các em vui vẻ làm nhóm cũng vui lâỵ', '', '', '', '', '', '', '', '', 'Hai chiếc xe đạp xinh xắn này sẽ giúp đỡ cho các em rất nhiều trong việc đi lại hằng ngày, như đi học và đi chợ.', '', '', 'Các em nhỏ đang hồi họp chờ đến phiên mình.', '', '', '', 'Các thùng mì gói, nước mắm, gạo, tả giao lại cho các soeurs bảo quản.', 'Các em trai đang hớn hở vì thấy mấy chiếc bóng to.  ', '', '', 'Tất cả mọi người cười to lên nghe :)', '', '', 'Vài em nhỏ dẫn nhóm đi vòng vòng thăm cô nhi viện.', 'Đây là nhà thờ Thiên Bình, nơi các em đến cầu nguyện hàng ngàỵ', 'Các em bé này đòi được thay vào những chiếc tả mới ngay sau khi nhận quà của mình.  Các em rất thích và cảm thấy thoải mái.', 'Một bà soeur đang thay tả mới cho bé ', 'Các em nhớ uống sữa cho nhiều chóng lớn.', 'Những hộp sữa này sẽ giúp cho các em có được những chất bổ cần thiết cho cơ thể và bù đắp lại những thiếu thốn hàng ngày', '', '', '', '', '', 'Hai em đang quét dọn nhà thờ.', 'Những chiếc tả mới và vài thùng sữa ngon này sẽ giúp cho bé mau lớn và khỏe mạnh.', 'Soeur ở cô nhi viện Thiên Bình đang trao đổi vào lời với thành viên của nhóm, và nhờ gửi lời cám ơn đến tất cả các ân nhân đã giúp đỡ cho các em ở cô nhi viện nàỵ']"
);
$album_desc = $album_desc["$album_sel-$lang_sel"];



$album = array();
$album['vn'] = array(
	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',


	);


$album['en'] = array(
	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',


	);



?>
<?php

include('./album-new-data.php');
?>
	<!-- Start Advanced Gallery Html Containers -->
<div id="gallery" class="content">
    <div id="controls" class="controls"></div>
    <div class="slideshow-container">
        <div id="loading" class="loader"></div>
        <div id="slideshow" class="slideshow"></div>
    </div>
    <div id="caption" class="caption-container"></div>
</div>

<div id="thumbs" class="navigation">
    <ul class="thumbs noscript">

    	<? for ($i=0; $i< $album_info['album_count']; $i++) { ?>
        <li>
            <a class="thumb" name="leaf" href="../<?=$album_info['imagepath']?>/<?=$album_info['imagename']; ?><?=$i; ?>.JPG" title="<?=$album_info['imagename']; ?><?=$i; ?>.JPG">
                <img src="../<?=$album_info['imagepath']?>/thm-<?=$album_info['imagename']; ?><?=$i; ?>.JPG" alt="Title #<?=$i; ?>" />
            </a>
            <div class="caption">
            <!--
                <div class="download">
                    <a href="http://farm4.static.flickr.com/3261/2538183196_8baf9a8015_b.jpg">Download Original</a>
                </div>-->
                <div class="image-title"><?=$album_info['albumtitle'];?></div>
                <p class="regular_content"><?=$album_desc[$i]; ?></p>
            </div>
        </li>
        <? } ?>
    </ul>

</div>
<div style="clear: both;"></div>

<script type="text/javascript">
			jQuery(document).ready(function($) {
					// We only want these styles applied when javascript is enabled
				$('div.navigation').css({'width' : '185px', 'float' : 'left'});
				$('div.content').css('display', 'block');
				// Initially set opacity on thumbs and add
				// additional styling for hover effect on thumbs
				var onMouseOutOpacity = 0.67;
				$('#thumbs ul.thumbs li').opacityrollover({
					mouseOutOpacity:   onMouseOutOpacity,
					mouseOverOpacity:  1.0,
					fadeSpeed:         'fast',
					exemptionSelector: '.selected'
				});

				// Initialize Advanced Galleriffic Gallery
				var gallery = $('#thumbs').galleriffic({
					delay:                     3000,
					numThumbs:                 8,
					preloadAhead:              10,
					enableTopPager:            true,
					enableBottomPager:         true,
					maxPagesToShow:            3,
					imageContainerSel:         '#slideshow',
					controlsContainerSel:      '#controls',
					captionContainerSel:       '#caption',
					loadingContainerSel:       '#loading',
					renderSSControls:          true,
					renderNavControls:         true,
					playLinkText:              'Play Slideshow',
					pauseLinkText:             'Pause Slideshow',
					prevLinkText:              '&lsaquo; Previous Photo',
					nextLinkText:              'Next Photo &rsaquo;',
					nextPageLinkText:          'Next &rsaquo;',
					prevPageLinkText:          '&lsaquo; Prev',
					enableHistory:             true,
					autoStart:                 false,
					syncTransitions:           true,
					defaultTransitionDuration: 900,
					onSlideChange:             function(prevIndex, nextIndex) {
						// 'this' refers to the gallery, which is an extension of $('#thumbs')
						this.find('ul.thumbs').children()
							.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
							.eq(nextIndex).fadeTo('fast', 1.0);
					},
					onPageTransitionOut:       function(callback) {
						this.fadeTo('fast', 0.0, callback);
					},
					onPageTransitionIn:        function() {
						this.fadeTo('fast', 1.0);
					}
				});

				/**** Functions to support integration of galleriffic with the jquery.history plugin ****/

				// PageLoad function
				// This function is called when:
				// 1. after calling $.historyInit();
				// 2. after calling $.historyLoad();
				// 3. after pushing "Go Back" button of a browser
				function pageload(hash) {

					var str_hash = "" + hash;
					if (str_hash.length > 3){
						window.location = './gift#' + str_hash;
						return;
					}

					// alert("pageload: " + hash);
					// hash doesn't contain the first # character.
					if(hash) {
						$.galleriffic.gotoImage(hash);
					} else {
						gallery.gotoIndex(0);
					}
				}

				// Initialize history plugin.
				// The callback is called at once by present location.hash.
				$.historyInit(pageload, "advanced.html");

				// set onlick event for buttons using the jQuery 1.3 live method
				$("a[rel='history']").live('click', function() {
					var hash = this.href;
					hash = hash.replace(/^.*#/, '');

					// moves to a new page.
					// pageload is called at once.
					// hash don't contain "#", "?"
					$.historyLoad(hash);

					return false;
				});


				/****************************************************************************************/
			});
		</script>
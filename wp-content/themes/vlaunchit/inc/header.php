
<html>
<head>
<title>One More Step</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<!-- <link rel="stylesheet" href="./CSS/csslayout.css" type="text/css" />-->

<link rel="stylesheet" href="./CSS/site_layout.css" type="text/css" />
<link rel="stylesheet" href="./CSS/basic.css" type="text/css" />
<link rel="stylesheet" href="./CSS/galleriffic-2.css" type="text/css" />


<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery.ui.all.css" rel="stylesheet" />

<script type="text/javascript" src="./scripts/jsscript.js"></script>
<script type="text/javascript" src="./scripts/src/jquery.galleriffic.js"></script>
<script type="text/javascript" src="./scripts/src/jquery.progressbar.min.js"></script>
<script>
	var lang_sel = "<?=$lang_sel; ?>";

	var image_index = 0;
	var album_count = <?=intval($album_info['album_count']); ?>;
	var last_img = "";

	var album_desc = <?=empty($album_desc) ? "''" : $album_desc; ?>;

	jQuery(document).ready(function() {
			$("#content_wrapper_outer").corner("10px");
			$(".item").corner("8px");
	
            $("#progressbar").progressBar(<?=$totalPercentSold?>,{ width:140, barImage: 'images/progressbar/progressbg_red.gif' });
			$("#education_progressbar").progressBar(<?=$totalEdFundPercentSold?>,{ width:140, barImage: 'images/progressbar/progressbg_green.gif' });
			$("#img_left_arrow").hide();
			$("#gift_list_wrapper").startUpGallery();


									});
</script>

</head>
<body bgcolor="#FFFFFF" text="#000000">
<div id="outer_wrapper">
	<div id="inner_wrapper">
        <div id="header_wrapper">
            <div id="bow_wrapper">
                <span id="Bow"><a href="http://onemorestep.ca"><img src="./images/logo-white.png" border="0" /></a></span>
            </div>
            <div id="header-right">
                <div id="title">
                    
                </div>
                <div id="top_menu">
                    <div class="float_left">
                            <a href="index.php"><?=$lang['Home']; ?></a>|<a href="mission.php"><?=$lang['Mission']; ?></a>|<a href="about.php"><?=$lang['About']; ?></a>|<a href="./album.php?projectName=Christmas 2010&albumName=prep-Christmas2010"><?=$lang['Album']; ?></a>|<a href="finance.php"><?=$lang['Finance']; ?></a>|<a href="donors.php"><?=$lang['Donors']; ?></a>|<a href="contact.php"><?=$lang['Team']; ?></a>		 
                           
                    </div>
                    <div id="small_menu" class="float_right">
                             <span class="small_menu_item" ><? if ($lang_sel == 'vn')
                                {
                                    echo '<a href="javascript:eng();">English</a>';
                                }else {
                                    echo '<a href="javascript:vn();">Tiếng Việt</a>';
                                } ?></span>
                               
                        </div>
                </div>
             	<div id="gift-list-outter"><?php include('gift-list.php');?></div>
             </div>
        </div>
		
        <div id="body_wrapper_outer">
        	<div id="body_wrapper_inner">
                <div id="content_wrapper_outer">
                	<div id="content_wrapper_inner">
<?


$album_sel = $_POST['albumName']; 

$album_info = array(
	'nh-Christmas2008' 		=> array(
							'album_count'	=> 54,
							'imagepath' 	=> './OMS2009/images/album/Christmas2008/Nam_Hung',
							'imagename'		=> 'nh-P1020969-',
							'albumtitle'	=> '2008 - Nam Hưng',
							'bgcolor'		=> '#FF833F'
						),

	'tb-Christmas2008' 	=> array(
							'album_count'	=> 56,
							'imagepath' 	=> './OMS2009/images/album/Christmas2008/Thien_Binh',
							'imagename'		=> 'tb-P1020993-',
							'albumtitle'	=> '2008 - Thiên Bình',
							'bgcolor'		=> '#67B7FB'
						),
	'prep-Christmas2009' 	=> array(
							'album_count'	=> 25,
							'imagepath' 	=> './OMS2009/images/album/Christmas2009/Preparations',
							'imagename'		=> 'pr-Christmas2009-',
							'albumtitle'	=> '2009 - Preparations',
							'bgcolor'		=> '#67B7FB'
						),
	'xt-Christmas2009' 	=> array(
							'album_count'	=> 34,
							'imagepath' 	=> './OMS2009/images/album/Christmas2009/XuanTam',
							'imagename'		=> 'xt-Christmas2009-',
							'albumtitle'	=> '2009 - Xuân Tâm',
							'bgcolor'		=> '#67B7FB'
						),
	'nh-Christmas2009' 	=> array(
							'album_count'	=> 14,
							'imagepath' 	=> './OMS2009/images/album/Christmas2009/NamHung',
							'imagename'		=> 'nh-Christmas2009-',
							'albumtitle'	=> '2009 - Nam Hưng',
							'bgcolor'		=> '#67B7FB'
						),
	'hue-Christmas2009' 	=> array(
							'album_count'	=> 6,
							'imagepath' 	=> './OMS2009/images/album/Christmas2009/Hue',
							'imagename'		=> 'hue-Christmas2009-',
							'albumtitle'	=> '2009 - Hue Schools',
							'bgcolor'		=> '#67B7FB'
						),
	'prep-Christmas2010' 	=> array(
							'album_count'	=> 15,
							'imagepath' 	=> './images/album/Preparation',
							'imagename'		=> 'pr-Christmas2010-',
							'albumtitle'	=> '2010 - Preparations',
							'bgcolor'		=> '#67B7FB'
						),
	'xt1-Christmas2010' 	=> array(
							'album_count'	=> 32,
							'imagepath' 	=> './images/album/Xuan_Tam_1',
							'imagename'		=> 'xt1-Christmas2010-',
							'albumtitle'	=> '2010 - Xuân Tâm Location 1',
							'bgcolor'		=> '#67B7FB'
						),
	'xt2-Christmas2010' 	=> array(
							'album_count'	=> 28,
							'imagepath' 	=> './images/album/Xuan_Tam_2',
							'imagename'		=> 'xt2-Christmas2010-',
							'albumtitle'	=> '2010 - Xuân Tâm Location 2',
							'bgcolor'		=> '#67B7FB'
						),
	'nh-Christmas2010' 	=> array(
							'album_count'	=> 29,
							'imagepath' 	=> './images/album/Nam_Hung',
							'imagename'		=> 'nh-Christmas2010-',
							'albumtitle'	=> '2010 - Nam Hưng',
							'bgcolor'		=> '#67B7FB'
						),
);
$album_info = $album_info[$album_sel];


$album_desc = array(
	'nh-Christmas2008-en' 	=> array('Each of these bags contains goodies for each child.  A bag consists of a few cakes, a box of milk, and some candies.', '', 'Piles of food, cooking supplies, toys, diapers and sports balls piled up and ready to be brought to the orphanages.', 'Two new bicycles graciously purchased by One More Step donors. Fitted with baskets to help older students pick up supplies throughout the year.', '', '', 'An example of a gift kit for one child including: some toys, supplies for school, and a bag of treats.', 'Boxes of instant noodles', 'All 14 of the mosquito nets', 'Kitchen and cleaning supplies from your donations allow a consistent source of tasty meals and a hygenic environment.', 'Rice in Vietnamese is G?o.  Thanks to those who contribute to this large supply for a large group of hungry children!(Each bag is 5kg)', 'These formula and diapers were purchsed as needed supplies for infants.', 'These are pencil cases', '', '', 'Kitchen sets, infant toys, dolls and action figures for kids of different ages.', 'If you bought these board games, thanks very much! ', 'Supplies arriving at the Nam Hung orphanage.', '', '', '', 'Some existing play equipment which had been supplied to the orphanage.', '', '', 'Young children greet those bring supplies as they arrive at Nam Hung.', '', '', '', 'Giving away treats and gifts.', '', '', '', 'Group picture with orphanage children and some of their new supplies and toys.', '', '', 'One child in ethnic clothing, showing off this new toy.', 'Uyen, one of our volunteers in Vietnam, poses with one of the nuns', '', '', '', 'Playing with the new toys in the courtyard.', 'Vu, another volunteer, with one of the children', '', 'Basic sleeping area at the orphanage.', 'This is the kitchen where new the rice, meat and cooking supplies will be used with fresh vegetables from the region.', 'The Christmas spirit can be seen with this display of the manger', 'Kids enjoying their new soccer balls.', '', '', 'The children of different ages live very closely, and help each other with they toys.', '', '', '', ''),
	'nh-Christmas2008-vn' 	=> array('Mỗi bao là quà bánh kẹo cho mỗi em.  Trong mỗi bao có vài cái bánh bông lan, một hộp sữa, và kẹo.', '', 'Mì gói, nước mắm, bánh, kẹo, trái banh, gạo, tả, đồ chơi đang được chuẩn bị mang ra xe.', 'Hai chiếc xe đạp xinh xinh này sẽ được các em dùng hàng ngày để đi học và đi chợ.  ', '', 'Mọi việc đã được sắp xếp chu đáo, bây giờ chỉ có xếp vào xe và chuẩn bị lên đường thôi :)', 'Vài món đồ chơi nho nhỏ cho các em.', 'Các thùng mì gói Gấu đỏ mới được mua về.', '', 'Nước mắm, xà bông giặt đồ, gạo', 'Nhờ vào số tiền đóng góp của tất cả quỶ ân nhân, nhóm đã có đủ tiền để mua những bao gạo thơm ngon đến cho các em.', 'Mấy lon sữa này sẽ giúp cho các em có đủ chất bổ dưỡng cho cơ thể.', 'Nhóm đã mua rất nhiều dụng cụ học sinh cho các em để giúp các em có những món cần thiết để các em có thể tiếp tục đi học.', '', '', '', '', 'Xe đang bon bon chạy trên con hẻm dẫn đến Giáo Xứ Nam Hưng.', 'Các em đang phấn khởi ra tiếp đón nhóm Tiến Bước.', 'Thành viên của nhóm Tiến Bước đang khệ nệ mang quà vào cho các em.', 'Các em nhỏ đang mở rộng cửa đón chào nhóm Tiến Bước.', 'Đây là sân chơi của các em.  Trong giờ nghỉ giải lao, ngoài chiếc cầu tuộc này, các em không có các món đồ chơi nào khác để giải trị', 'Soeurs trong cô nhi viện dẫn các em ra chào đón phái đoàn.', 'Sau khi phânh phát quà cho các em thành viên trong nhóm Tiến Bước chúc các em một mùa Giáng Sinh vui vẻ.', 'Các em nhỏ đang lắng tay nghe lời chúc Giáng Sinh và đang nôn nóng được trở vào nhà để chơi với các món quà của mình.', 'Một vài em lớn đứng ra chúc Giáng Sinh và tỏ lòng cám ơn đến tất cả các quỶ ân nhân đã giúp đỡ Nhóm Tiến Bước thực hiện được nhiệm vụ này thành công một cách mỹ mãng.', 'Thành viên trong nhóm Tiến Bước chụp vài hình với các em để làm kỷ niệm.  Đây là những giây phút vui vẻ nhất.', '', '', 'Các món quà nho nhỏ nhưng chứa đầy tình thương đang được phân phát ra cho các em.', 'Mọi người tụ họp lại nói cười vui vẻ', 'Các em nhỏ đang giơ tay nhận lấy món quà của mình.', 'Mọi người đang hô to \"Cám ơn\"', '', '', '', '', 'Các em đang hớn hở mở gói quà của mình ra và khoe với các  bạn.', '', '', '', '', '', '', 'Đây là nhà bếp.', 'Nhân dịp Giáng Sinh, các em đã cùng nhau xây hang Chúa đơn sơ này dùng làm nơi cầu nguyện.', '', 'Em bé trai này đang thử món đồ chơi mới của mình.', 'Em nào cũng bận bịu với món đồ chơi mới có.', '', '', '', '', 'Thành viên nhóm Tiến Bước đang tập hát cho em bé trai nàỵ'),

	'tb-Christmas2008-en'	=> array('', 'Supplies arriving at the Thien Binh orphanage.', 'Our rental van loaded with supplies and gifts', '', 'Large bags of meat are enough to provide the children at Thien Binh hearty and delicious meals this Christmas', '', '', 'Unloading supplies', 'One of the kids gives the sign to the camera as the van arrives with supplies.', '', 'A group picture of the boys with their new soccer balls.', '', 'Kids lining up to receive their presents', '', 'Children greeting their guests.', 'And waiting in line for toys and treats.', '', '', '', '', '', 'Kids check out their new goods.', '', '', '', '', '', '', '', 'Handing out toys and school supplies, the kids inspect.', '', '', '', 'Children and youth of all ages are provided with schooling and community at Tien Binh.', '', '', 'Boxes and piles of food and kitchen supplies have been delivered to Thien Binh.', 'Children receiving gifts bought buy you!', '', 'The Sisters of Thien Binh are pictured with our volunteers', '', 'Courtyard at the Thien Bing orphanage.', '', '', 'The play area of infants, which is in poor condition', '', '', '', 'The formula provides a healthier start where infants don\'t have mothers to provide.', '', '', '', '', '', 'This orphanage has a few young infants.', ''),
	'tb-Christmas2008-vn' => array('Trên đường đi đến Cô Nhi Viện Thiên Bình.', 'Cửa đã mở sẵn sàng để chào đón phái đoàn.', 'Chiếc xe van đang chở phái đoàn của nhóm Tiến Bước đến Cô Nhi Viện Thiên Bình.', '', 'Đây là những bao thịt.  Giáng Sinh năm nay các em sẽ có những bữa ăn đầy đủ', 'Mọi người đang chuẩn bị mang quà ra.', '', '', 'Ông \"lơ xe\" tí hon đang đứng chụp hình trước xe.', 'Các em khác tụ họp lại để chụp vài tấm làm kỷ niệm nhưng vì đang vui quá, cho nên em nào cũng lo ra, đang chăm chú nhìn những món quà mang đến.', 'Nhóm Tiến Bước cũng bắt chước đến chụp ké vài tấm hình với các em.  Các em trai đang ôm lấy mấy chiếc bóng mới nhận được.', 'Các em nhỏ lễ phép xếp hàng nhận quà.  Tuy cơ hội đến trường rất ít, nhưng các em đã được các soeurs nuôi dạy rất kỹ lưỡng cho nên em nào cũng rất lễ phép.', 'Một thành viên trong nhóm đang chúc Giáng Sinh và chúc các em chóng lớn, học giỏi và hạnh phúc.', '', 'Cô nhi viện Thiên Bình ở xa thành phố hơn và có nhiều em nhỏ có khuyết tật hơn cô nhi viện ở Nam Hưng.', 'Một bé trai lớn đã đại diện các em, đứng ra cám ơn nhóm Tiến Bước và các quỶ ân nhân đã giúp đỡ các em.  Sau khi cám ơn xong, các em khác hát to một bài để tỏ lòng biết ơn.', 'Từng em một nề nếp tiến lên để nhận món quà của mình.', 'Nhìn các em cười vui vẻ đã làm nhóm rất cảm động.', 'Mọi người đều vui vẻ nhận lấy quà của mình.', 'Thấy các em vui vẻ làm nhóm cũng vui lâỵ', '', '', '', '', '', '', '', '', 'Hai chiếc xe đạp xinh xắn này sẽ giúp đỡ cho các em rất nhiều trong việc đi lại hằng ngày, như đi học và đi chợ.', '', '', 'Các em nhỏ đang hồi họp chờ đến phiên mình.', '', '', '', 'Các thùng mì gói, nước mắm, gạo, tả giao lại cho các soeurs bảo quản.', 'Các em trai đang hớn hở vì thấy mấy chiếc bóng to.  ', '', '', 'Tất cả mọi người cười to lên nghe :)', '', '', 'Vài em nhỏ dẫn nhóm đi vòng vòng thăm cô nhi viện.', 'Đây là nhà thờ Thiên Bình, nơi các em đến cầu nguyện hàng ngàỵ', 'Các em bé này đòi được thay vào những chiếc tả mới ngay sau khi nhận quà của mình.  Các em rất thích và cảm thấy thoải mái.', 'Một bà soeur đang thay tả mới cho bé ', 'Các em nhớ uống sữa cho nhiều chóng lớn.', 'Những hộp sữa này sẽ giúp cho các em có được những chất bổ cần thiết cho cơ thể và bù đắp lại những thiếu thốn hàng ngày', '', '', '', '', '', 'Hai em đang quét dọn nhà thờ.', 'Những chiếc tả mới và vài thùng sữa ngon này sẽ giúp cho bé mau lớn và khỏe mạnh.', 'Soeur ở cô nhi viện Thiên Bình đang trao đổi vào lời với thành viên của nhóm, và nhờ gửi lời cám ơn đến tất cả các ân nhân đã giúp đỡ cho các em ở cô nhi viện nàỵ'),
	
	'prep-Christmas2009-en'	=> array('Arriving with supplies loaded and ready for the orphanages',
			'Purchasing baby formula and drinks from the list of One More Step items',
			'',
			'More supplies arrive by scooter',
			'',
			'',
			'New bicycles for the older children',
			'A lot of rice',
			'Baby formula for the orphanage\'s infants',
			'Toys purchased by our donors',
			'Cooking supplies, oil, sugar and spices',
			'Laundry detergent',
			'More cooking and medical instruments',
			'',
			'',
			'Books and pencil crayons for the kids',
			'',
			'Individual packages of sweets for each kid.',
			'Getting supplies together and ready for the Nam Hung and Xuan Tam',
			'',
			'',
			'Medical supplies and medicine',
			'',
			'Toys and small gifts that the kids adore',
			'Meats ready for delivery',
			''),
	'prep-Christmas2009-vn'	=> array(),
	'xt-Christmas2009-en'	=> array('On the way to the Xuam Tam orphanage with supplies and gifts',
'',
'',
'Thanks for those who donated to Transportation that helped fuel this van!',
'Meats purchased for Xuan Tam',
'One of our volunteers learning about Xuan Tam from the Sisters',
'Young children receive education at the orphanage',
'',
'',
'',
'A child holding infant formula necessary for the youngest members of the orphanage',
'Handing over gifts and supplies donated by One More Step donors',
'Kids posing before receiving their gifts',
'',
'Giving the head nun the donation for clothing',
'',
'The managers of Xuam Tam and One More Step volunteers hand out supplies',
'OneMoreStep team is taking a group picture with the little ones and the nuns.',
'',
'The little ones curiously opened everything that was given to them within a couple of minutes and tried out everything.',
'A talented little girl trying to impress the visitors',
'This little one just wants to eat his delicious snack, minding his own business.',
'These kids are well taught, they are sharing their new toys and snacks with each other.',
'One More Step team is packing up and getting ready for Xuan Tam location 2',
'At Xuan Tam location 2. This bike will help these kids with many chores, from getting the grocery to going to school.',
'One More Step team is distributing the presents to the little ones at Xuan Tam location 2',
'The Xuan Tam location 2 Sisters came out to welcome the team and expressing her sincere thanks to all the donors for their generositỵ',
'One More Step team hands over the money for clothing.',
'The two little ones are showing off their presents.',
'The little ones are lining up properly to receive their presents.',
'These little ones are are not used to taking pictures at all!  They are very shy and didn\'t know what to do.',
'',
'One More Step team and the head nun at Xuan Tam Location 2 are taking a little break and talking about the kids.',
'This little one is anxious to get her hands on her new coloring book.',
'',),
	'xt-Christmas2009-vn'	=> array('Trời mới sớm tinh mơ nhóm đã bắt đầu chuẩn bị chuyển đồ lên xe để tới thăm các em ở Cô Nhi Viện Xuân Tâm trước và sau đó là cô Nhi Viện Nam Hưng',
'Sau một chuyến đi dài, xe đã tới Cô Nhi Viện Xuân Tâm. ',
'Cổng vào Cô Nhi Viện Xuân Tâm',
'Các gói quà đang từ từ mang vào trong.',
'Đây là bịch thịt bò tươi.',
'Các soeurs ra tiếp nhóm Tiến Bước.',
'Một thành viên trong nhóm đang tò mò xem em này đang làm bài gì.  Em đang tập làm toán.',
'Các em nhỏ chạy ra tiếp đón nhóm Tiến Bước',
'Em bé nhỏ này cũng được một bình sữa to.',
'Các soeur và các em đều rất vui mừng khi nhận được những món quà này do các ân nhân trợ giúp, ai cũng cười toe toét.',
'Em bé này sau khi được nhận hộp sữa rồi thì cứ ôm chặt vào mình, không cho ai đụng đến.',
'Soeur và các chị giúp việc muốn chụp ké vài tấm hình làm kỷ niệm',
'Các em nhỏ và nhóm Tiến Bước chụp tập thể một tấm hình đẹp cho các ân nhân xem.',
'Chị Uyên đang trao quà cho các em.',
'Soeur đang thay mặt các em nhỏ cám ơn nhóm Tiến Bước và các ân nhân bên Canada đã có lòng hảo tâm nhớ đến các em và đã giúp đỡ các em rất nhiều.',
'',
'Các em đang ngồi xem các bạn mình có gì ',
'',
'',
'',
'',
'',
'',
'',
'',
'',
'',
'',
'',
'',
'',
'',
'',
'',
''),
	'nh-Christmas2009-en'	=> array('OneMoreStep team arriving at Nam Hung Orphanage with the Gifts and Supplies',
'An eager welcome from the children for the OneMoreStep Team',
'Medicines, medical supplies and equipment, and a lot more gifts! One of the nuns who run the orphanage is amazed by the generosity gifts',
'The children are curious and happy as they check out the gifts!',
'More overjoyed children at the orphanage, eagerly looking at the piles of supplies and gifts.',
'Uyen, our head volunteer in Vietnam takes a moment to explain how the donors chose different items to sponsor for the orphanage !',
'',
'Kids and youngsters at Nam Hung lining up to receive their goodies.',
'They wear their happiness with a Smile and Thank the donors with all their hearts.',
'',
'More supplies of food and other necessities for running the orphanage.',
'The youngest of the orphans sharing a fun moment.',
'Uyen giving the head nun the money to purchase clothing for the children as we do not know their sizes',
'',
''),
	'nh-Christmas2009-vn'	=> array('Xe đang lăng bánh từ từ vào Cô Nhi Viện Nam Hưng.  Các thành viên trong nhóm đều đang mong mỏi được gặp mặt lại các em ở trại.',
'Các em ở Cô Nhi Viện Nam Hưng ra chào đón nhóm Tiến Bước',
'Chị Uyên thay mặt nhóm chúc Giáng Sinh cho một trong những người giúp trại.',
'Các em hăm hở chạy ra nhận quà của mình.  Ai nấy đều chăm chú coi năm nay mình được món gì.',
'Các em trai nhanh chân chạy ra trước, bây giờ đến phiên các em nữ chạy ra nhận quà .',
'Chị Uyên trao từng món qua đến cho các em.',
'Em nào cũng tò mò nhìn xem các món đồ chơi này dùng như thế nào, có nhiều món các em chưa hề có cơ hội thấy qua hay chơi qua bao giờ',
'các em nhỏ cũng túm lại xem các anh chị lớn làm gì.',
'chụp thêm một tấm nữa cho chắc ăn.',
'Tấm này có thêm các soeurs ra chụp chung với các em cho vui',
'Chị Uyên chụp chung với một trong những em nhỏ nhất trong Cô Nhi Viện.',
'',
'Uyên đưa tiền cho sơ để sắm quần áo mới cho các em vì Tiến Bước không biết số đo của các em.',
'',
''),
	
'hue-Christmas2009-en'	=> array('','','','','',''),
'hue-Christmas2009-vn'	=> array('','','','','',''),
	
'prep-Christmas2010-en' => array('Our helpful volunteers in Vietnam doing the hardest parts of our effort; counting, sorting, packing and labeling.',
'Each orphanage has given a specific list for supplies and gifts. Our volunteers are dividing all the purchased supplies and sorting them as per the requested lists.',
'Checking and double checking to make sure everything is indeed OK and ready for delivery.',
"This year's gift bag contents: Chocolates, milk boxes, candies and goodies, crayons, pencils, stationery kit for every orphan kid at the two orphanages.",
"Each red bag contains every individual's gifts. Carefully sorted and packed.",
'All ready to be delivered!',
'',
'',
'',
"Cans of milk formula are a vital replacement for mother's milk. There are orphaned infants who solely rely on this for healthy development.",
'This is laundry detergent',
'These are economy size cooking oil bottles.',
'These are soaps for hand washing and bathing',
'Some soy sauce and fish sauce are displayed',
'Some colouring books novels for the children'
),

'prep-Christmas2010-vn' => array('Các chị thiện nguyện viên đang chuẩn bị gói những món quà vào bao cho các em.',
'Mỗi cô nhi viện giúp đỡ các em mồ côi theo nhiều lứa tuổi, và mỗi lứa tuổi cần những món quà khác nhau cho nên các chị thiện nguyên phải sắp xếp sao cho đúng theo cái danh sách mà nhóm gửi về.',
'Thiện nguyện viên kiểm tra từng gói quà thật kĩ coi có còn thiếu xót gì không.',
'Mỗi phần quà gồm có các món như sau: sôcôla, kẹo, viết chì màu, và viết chì đen.',
'Các món quà được bỏ cận thận vào từng bao đỏ để tặng cho các em.',
'Các phần quà đã xong, chuẩn bị lên đường!',
'',
'',
'',
'Sữa hộp cho em bé.',
'Đây là xà bông giặt đồ',
'Các chai dầu ăn thật lớn.',
'Đây là xà bông tắm',
'Vài chai nước tương và nước mắm được trưng bày',
'Sách tô màu và truyện cho các em.'
),

'xt1-Christmas2010-en' => array(
'Arrived at our first destination, a van filled with gifts ready to be given out to the children',
'Meat portions as generously purchased on the website by our donors.',
'Two of our volunteers',
'Some of the children that were present at the time and their caretakers at Xuan Tam Orphanage - Location 1',
'The volunteers are handing out gifts to all the little children',
'',
'The nuns are very happy with the amount of gifts the orphanage is given.',
'',
'',
'',
'',
'Some of the little ones at Xuan Tam enjoying their Christmas treats',
'At the orphanage, kids as young as 6 would be taking care of the younger "siblings"',
'The kids love the school kits',
'',
'',
'',
'',
'The children were so excited to try the delicious foods.',
'',
'These adorable faces enjoying their Christmas treats makes it all worthit',
'',
'',
'',
'The sisters and volunteers at the orphanage exchanging information about their activities to better understand the situation and how OneMoreStep can help.',
'Our volunteers explaining the proof of receipt form.  The nuns are asked to sign off on all the items and quantities they have received',
'',
'',
'One of the nuns is distributing notebooks',
'',
'',
'',
),

'xt1-Christmas2010-vn' => array(
'Nhóm bắt đầu chuẩn bị lên thăm các em.  Xe chất đầy quà tặng của các ân nhân gửi tiền về mua.',
'Những phần thịt này sẽ cho các em mấy bữa ăn thật ngon',
'Vài thiện nguyện vien của chúng tôi',
'Các em và các soeurs ra chào đón nhóm Tiến Bước.',
'Các anh chị thiện nguyện viên đang phát quà cho các em.',
'',
'Các soeurs rất mừng vì được qúy vị tăng rất nhiều quà',
'',
'',
'',
'',
'Các em nhỏ rất là thích thú nên lấy ra ăn liền :) Có nhiều món các em chưa ăn qua bao giờ :(',
'Tại Xuân Tâm cac em mới cấp 1 đã bắt đầu săn sóc cho các em nhỏ hơn',
'Các em nhỏ rất thích các dụng cụ đi học',
'',
'',
'',
'',
'',
'',
'Nhìn các em vui thú bên những món quà nhóm mang tới làm nhóm rất bùi ngùi cảm động và thương cho các em.',
'',
'',
'',
'Các soeurs và các thiện nguyện viên của nhóm đang trao đổi thêm tin tức về các hoạt động của trại để nhóm có thể nắm rõ thêm tình hình như thế nào và làm cách nào để có thể giúp thêm cho các em.',
'Uyên yêu cầu các sơ ký nhận tất cả những món quà đã nhận',
'',
'',
'Sơ phụ giúp các em chia tập vở.',
'',
'',
''
),

'xt2-Christmas2010-en' => array(
'The children of Xuan Tam orphanage, location two which is a 30 min drive from the location 1.',
'Unloading gifts and supplies',
'Such adorable faces, curiously accepting gift bags',
'Every kid was anxiously awaiting her gift bag',
'The children customarily lining up for presents',
'Another souvenir photo',
'',
'',
'',
'',
'',
'The girls were all smiles as they open their gift bags.',
'',
'',
'',
'',
'',
'T',
'',
'',
'',
'The head nun is signing the Gift Receipt Form acknowledging the items and quantity of gifts received.',
'',
'',
'',
'',
'The children at the orphans were waving "Bye" to the volunteers as the were leaving for the day.',
''
),

'xt2-Christmas2010-vn' => array(
'Các em ở viện mồ côi Xuân Tâm ra chụp hình kỷ niệm với nhóm.',
'Đồ dùng, thức ăn và quà cho các em được các thiện nguyện viên chia ra thành nhiều phần và phân phát cho từng cô nhi viện theo sự yêu cầu của các soeurs.',
'Các em bé với khuôn mặt ngô ngĩnh nhận quà tặng ',
'Các chị trong nhóm đang trao quà cho các em.',
'Các em nhỏ đứng xếp hàng theo thứ tự để nhận quà trông thật dễ thương.',
'Nhóm chụp hình lưu niệm',
'',
'',
'',
'',
'',
'Những bé gái cười thật to khi mở giỏ quà',
'',
'',
'',
'',
'',
'',
'',
'',
'',
'Sơ trưởng đang ký giấy biên nhận quà do nhóm mang tới.',
'',
'',
'',
'',
'Các em chào tạm biện nhóm.',
''
),

'nh-Christmas2010-en' => array(
'The children at Nam Hung orphanage recreate a nativity scene with makeshift items. It comes complete with a foil river and a little wooden bridge.',
'Getting for day two of the Gift Drive',
'',
'Unloading gifts and supplies at Nam Hung',
'The children were admiring the Nativity Scence that was set up at the orphange.',
'',
'',
'',
'',
'',
'',
'',
'Vương, the oldest orphan at Nam Hưng is helping a little one accept his present',
'Soeur Hằng shows her joy',
'',
'Cu Đen, the child we featured on the "Milk" item page is seen here all grown up',
'',
'',
'',
'',
'Cu Đỏ, another of our featured kids on the "toys" item page.',
"This little cutie just recieved a bag of cookie, she can't wait to open it.",
'Packing up and ready to head home',
'',
'',
'',
'',
'',
''
),

'nh-Christmas2010-vn' => array(
'',
'Đang chuẩn bị cho ngày đưa quà thứ hai',
'',
'Chuyển quà vô Nam Hưng',
'Các em nhỏ dang thưởng thức hang đá.',
'',
'',
'',
'',
'',
'',
'',
'Em Vương, cô nhi lớn nhất tại Nam Hưng giúp một em nhỏ nhận quà',
'Hôm nay là ngày vui cho nên ai cũng vui vẻ.',
'',
'Em Cu Đen có mặt trong trang quà "sữa" giờ đà lớn',
'',
'',
'',
'',
'Cu Đỏ có mặt trên trang "đồ chơi" của chúng tôi',
'Bé gái này được tặng bịch bánh cho nên rất là hớn hở.',
'Chuẩn bị ra về',
'',
'',
'',
'',
'',
''
)
);

$desc = $album_sel.'-'.$_POST['lang_sel'];

$album_desc = $album_desc[$desc];

$album = array();
$album['vn'] = array(
	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description'


	);


$album['en'] = array(
	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description',

	'imagefilename'		=> 'description'


	);



?>
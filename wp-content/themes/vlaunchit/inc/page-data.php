<?php
if (!defined('FILE_APPEND')){
	define("FILE_APPEND", 'FILE_APPEND');
}
if (!function_exists('file_put_contents')) {
    function file_put_contents($filename, $data, $overwrite = false) {
    	if ($overwrite != FILE_APPEND){
	        $f = @fopen($filename, 'w');
    	}
    	else {
			$f = @fopen($filename, 'a+');
    	}

    	if (!$f) {
            return false;
        } else {
            $bytes = fwrite($f, $data);
            fclose($f);
            return $bytes;
        }
    }
}

function get_file_contents($filename)
{
	if (!function_exists('file_get_contents'))
	{
		$fhandle = fopen($filename, "r");
		$fcontents = fread($fhandle, filesize($filename));
		fclose($fhandle);
	}
	else
	{
		$fcontents = file_get_contents($filename);
	}
	return $fcontents;
}


$lang = array();
$lang['en'] = array (
		'Home'			=> 'Home',
		'ViewGiftBag' 	=> 'View Gift Bag',
		'Mission'		=> 'Mission',
		'Left'			=> 'LEFT',
		'Target'		=> 'Target',
		'Price'			=> 'Price',
		'Qty'			=> 'Qty',
		'Item'			=> 'Item',
		'TotalCost'		=> 'Cost',
		'Total'			=> 'Total',
		'PurchaseGifts' => 'Purchase Gifts',
		'NoGift'		=> 'You gift bag is currently empty. Please select an item from our online catalogue above.',
		'PurchaseGifts'	=> 'Purchase Gifts',
		'Name'			=> 'Name',
		'AddToBagImg'	=> 'AddToGiftBagButton.gif',
		'Team'			=> 'Meet The Team',
		'Contact'		=> 'Meet the Team',
		'SendCheck'		=> 'Please print this page, make the cheque payable to <b>ONEMORESTEP International Relief Org</b> and mail it to the address below: ',
		'About'			=> 'About the Project',
		'SoldOut'		=> 'Sold Out',
		'ThankYou'		=> 'Thank You',
		'Album'			=> 'Album',
		'Finance'		=> 'Financial Report',
		'Donors'		=> 'Donor List',
		'PaymentMethod'	=> 'Method of payment',
		'Optional'	=> 'Optional',
		'ShowDonor'		=> 'Yes, my name can be listed on the <a href="donors.php">Donors</a> page',
		'QuickLinks'	=> 'Quick Links',
		'QuickLinks-2008'		=> '2008 Project Website',
		'QuickLinks-2009'		=> '2009 Project Website',
		'QuickLinks-GiftBag'		=> 'View Your Gift Bag',
		'QuickLinks-NamHung'		=> 'Nam Hung Orphanage',
		'QuickLinks-XuanTam'		=> 'Xuan Tam Orphanage',
		'QuickLinks-HueBinhDien'		=> 'Hue & Binh Dien Schools',
	'Announcement1-Heading'	=> 'Christmas Gifts',
		'Announcement1-Text'	=> 'Fundraising Target: <span class="side_emphasized">$1651</span> <br>End Date: <span class="side_emphasized">Dec. 13th</span>',
		'Announcement2-Heading'	=> 'Education Fund',
		'Announcement2-Text'	=> 'Fundraising Target: <span class="side_emphasized">$1350</span> <br>End Date: <span class="side_emphasized">Jan. 14th</span>'
);


$lang['vn'] = array (
		'Home'			=> 'Trang Chính',
		'ViewGiftBag' 	=> 'Giỏ Quà',
		'Mission'		=> 'Mục Đích',
		'Left'			=> 'Còn',
		'Target'		=> 'Mục Tiêu',
		'Price'			=> 'Giá',
		'Qty'			=> 'Số Lượng',
		'Item'			=> 'Quà',
		'TotalCost'		=> 'Tổng Giá',
		'Total'			=> 'Tổng Số',
		'PurchaseGifts' => 'Mua Quà',
		'NoGift'		=> 'Giỏ quà của quý vị hiện đang trống.  Xin vui lòng chọn một món quà.',
		'PurchaseGifts'	=> 'Mua Quà',
		'Name'			=> 'Họ Tên',
		'AddToBagImg'	=> 'AddToGiftBagButton_vn.gif',
		'Team'			=> 'Gặp Gỡ Nhóm',
		'Contact'		=> 'Gặp Gỡ Nhóm',
		'SendCheck'		=> 'Xin quý vị in trang này ra, gởi kèm với check cho <b>Phong Le</b> tới địa chỉ ở dưới: ',
		'About'			=> 'Dự Án',
		'SoldOut'		=> 'Hết Hàng',
		'ThankYou'		=> 'Cám Ơn',
		'Album'			=> 'Ảnh',
		'Finance'		=> 'Bản Báo Cáo Tài Chánh',
		'Donors'		=> 'Các Ân Nhân',
		'PaymentMethod'	=> 'Cách trả tiền',
		'Optional'	=> 'Không bắt buộc',
		'ShowDonor'		=> 'Tôi đồng ý để tên tôi được ghi trên trang <a "donors.php">Ân Nhân</a>',
		'QuickLinks'	=> 'Thông Tin',
		'QuickLinks-2008'		=> 'Trang Web Gây Quỹ 2008',
		'QuickLinks-2009'		=> 'Trang Web Gây Quỹ 2009',
		'QuickLinks-GiftBag'		=> 'Xem Giỏ Quà',
		'QuickLinks-NamHung'		=> 'Cô Nhi Viện Nam Hưng',
		'QuickLinks-XuanTam'		=> 'Cô Nhi Viện Xuân Tâm',
		'QuickLinks-HueBinhDien'		=> 'Lớp Học Huế & Bình Điện',
		'Announcement1-Heading'	=> 'Quà Giáng Sinh',
		'Announcement1-Text'	=> 'Mục Tiêu Gây Quỹ: <span class="side_emphasized">$1651</span> <br>Ngày Cuối:<span class="side_emphasized">ngày 13,tháng 12</span>',
		'Announcement2-Heading'	=> 'Quỹ Ăn Học',
		'Announcement2-Text'	=> 'Mục Tiêu Gây Quỹ: <span class="side_emphasized">$1350</span> <br>Ngày Cuối: <span class="side_emphasized">ngày 14,tháng 1</span>'
);


$items = array();
$items['en'] = array
(

 'education-fund' 	=> array	(
			'name' 		=> "Education Fund",
			'desc'		=> 'The need in Vietnam is great, particularly for educational tools that will further the ability of poor children to learn and have a chance.  This fund met those needs for <a href="./Hue_Binh_Dien.php">Hue and Binh Dien schools</a> in 2009.  The fund will similiarly provide for materials on a more sustainable basis for other education projects throughout 2011.</a>',
			'price'		=> 15,
			'target'	=> 90,
			'image'		=> 'education_fund.jpg'
	),
	'cooking-supplies' 		=> array	(
				'name' 		=> "Kitchen Supplies",
				'desc'		=> "Cooking spices and kitchen supplies are needed year round to provide the children with tasty meals. We are purchsing sugar, salt, soy sauce, fish sauce, cooking oil, a little bit of MSG(requested by the orphanges), other cooking spices.",
			'price'		=> 5,
			'target'	=> 35,
			'image'		=> 'cooking-supplies.jpg'
	),
	'milk' 		=> array	(
			'name' 		=> "Milk",
			'desc'		=> " Provide young children from 2 to 5 years with the necessary nutrients from milk.  Milk is an important source of protein, lactose and calories. Young children who lack their mother’s natural nutrients are in particular need of this process and less natural supplement.<br><b>(Note: 1 unit = 1 box of fifty 220ml bags)</b>",
			'price'		=> 11,
			'target'	=> 10,
			'image'		=> 'milk.jpg'
	),
	
	
	'books' 		=> array	(
			'name' 		=> "Novels",
			'desc'		=> " A book can serve as the fountain of joy, the ingredient of hope, and the consolation of a loyal friend who will remain with you through out your life. Give a few books to allow children to learn outside of school and practice life and social skills with each other. <br><b>(Note: 1 unit = approx. 3 books)</b>",
			'price'		=> 5,
			'target'	=> 12,
			'image'		=> 'books.jpg'
	),
	'school-supply' 		=> array	(
			'name' 		=> "School Supplies",
			'desc'		=> "Students of all ages at the orphanages need the basics of education – from pens and pencils, notebooks and rulers, help fill the classrooms. <br><b>(Note: 1 unit = 4 supplies kits)</b>",
			'price'		=> 8,
			'target'	=> 15,
			'image'		=> 'school_supplies.jpg'
	),
	'lets-go' 		=> array	(
			'name' 		=> "Textbooks",
			'desc'		=> "Let's Go textbooks are a series of English learning books not available at school.  Nam Hung has requested 10 books from levels 1 and 2 to provide the children with a fun and effect tool for learning English. <br><b>(Note: 1 unit = 1 book)</b>",
			'price'		=> 3.25,
			'target'	=> 20,
			'image'		=> 'lets-go.jpg'
	),
	'bag-of-rice' 	=> array	(
			'name' 		=> "Rice",
			'desc'		=> "Rice is Vietnam’s main staple and at times, can be the only meal to be had. It provides protein, carbohydrates, folate and iron. A 16.6 kg bag can support a group of 6-8 for a month.  <br><b>(Note: 1 unit = 16.6kg of rice)</b>",
			'price'		=>15,
			'target'	=> 18,
			'image'		=> 'bag-of-rice.jpg'

	),
	'formula' 			=> array	(
			'name' 		=> "Formula",
			'desc'		=> " Provide babies and young children with a most necessary ingredient they may be missing.  Milk is an important source of protein, lactose and calories for young infants and growing children. Infants who lack their mother’s natural nutrients are in particular need of this process and less natural supplement.<br><b>(Note: 1 unit = 900gr of milk formula)</b>",
			'price'		=> 7,
			'target'	=> 18,
			'image'		=> 'formula.jpg'
	),
	'meatfish' 		=> array	(
			'name' 		=> "Meat/Fish",
			'desc'		=> " The children at the orphanages rarely see meat on the dinner table.  Meat and fish contain much needed protein for muscle development. This Christmas, provide the children with something that is both delicious and nutritious. <br><b>(Note: 1 unit = 2 kg of meat)</b>",
			'price'		=> 15,
			'target'	=> 10,
			'image'		=> 'meat.jpg'
	),
	
		'desserts' 		=> array	(
			'name' 		=> "Sweets",
			'desc'		=> "Give some Vietnamese cakes, sweets, and milk for an occasional treat for the kids at the orphanages.  They bring so much happiness to these children… and to some adults as well. This year we are planning to include for each present: chocolate, cakes, milk, and many variety of yummy candies.<br><b>(Note: 1 unit buys 4 presents)</b>",
			'price'		=> 7,
			'target'	=> 20,
			'image'		=> 'dessert.jpg'
	),
	'dolls' 		=> array	(
			'name' 		=> "Toys",
			'desc'		=> " These simple toys are great for promoting creativity and build an imaginative framework.  It is something for a child to enjoy for their time outside of the classroom.",
			'price'		=> 10,
			'target'	=> 11,
			'image'		=> 'dolls.jpg'
	),
		
	'hygiene' 		=> array	(
			'name' 		=> "Hygiene kit",
			'desc'		=> "Daily items we take for granted are needed: soap, shampoo, toothbrushes and toothpaste, laundry detergent. A clean child will also keep the diseases at bay.<br><b>(Note: 1 unit = 1 3kg detergent bag, 2 shampoo bottles, 6 soap bars, and a few toothbrushes)</b>",
			'price'		=> 11,
			'target'	=> 15,
			'image'		=> 'handsoap.jpg'
	),
	
	'instant-noodles' 	=> array	(
			'name' 		=> "Instant Noodles",
			'desc'		=> "Noodles provide an important ingredient and base calories for meals like Phở.  They are simple to make when short on time at the orphanage. <br><b>(Note: 1 unit = 1 box of 30 packs)</b>",
			'price'		=> 4,
			'target'	=> 10,
			'image'		=> 'instant_noodles.jpg'
	),
		'transport' 	=> array	(
			'name' 		=> "Transport Cost",
			'desc'		=> "This necessary expense is needed to cover the fuel and rental cost of a van, big enough to carry all these supplies and presents to the two orphanages that are hours away from the city.<br><b>(Note: 1 unit = 1/12 of the cost)</b>",
			'price'		=> 10,
			'target'	=> 12,
			'image'		=> 'transport.jpg'
	)
	

);


$items['vn'] = array
(
 	
	'cooking-supplies' 		=> array	(
			'name' 		=> "Gia Vị",
			'desc'		=> "Không có gia vị lắm khi cơm không lành mà canh thì chẳng ngọt. Và nếu vậy e rằng các em ăn ít đi, dễ mắc bịnh, thì người cũng chẳng lành"
	),
	'education-fund' 	=> array	(
			'name' 		=> "Quỹ Ăn Học",
			'desc'		=> 'Những dụng cụ học hành rất cần thiết tại Việt Nam, nhất là để tạo dựng cơ hội học hành cho các trẻ em nghèo.  Quỹ ăn học đã giúp các trẻ em của trường học tình thương <a href="./Hue_Binh_Dien.php">Huế và Bình Diện</a> năm học 2009-2010.  Cũng tương tự, năm nay quỹ này sẽ giúp đóng góp vào các dự án phát triển sự học vấn của các trẻ em nghèo.',
			
	),

	'milk' 		=> array	(
			'name' 		=> "Sữa Nước",
			'desc'		=> "Sữa nước dành riêng cho các em nhỏ từ 2 đến 5 tuổi. Sữa là nguồn protein, đường lactose và calorie cho trẻ em. Các em thơ sinh vì thiếu sữa mẹ và rất cần sự thay thế của sữa nhân tạo.<br><b>(Lưu ý: 1 đơn vị = 1 hộp gồm 50 bịch 220ml)</b>"
	),
	
'books'    => array     (
			'name'		=> "Sách Truyện",
			'desc'		=> "Một quyển sách quí có thể là nguồn gốc của niềm vui, là chân lý của nỗi hy vọng, và là sự an ủi của một người bạn vĩnh viễn trung thành qua mọi giai đoạn của cuộc đời. <br><b>(Lưu ý: 1 đơn vị = khoảng chừng 3 cuốn)</b> "
	),
	'school-supply'    => array     (
			'name'		=> "Dụng Cụ Đi Học",
			'desc'		=> "Lớp học tại nhà trường cần những dụng cụ cần thiết như giấy bút, tập để viết, thước kẻ, phấn ...vv.  <br><b>(Lưu ý: 1 đơn vị = dụng cụ cho 5 em)</b>"
	),
	'lets-go' 		=> array	(
				'name' 		=> "Sách Tiếng Anh",
				'desc'		=> "Sách Let's Go dùng để giúp các em học Anh Văn.  Nam Hưng ngỏ lời xin 10 quyển sách Tập 1 và Tập 2.<br><b>(Lưu ý: 1 đơn vị = 1 quyển)</b>"
	),			
	'bag-of-rice'	=> array     (
			'name'		=> "Gạo",
			'desc'		=> "Gạo là món ăn chính tại Việt nam. Gạo có thể cung cấp chất đạm, chất béo, sợi thớ (fiber), lượng đường (carbohydrate), vitamin và những chất sắt cùng những kim loại cần thiết. Một bao gạo 10 kg có thể nuôi một nhóm từ 4-6 em trong một tháng trời. <br><b>(Lưu ý: 1 đơn vị = 13.5kg gạo)</b>"
	),

	'formula'    => array(
			'name'		=> "Sữa Bột",
			'desc'		=> "Sữa là nguồn protein, đường lactose và calorie cho trẻ em. Các em thơ sinh vì thiếu sữa mẹ và rất cần sự thay thế của sữa nhân tạo. <br><b>(Lưu ý: 1 đơn vị = 900gr bột sữa)</b>"
	),
	'meatfish'    => array     (
			'name'		=> "Thịt/Cá",
			'desc'		=> "Thịt cá là nguồn protein rất cần thiết cho cơ thể. Mỗi ngày, thân thể cần 0.8g cho mỗi 1kg. Như vậy, một em 30 kg cần khoảng 24g protein. Gạo và rau quả cộng lại thường chỉ cung cấp 5-8 g protein là cao, vì vậy thịt cá rất cần thiết cho sự phát triển của các em.<br><b>(Lưu ý: 1 đơn vị = 2 kg thịt)</b> "
	),
		'desserts' => array     (
			'name'		=> "Bánh kẹo",
			'desc'		=> "Tuy đối với người có đầy đủ dinh dưỡng, bánh kẹo chỉ là đồ tráng miệng. Thật ra, no cũng là một nguyên liệu có lượng đường khá tốt và có thể dùng cho đỡ đói. <br><b>(Lưu ý: 1 đơn vị = bánh kẹo cho 7 em)</b>"
	),
	'dolls'    => array     (
			'name'		=> "Đồ Chơi",
			'desc'		=> "Đây là những món đồ chơi giúp cho sự phát triển của phần sáng tạo hoặc đào tạo sức tưởng tượng trong não bộ. Nó sẽ giúp các em khây thỏa trong lúc nghỉ ngơi."
	),
	
	'hygiene'   => array     (
			'name'		=> "Dụng Cụ Vệ Sinh",
			'desc'		=> "Xà bông, dầu gội đầu, bàn chải đánh răng và kem đánh răng là những vật giúp các em giữ gìn một hàm răng hoàn mỹ,  giữ gìn vệ sinh sạch sẽ, và đỡ bịnh tật."
	),

	'instant-noodles' 	=> array	(
			'name' 		=> "Mì Gói",
			'desc'		=> "Đã những ai từng độc thân sẽ hiểu được sự bổ ích của mì gói. Nếu dựa theo chất bổ thì hơi thiếu thốn, nhưng nếu sét theo tiện lợi và nhất là lúc có mấy chục em than đói mà không kịp làm đồ ăn thì thật chẳng có món nào qua mặt được. <br><b>(Lưu ý: 1 đơn vị = 1 hộp mì ngon)</b>"
	),
	'transport' 	=> array	(
			'name' 		=> "Chuyên Chở",
			'desc'		=> "Hai cô nhi viện nằm xa thành phố, nên cần một khoảng chi phí khá lớn để thuê một chiếc xe 16 chỗ ngồi, dùng chuyên chở rất nhiều quà đến cho các em. <br><b>(Lưu ý: 1 đơn vị = 1/12 số tiên chi phí)</b>"
	)

);



if ((empty($_COOKIE['lang']) && $is_vietnam_ip) || $_COOKIE['lang'] == 'vn'){
	$lang_sel = 'vn';
	$img_lang = '_vn';
}
else {
	$lang_sel = 'en';
	$img_lang = '';
}
$lang = $lang[$lang_sel];


$inv_file = $root . 'data/inventory.txt';
$inv_data = array();
if (file_exists($inv_file)){
	$inv_data = unserialize(file_get_contents($inv_file));
}

foreach ($items['vn'] as $k => $v){
	if (!is_array($items['en'][$k]) || !is_array($items['vn'][$k]))
	{
		print_r($items['en'][$k]);
		print_r($items['vn'][$k]);
	}
	$items['vn'][$k] = array_merge($items['en'][$k], $items['vn'][$k]);
}

$totalDollarSold = 0;
$totalPercentSold = 0;
$totalEdFundDollarSold = 0;
$totalEdFundPercentSold = 0;

$items = $items[$lang_sel];
foreach ($items as $k => $v){
	$sold = 0;
	if (!empty($inv_data[$k])){
		$sold = $inv_data[$k]['sold'];
	}
	$items[$k]['remaining'] = max($items[$k]['target'] - $sold, 0);
	if($k!='education-fund')
	{
		$totalDollarSold = $totalDollarSold+$sold*$items[$k]['price'];
		
		//this if statement is to correct an over purchase of rice during testing (24 units purchased, but only 18 available
		if($items[$k]['name']=="Rice"||$items[$k]['name']=="Gạo")
		{
			$totalDollarSold -= $items[$k]['price']*(24-18);
		}
		
		/* used to print out a log of what has been sold
		try {
			file_put_contents( $root . 'data/inventory_log.txt', $items[$k]['name'].': Sold ('.$sold.') Price ('.$items[$k]['price'].') Total $ Sold ('.$sold*$items[$k]['price'].')'." \n",FILE_APPEND );
		}
		catch (Exception $xpt){
			file_put_contents($root . 'inventory_error.txt', $xpt->getMessage());
		}*/
	}
	else
	{
		$totalEdFundDollarSold = $totalEdFundDollarSold+$sold*$items[$k]['price'];
	}
}
$totalPercentSold = 100*($totalDollarSold)/1651; 
$totalEdFundPercentSold = 100*$totalEdFundDollarSold/1350;


$requested_item = $_GET['item'];


if (!array_key_exists($requested_item, $items)){
	$requested_item = 'bag-of-rice';
}
$current_item = $items[$requested_item];

if ($lang_sel == 'vn'){
	$news1_headline ='94% Tổng Số Tiền Quyên Mang Đến Viện Mồ Côi';
	$news1_body = '<p class="regular_content">Nhóm Tiến Bước xin hân hạnh báo cùng mọi ân nhân đã giúp đỡ các em mồ côi trong dịp Giáng Sinh năm 2008 chúng tôi đã quyên được $1940 đô Canada. Con số này gần gấp đôi khoảng phần đã dự trù. Ngoài $20 đồng phí tổn cho Pay Pal và $100 đồng tiền quyên riêng dành cho vấn đề chuyên chở, tất cả các phần còn lại đã dùng mua những món quà đã hứa và đã được mang tới hai viện mồ côi như đã dự trù. Hai con số trên đây tính ra, 94% của tổng số tiền được dành riêng cho việc mua những món quà mang lại cho các em. Mọi chi phí lặt vặt khác đều được thanh toán qua quỹ riêng của nhóm.
</p>
<p class="regular_content">
	Tất cả mọi em rất vui và an ủi vì được những món quà bất ngờ trong Giáng sinh. Nét mặt tươi rói có thể thấy rõ trong những tấm hình (xin xem tại <a href="./album.php?projectName=Christmas 2008&albumName=Thien Binh">đây</a>). Để được sáng tỏ mọi chi phí, chúng tôi có ghi lại mọi vấn đề tài chính trong <a href="./finance.php">Bản Báo Cáo Tài Chính</a>.
    </p>';
	$news2_headline ='Xây Tổ Ấm Cho Mái Trường';
	$news2_body = '<p class="regular_content">
	Năm nay, ngoài việc mang món quà đến cho những em tại hai viện mồ côi – Xuân Tâm và Nam Hưng – nhóm Tiến Bước bắt đầu tham dự vào dự án giáo dục để có thể mang lại dự án có tính cách lâu dài hơn.</p>
<p class="regular_content">
Trong tương lai gần đây, kiến thức sẽ là một công cụ rất quan trọng trong sự tiến thoái của một quốc gia hoặc của một dân tộc. Cơ Quan Liên Hợp Quốc cũng công nhận điểm này và đã dành cho sự giáo dục một vị trí trong những hàng đầu của mọi <a href="http://www.un.org/millenniumgoals/education.shtml">dự án trong thiên niên kỷ</a> này.
</p>
<p class="regular_content">
Tuy những năm gần đây trường học bắt đầu được xây lên cho các em tại các miền quê, trang bị của những lớp này thật rất đơn sơ và thiếu thốn mọi bề, nhất là sách vở cũng như giấy bút. Năm ngoái, một khoảng nhỏ trong phần gây quỹ của nhóm Tiến Bước đã được dành riêng cho <a href="gift#school-supply">vật dụng đi học</a> cũng như sách vở. Năm nay, chúng tôi đã có tiếp xúc được với hai ngôi trường nghèo túng không được trợ cấp của chính quyền. Trường này hiện đang ở tại  <a href="Hue_Binh_Dien.php">Huế và Bình Điện</a>, nơi đang gặp nhiều khó khăn vì bão lụt.
</p>
<p class="regular_content">
Nhóm Tiến Bước hy vọng năm nay có thể bắt đầu xây một tổ ấm cho vài mái trường qua việc  <a href="gift#education-fund">gây quỹ</a> vừa đủ để có thể mua sắm vật liệu học vấn – như phấn, bút giấy và sách vở. Hy vọng rằng khoảng tiền quyên được sẽ đủ để phân phát cho từng em học sinh và dùng được nguyên năm trong trường.
</p>';
	$news3_headline ='Quỹ Nhận Được Gấp Đôi Dự Tính Vào Diệp Giáng Sinh Vừa Qua';
	$news3_body = '<p class="regular_content">Nhờ sự đóng góp của nhiều ân nhân, sự lãnhh đạo của một người cùng với sự cộng tác của tám vị trong giới thành niên cư ngụ cách nhau nửa quả địa cầu, dự án Tiến Bước được khởi hành vào năm 2008 và mang lại nhiều thành quả tốt. Được thành lập với mục đích đem lại cho trẻ em mồ côi tại Việt nam một chút quà như đồ ăn, đồ đi học hoặc đồ chơi  trong ngày Giáng Sinh, năm đầu tiên Tiến Bước chú trọng tới hai nơi – Thiên Bình và Nam Hưng. Cộng lại tổng cộng là có 170 em, trong số này có một số vẫn còn thơ sinh.
</p>
<p class="regular_content">
      Dự án Tiến Bước tuy được hoàn tất một cách tốc bách, những vẫn hoàn chỉnh. Chỉ vọn vẹn trong  vòng một tháng trời, nhóm Tiến Bước đã thàh công trong việc làm trang mạng, kêu gọi sự giúp đỡ của nhiều ân nhân, mua những đồ đã hứa và chuyên chở mọi món quà tới mọi em trong hai cô nhi viện. Dù dự trù chỉ có $1000 đô Canada, cuối cùng tổng số quyên được là $1940 đô, coi như là gần gấp hai số dự định. Ngoài tiền mướn xe và tiền xăng – tổng cộng là $100, ban tổ chức đã gánh lấy mọi chi phí khác nhằm mục đích là dành mọi số tiền quyên được cho các em. Như được thấy trong mọi tấm hình, các em rất vui mừng và cảm xúc khi nhận được những món quà đã mang lại.
	  </p>
<p class="regular_content">
      Qua những lần liên lạc với những vị đã coi sóc hai cô nhi viện, họ đều muốn gởì lời chân thành cảm tạ mọi ân nhân đã có nhã ý, dù đang gặp khó  khăn trong vấn đề kinh tế cũng không ngần ngại rộng lòng cho một món quà nhỏ để mang lại một nụ cười cho các em. Dựa trên sự rộng lượng của những ân nhân và những nét mặt tươi rói của những em trong viện mồ côi, dự án Tiến Bước 2008 coi như đã được kết thúc một cách mỹ mãn.
	  </p>';
	$front_page_heading = 'Giáng Sinh 2010 Dự Án Tình Thương';
	$page_body = '<p class="regular_content">Nhân dịp Giáng Sinh là lúc bao nhiêu gia đình sum họp để đón chào một năm mới, ban tổ chức của nhóm Tiến Bước muốn thừa cơ hội làm một việc tốt để mang lại một nụ cười và một niềm hy vọng cho những trẻ em vô tội, nhưng vì vô phước nên đã sinh ra hoặc đã trưởng thành trong hoàn cảnh khốn đốn nên nay đã thành cô nhi.
	<p class="regular_content">Xin quí vị hãy thường xuyên quay về trang này để theo dõi tiến trình của Tiến Bước, về mặt tài chánh cũng như về vấn đề tiến triển qua những lãnh vực khác mà các em cần.</p>
	<p class="regular_content">Riêng về công việc hiện tại, chúng tôi sẽ nhận sự giúp đỡ cho tới ngày 13 tháng 12 năm nay mà thôi. Xin cám ơn lòng tốt của quí vị.</p>';
	$mission = '<p class="regular_content">Ban tổ chức lấy tên Tiến Bước cho dự án này dựa trên câu truyện Vô Gia Đình của Hector Malot. Câu truyện này tả một đứa bé mồ côi phiêu lãng trên hè phố tại Âu Châu làm nghề hát rong để nuôi thân. Trong những lúc bần cùng nhất, trong những lúc đói rét và mệt lả tới nỗi em chỉ muốn ngả lưng trút hết gánh nặng của cuộc đờì, người thầy yêu dấu của em đã kiên cường khuyến khích và thúc giục: “Các con, can đảm lên, hãy tiến bước, dù chỉ một bước mà thôi.”</p><p class="regular_content">Hai chữ “Tiến Bước” ám chỉ cho tiến từng bước, đã bao hàm mọi chủ đích của dự án này.  Đây chỉ là một bước nho nhỏ chúng tôi làm để đóng góp cho những em mà có lẽ đã bị hắt hủi nhất trong xã hội. Đối với những người chúng tôi gởi trang này tới, chúng tôi cũng xin họ tiến một bước nho nhỏ, chỉ cần thêm một người có lòng rộng lượng trong khả năng của họ để mua một món quà cho một em, như vậy là đủ rồi. Còn đối với các em mồ côi được nhận lãnh món quà, chúng tôi cũng muốn cho các em biết rằng trên bước đường của chúng tôi và trong sự vui vẻ của chúng tôi, chúng tôi vẫn nhớ đến các em và mời gọi các em hãy can đảm lên để tiến bước đến con đường tương lai. Chúng tôi chỉ muốn mang lại cho các em một nụ cười trong mùa Giáng Sinh này mà thôi. Chỉ cần có một em nở một nụ cười an ủi là dự án của chúng tôi đã được hoàn hảo.</p>';
	$about ='<p class="regular_content">Tiến Bước là một dự án mua đồ ăn hoặc đồ dùng để giúp đỡ hai viện mồ côi tại Việt Nam. Hiện tại, chúng tôi chỉ chuyên chú quyên đồ cho dịp Giáng Sinh này thôi. Tất cả món đồ và tiêu chuẩn của từng món đều theo sự phẩm ước của người săn sóc những viện mồ côi này đã nêu ra. Để làm cho giản dị, chúng tôi chỉ quyên một món tiền tương đương với từng món vật, còn người tại Việt Nam sẽ lo dịch vụ đi mua và gói những món đồ này để mang vô cho các em.</p><p class="regular_content">Nói về viện mồ côi, chỗ thứ nhất là viện mồ côi Nam Hưng nằm ở bờ rìa của thành phố Sài Gòn. Chỗ này gồm có khoảng 40 em, từ thơ sinh tới 18 tuổi và được những bà sơ dòng Đa Minh coi sóc. Các em tại đây được đi học. Đồ ăn tuy tương đối được nhưng không có đủ dinh dưỡng cho lắm. Thịt cá thiếu thốn mọi điều. Thiên Bình, viện mồ côi thứ nhì thì cực khổ hơn nhiều. Cách Sài Gòn khoảng một tiếng đồng hồ đi xe máy, trại này gồm có khoảng trên 130 em và con số này vẫn còn đang phát triển. Trại này nghèo đói và thiếu thốn rất thảm thương.  Không những vậy, ngoài việc không đủ cơm ăn áo mặc, các em còn phải làm ruộng , làm vườn.  Công việc này  một là để làm kinh doanh cho viện và hai là để lấy thực phẩm nuôi các em hàng ngày. Theo hiển nhiên, thực đơn của các em thường ngày là rau quả. Trong một năm, những ngày có thịt có cá chắc chỉ đếm đủ trên đầu ngón tay.</p>';
	$team='Du Đặng<br>Hải Đoàn<br>Darcy Higgins<br>Arun Kumar<br>Hà Lê<br>Phong Lê<br>Thanh Nguyễn<br>Hiếu Phạm<br>Hưng Phạm<br>Thảo Phạm<br>Uyên Trần<br>Vũ Trần<br>Tam Vo';
	$contact = '<p class="regular_content">Nếu quý vị có ý kiến, xin vui lòng liên lạc địa chỉ email sau đây. <br><br>Email : onemorestepvietnam@gmail.com</p>';
	$purchase_gift_msg = 'Xin vui lòng điền vào tên và số địa chỉ email của quý vị nếu quý vị muốn được nhắn tin khi hình ảnh của chuyến đi hai cô nhi viện được đưa lên trên trang web. <b>Mua bằng Visa bảo đảm an toàn qua hệ thống Pay Pal.</b>';
	$thank_you = 'Chúng tôi xin chân thành cám ơn sự đóng góp quý báu của quý ân nhân .  Những món quà của quý vị là những niềm an ủi, và là sự cổ vũ để các em cô nhi can đảm “tiến bước” trên con đường tương lai.<p>Quý vị nhớ trở lại đây sau ngày 25 tháng mười hai, để xem những hình ảnh phát quà cho các em</p><p>Thân Mến,</p><p>Ban Tổ Chức Tiến Bước</p>';
	$finance = 'Đây là bản báo cáo tường tận về tài chánh của dự án mua quà Noen.  Quý vị có thể lưu trữ và xem những tài liệu sau đây bằng cách ấn vào tấm hình nhỏ bên trái.';
	$finance_bs = 'Dự án Mua Quà 2008 - Bản Báo Cáo Tài Chánh';
	$donors = 'Sau đây là danh sách các vị ân nhân đã đóng góp cho dự án Quà Tình Thương';
	$finance_breakdown = 'Danh Sách Những Món Quà';
}
else {
	$news1_headline ='Every Step Counts - Our Story';
	$news1_body = '
	<p class="regular_content">
  OneMoreStep started in 2008 as a one time gift drive to help out needy kids in two overlooked orphanages in remote corners in Vietnam. With the amount of positive impact and support from friends and community, the gift drive grew into an annual fundraiser. We repeated our efforts in 2009 and increased our scope to also provide assistance to two storm stricken schools in Hue province of Vietnam. Your continued generosity and support in the past two years has translated into better nutrition, books, toys, medical instruments, essential transportation, school supplies, small gifts and most important of all HOPE for the orphans. Together, our efforts have gone a long way in improving the lives of these kids in many ways and we humbly say “Thank You”.
 </p>
 <p class="regular_content">
Though we have made concrete change in these two orphanages, live has not changed much in rest of poverty stricken Vietnam. A repressive regime with rampant corruption, unequal income distribution, very limited access to quality healthcare or clean drinking water, bouts of tuberculosis, malaria and cholera are everyday realities for the Vietnamese population. To add to their misery, typhoons and floods constantly ravage large parts of this country like hurricane Ketsana in 2009. In year 2010 alone, Vietnam endured three storms and lost more than 20 people and more than 2million people suffered the devastation. In this entire melee the orphanages suffer the most. At OneMoreStep, we wish we can do more to help these victims and we will be launching more projects to do our little bit.
  </p>
 <p class="regular_content">
This year we are back with the now familiar gift giving drive. The items reflect the most urgent need at the orphanages and we hope you will open your heart and continue your support and also spread the word. Seeing the positive impact and the pictures from Vietnam, we have a few more wonderful members who have joined our team and are contributing their time and effort to this cause. Together, we believe that change is possible; One Small Step at a Time !!

    </p>';
	$news2_headline ='A Focus on Education';
	$news2_body = '<p class="regular_content">This year, One More Step lends our focus to educational initiatives for schools in Viet Nam that just don&rsquo;t have the resources to succeed. Global education is one of the <a href="http://www.un.org/millenniumgoals/education.shtml">United Nations Millennium Goals</a> and its absence is often a sign of poverty. In many of Viet Nam&rsquo;s poor communities, young children and orphans are fortunate to have access to classrooms. Unfortunately, these classrooms are often underfunded,  substandard, and lack many basic resources.  That&rsquo;s where One More Step comes in.
</p>
<p class="regular_content">
We are in our second year of providing <a href="gift#school-supply">school supplies</a> to orphanages in that country.  Books provide educational opportunities that expand not only literacy opportunities but also the imagination of kids that may not have strong connections to family or have had a rough start to life.
</p>
<p class="regular_content">
This year we also work to support the <a href="Hue_Binh_Dien.php">Hue and Binh Dien Schools</a> for children in poverty, schools initiated for children who have no access to government-sponsored education.  We have set up an <a href="gift#education-fund">Educational Fund</a> which, if our donation goal is met, will provide sustainable year-round funding for a school that is in great need, additionally affected by recent typhoons.
</p>';
	$news3_headline ='Moving Ahead After Two Years of Success';
	$news3_body = '
	<p class="regular_content">
Based on the work of nine young people spanning two different continents, and the incredible generosity of a large number of donors, One More Step (OMS) was launched in late 2008 with incredible success. OMS was originally conceived to offer aid in the form of needed food, school supplies and toys to orphans in dire circumstances in Vietnam. In its first year, OMS focused on two orphanages in Vietnam, Thien Binh and Nam Hung, which together oversee 170 children, a significant portion of whom are infants.
</p>
<p class="regular_content">
With rapidity from the initial conception, the building of the website, the coordination of logistics, all the way to making deliveries, the first project in 2008 was accomplished within a month. Originally targeted for $1000 equivalent in items purchase, the actual accrued to $1940, almost doubling the target! With the exception of $100 in transportation costs, OMS members absorbed all costs. All gifts were received with great joy and gratitude, as can be seen in the photographs. 
</p>
<p class="regular_content">
The same happened again last year at a larger scale.  With a renewed focus in education, we were able to provide learning materials to the orphanages along with sustainable support for two schools, Hue and Binh Dien, to last the year.  Thanks to our generous donors, One More Step last year exceeded our goals and raised $1610 for these schools and $1544 for the orphanages.
</p>
<p class="regular_content">
The children and the women running these two orphanages have expressed their thanks to all the donors who, despite difficult economic circumstances, have remained generous. The smiles on the children’s faces when receiving gifts and education is the surest proof of our success and makes us feel accomplished.
</p>
';
	$front_page_heading='Christmas 2010 Gift Drive';
	$page_body = '<p class="regular_content">Christmas is a time to give of yourself, to those you love and to those in need.</p>

<p class="regular_content">That is why we are asking you to take that extra step this Christmas to donate much needed food, toys and school supplies to orphans and poor children living without basic necessities in Viet Nam. Help an orphan take their next step towards a healthier livelihood by purchasing an item or two. Explore our site and news items to find out more about our initiative, and make your donation using our online catalogue above.</p>

<p class="regular_content">The last day that we accept donations is <b>Monday, December 13th</b>. Check back often and tell your friends!</p>
';
	$mission = '<p class="regular_content">One More Step is a group of young people in Canada and Viet Nam who have a connection to the Thien Binh and Nam Hung orphanages near Saigon.  About 170 children live at these orphanages.</p><p class="regular_content">Through intimate knowledge of these two orphanages, we became aware of the specific needs of poor and neglected children living not just in orphanages but in penurious conditions throughout Vietnam. Though originally conceived to help orphans, we have extended our networks and our mission to help create sustainable socio-economic development and to improve access to health and education of underprivileged children in impoverished and remote regions of Vietnam by means of financial, material and medical assistance. The mean by which we attain this goal is through targeted aids and products, oftentimes specifically requested by our contacts in these regions.  One More Step further distinguished itself amongst similar organizations in our frugal cost structure. As project last year indicates, members often absorb miscellaneous charges to ensure maximum delivery of donor contribution. We intend to maintain this tradition.</p><p class="regular_content">Our name comes from a once famous french novel Sans Famille by Hector Malot. It describes an orphan being bought from his foster parents by an old, but kind and poor wandering showman. The boy was forced by circumstances to learn the trade and it was his destiny to wander with the old man from city to city performing on the streets for sporadic fees.</p><p class="regular_content">The story describes the ordeals of surviving in the street, of living in dire poverty, of being weighed down by fatigue, hunger and homelessness. And in these moments of intense suffering, when the company\'s will seemed to waned to the edge in a way that was nearly incapacitating, their master would gather the courage to urge them on...,<br>"Just one more step..."</p><p class="regular_content">We are looking for one more donor, just one more act of kindness, as any development always begins and move forward with just one more step. The orphans we are here to help are taking one more step, so let\'s take ours.</p>';
	$about ='<p class="regular_content">Facing an economic crisis, many Canadians are still being hit hard with job losses and cuts to savings. Viet Nam continues to face further difficulties like most in the third world, who faced a food crisis due to mismanaged global economies and flooding caused by the increased strength and occurence of typhoons. With many young people in Canada having luxuries like computers, cars and cell phones, these kids are still looking for the basics.
	</p>
	<p class="regular_content">
Purchasing an item from One More Step’s online catalogue ensures that money for those items will be sent directly to Viet Nam where supplies will be purchased directly for the <a href="./Xuan_Tam.php">Xuan Tam</a> and <a href="./Nam_Hung.php">Nam Hung</a> orphanages and support our education projects throughout 2011. Your Canadian dollars go a long way in Viet Nam (one dollar worth 20 thousand Vietnamese Dong), as we purchase items in Viet Nam from local markets to support childrens´ needs.
	</p>
	<p class="regular_content">
We strive to reach a level of donations that will make a significant impact in the daily lives of those less fortunate, and to give you the opportunity to make a Christmas gift that will make a real, practical difference. The results of last year´s project showed many smiles, and that keeps us going.
</p>
<p class="regular_content">
Please leave us any comments or feedback below or visit our <a href="./contact.php">Meet the Team</a> page to find out more about the One More Step team</p>';
	$team='Du Đặng<br>Hải Đoàn<br>Darcy Higgins<br>Arun Kumar<br>Hà Lê<br>Phong Lê<br>Thanh Nguyễn<br>Hiếu Phạm<br>Hưng Phạm<br>Thảo Phạm<br>Uyên Trần<br>Vũ Trần<br>Tam Vo';
	$contact='<p class="regular_content">If you have any concerns or feedback, please do not hesitate to contact us.<br><br>Email : onemorestepvietnam@gmail.com</p>';
	$purchase_gift_msg = 'Please provide us with your name and email if you would like to receive notifications of when the photos of the present delivery are made available online. <br>***We are unable to issue tax receipts for this project. <b>Visa purchases are secured via Pay Pal</b>.';
	$thank_you = '<p class="regular_content">We would like to express our sincere gratitude for your generosity.  Thank you for taking that important step to encourage these forgotten children to take “one more step” towards a brighter future.</p><p class="regular_content">Be sure to check back after Dec 27th to view the images of the children who benefited from your donation. </p><p class="regular_content">Sincerely,</p><p>The organizers of One More Step</p>';
	$finance = 'Below are financial reports for the Christmas Gift Drives.  Please click on the icon to the left to download the excel files.';
	$finance_bs = 'Christmas Project 2008 Blance Sheet';
	$donors = 'Below is the list of donors who supported our Christmas Gift Drives';
	$finance_breakdown = 'Items Cost Breakdown';
}



function shuffle_with_keys(&$array) {
	$aux = array();
	$keys = array_keys($array);
	shuffle($keys);
	foreach($keys as $key) {
		$aux[$key] = $array[$key];
		unset($array[$key]);
	}
	$array = $aux;
}


//$fund = $items['education-fund'];
//unset($items['education-fund']);
//shuffle_with_keys($items);
//$items['education-fund'] = $fund;
//$items = array_reverse($items, true);
?>
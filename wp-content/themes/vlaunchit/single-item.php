<?php get_template_part('header')?>


<div id="content-inner-wrap">
     <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php //$post_meta = get_post_custom($post->ID);
          $remaining_str = get_field('item_remaining');
          $remaining = $remaining_str?intval($remaining_str):0;
          $available = $remaining; //the qty available for purchase (exclude qty already in basket
           if (!empty($_SESSION['basket'][$post->ID]['ItemQty'])){
                $available -= intval($_SESSION['basket'][$post->ID]['ItemQty']);
           }

    ?>
    <div class="ItemDesc">
	    <div class="left_column float_left" id="itemImage">
	        <img src="<?php the_field('item_main_photo') ?>" width="440px">
	    </div>
	    <div class="right_column float_right">

	        <h1 class="float_left"><?php the_title(); ?></h1>
	        <h1 class="float_right">$<?php the_field('item_price') ?></h1>
	        <div id="item_description_wrapper"><p class="regular_content clear"><?php the_content(); ?></p></div>

	        <?php if ($available > 0) : ?>
                <div class="submit_form grey_font" id="AddItem">
                    <form name="frmOrder" id="frmOrder" method="post" action="./ajax/addtobag.php">
                        <input type="hidden" name="ItemCode" id="ItemCode" value="<?php the_ID(); ?>" />
                        <input type="hidden" name="ItemName" id="ItemName" value="<?php the_title(); ?>" />

                        <?php if ($remaining > -1) { ?>
                            <?php _e("[:en]Quantity[:vi]Số Lượng");?>:
                            <select size="1" name="ItemQty" id="ItemQty" tabindex="0">
                                <?php for($i=1;$i<=$available;$i++){

                                    echo '<option>'.$i.'</option>';
                                    }
                                ?>

                            </select>
                            <br />
                            <a class="btn" id="btn-add-gift" href="#">
                                <div class="btn-left-edge"></div>
                                <div class="btn-center"><span><?php _e("[:en]Add to Gift Bag[:vi]Chọn Quà");?></span></div>
                                <div class="btn-right-edge"></div>
                            </a>

                        <?php }?>
                        <div id="message">

                        </div>
                    </form>
                </div>
	        <?php endif; ?>

	        <!--<div id="Status">

	            <span class="TargetQty grey_font"> /<?php /*the_field('item_quantity');*/?></span> <span class="QtyRemaining green_font"><?php /*echo $remaining*/?></span>
	            <label class="remaining grey_font"><?php /*_e("[:en]Left[:vi]Còn"); */?></label>
	        </div>-->

	    </div>
    </div>


    <?php endwhile; endif; ?>
</div><!--cs wrap-->
<script type="text/javascript">
    var $j = jQuery.noConflict();
    $j(document).ready(function($) {
        $("#btn-add-gift").click(function(e){
            e.preventDefault();
            var qty = $("#ItemQty").val();
            $("#btn-add-gift").addGiftToBasket(
                {
                    divToChange: "#message",
                    lang:'<?php echo qtranxf_getLanguage();?>',
                    itemCode:$("#ItemCode").val(),
                    itemURL:'<?php echo get_post_permalink($post->ID);?>',
                    itemName:$("#ItemName").val(),
                    itemQty:qty
                });
        });

    });
</script>
<?php get_template_part('footer')?>



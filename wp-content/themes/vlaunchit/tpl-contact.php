<?php
/*
 * Template Name: Contact
 * @package WordPress
 * @subpackage citi
 */?>

<?php get_template_part('header')?>
<div id="content-inner-wrap">
     <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <h1><?php the_title(); ?></h1>

            <div class="post" id="post-<?php the_ID(); ?>">
                <div class="entry">
                    <div style="clear:left;margin-top:10px;">
                        <p>
                            <?php _e("[:en]If you have any concerns or feedback, please do not hesitate to contact us. [:vi]Xin vui lòng liên lạc nhóm Tiến Bước tại địc chỉ email.")?>
                        </p>
                        <p> Email : <a href="mailto:info@onemorestep.ca" target="_blank">info@onemorestep.ca</a></p>
                    </div>
                    <h2><?php echo get_post_meta($post->ID,'current_year_heading_'.qtranxf_getLanguage(),true);?></h2>
                    <div class="team-photo">
                         <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                              the_post_thumbnail('large');
                            } ?>
                    </div>
                    <?php the_content();?>

                </div><!--entry-->
            </div><!--post-->
       <?php endwhile; endif; ?>
</div><!--cs wrap-->

<?php get_template_part('footer')?>
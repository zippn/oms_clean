<div id="side_menu_wrapper">

    <!--<span class="top_left_corner_green"></span>-->
    <!--<span class="top_right_corner_green"></span>-->
    <div id="side_menu_wrapper_inner">

        <!--<span id="checkout"><a href="<?php /*echo get_post_permalink(1097);//get the checkout page */?>"><img src="<?php /*bloginfo('stylesheet_directory');*/?>/images/giftbag.png" border="0">View Gift Bag </a></span>
-->

        <div id="scholarship_panel">
            <h3><?php _e("[:en]Scholarship Fund[:vi]Quỹ học bổng");?></h3>
            <ul class="side_content">
                <li class="side_content">&raquo; <a href="<?php echo get_permalink(SCHOLARSHIP_PAGE_ID);?>"><?php _e("[:en]About the Program[:vi]Về Dự Án");?></a></li>
                <li class="side_content">&raquo; <a href="<?php echo get_permalink(SCHOLARSHIP_STUDENT_PROFILES);?>"><?php echo get_the_title(SCHOLARSHIP_STUDENT_PROFILES);?></a></li>
                <!--<li class="side_content">&raquo; <a href="<?php /*echo bloginfo('home')*/?>/gift/?gift=1112"><?php /*_e("[:en]Donate[:vi]Đóng Góp");*/?></a></li>-->
            </ul>
        </div>

        <div id="gift-receipient">
            <h3><?php _e("[:en]Quick Links[:vi]Thông Tin");?></h3>

                <ul class="side_content">
                    <li class="side_content">&raquo; <a href="<?php echo get_permalink(PAST_DRIVE_PAGE_ID);?>"><?php _e("[:en]View Past Gift Drives[:vi]Xem Trang Web Gây Quỹ Cũ");?></a></li>
                    <li class="side_content">&raquo; <a href="<?php echo get_permalink(THE_TEAM_PAGE_ID);?>"><?php _e("[:en]Contact Us[:vi]Liên Lạc");?></a></li>
                    <?php $orphanages = new WP_Query('post_parent=33&post_type=page');?>
                    <li id="gift-receipient-list" class="side_content">&raquo; <a href="#" class="sidebar-level1"><?php _e("[:en]Gift Drive Recipients[:vi]Các nơi được tài trợ");?></a>
                        <ul id="receipient-list" <?php echo get_top_ancestor($post->ID)==33?'':'class="hide"';?>>
                            <?php while($orphanages->have_posts()):
                                $orphanages->the_post();?>
                                    <li class="side_content">&raquo; <a href="<?php the_permalink();?>"><?php the_title();?></a></li>
                            <?php endwhile;?>
                        </ul>
                    </li>
                </ul>

        </div>


         <div>

            <div class="statusbars">
                <h3><?php _e("[:en]Gift Drive[:vi]Quà Giáng Sinh");?></h3>
                <div id="progressbar"></div>
                <?php _e("[:en]Fundraising Target[:vi]Mục Tiêu Gây Quỹ");?>: <span class="side_emphasized">$<?php echo GIFT_DRIVE_FUNDRAISING_TARGET;?></span> <br>
                <?php _e("[:en]End Date[:vi]Ngày Cuối");?>: <span class="side_emphasized"><?php echo GIFT_DRIVE_END_DATE?></span></h3>
            </div>
        </div>

        <div>
            <h3><?php _e("[:en]Scholarship Fund[:vi]Quỹ Học Bổng");?> </h3>
            <div class="statusbars">
                <div id="education_progressbar"></div>
                <?php _e("[:en]Fundraising Target[:vi]Mục Tiêu Gây Quỹ");?>: <span class="side_emphasized">$<?php echo SCHOLARSHIP_FUNDRAISING_TARGET;?></span> <br>
                <?php _e("[:en]End Date[:vi]Ngày Cuối");?>: <span class="side_emphasized"><?php echo SCHOLARSHIP_END_DATE;?></span></h3>
            </div>
        </div>

        <span id="facebook-like">
            <iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fonemorestep.ca&amp;width=60&amp;layout=box_count&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=65&amp;appId=171453632872821" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:60px; height:65px;" allowTransparency="true"></iframe>
         </span>
         <span id="twitter">
             <a class="twitter-share-button"
                href="https://twitter.com/share"
                data-url="http://onemorestep.ca"
                data-via="takeonemorestep"
                data-text="Great Xmas project - Gift Drive for orphans in Vietnam #onemorestep"
                data-count="vertical">
                 Tweet
             </a>
            <script type="text/javascript">
                window.twttr=(function(d,s,id){var t,js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return}js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);return window.twttr||(t={_e:[],ready:function(f){t._e.push(f)}})}(document,"script","twitter-wjs"));
            </script>

         </span>
<div>
	<span>
	<!-- Begin MailChimp Signup Form -->
<!--<link href="http://cdn-images.mailchimp.com/embedcode/slim-081711.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#73a332; clear:left;}
	#mc_embed_signup .button {clear:both; background-color: #9f1f63; border: 0 none; border-radius:4px; color: #FFFFFF; cursor: pointer; display: inline-block; font-size:15px; font-weight: bold; height: 32px; line-height: 32px; margin: 0 5px 10px 0; padding:0; text-align: center; text-decoration: none; vertical-align: top; white-space: nowrap; width: auto;}
#mc_embed_signup .button:hover {background-color:#8dc63f;}
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="http://onemorestep.us4.list-manage.com/subscribe/post?u=3c7bc711d1d57e4ce0f5cb4f3&amp;id=035a7c89f9" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
<h3>	<label for="mce-EMAIL">Subscribe to Our Newsletter</label></h3>
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
	<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
</form>
</div>
-->
<!--End mc_embed_signup-->
</span>
</div>


     </div>
</div>
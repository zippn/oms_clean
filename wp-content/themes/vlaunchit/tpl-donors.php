<?php
/*
 * Template Name: Donor List
 * @package WordPress
 * @subpackage citi
 */?>

<?php get_template_part('header')?>
<div id="content-inner-wrap">
     <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <h1><?php the_title(); ?></h1>

            <div class="post" id="post-<?php the_ID(); ?>">
                <div class="entry">
                    <?php the_content(); ?>

                    <?php $donors = new WP_Query(array('cat'=>'4','orderby' => 'modified', 'order' => 'ASC' ));
                        while($donors->have_posts()):$donors->the_post();?>
                         <h2><?php the_title();?></h2>
                         <?php the_content();?>
                    <?php endwhile;?>
                </div><!--entry-->
            </div><!--post-->
       <?php endwhile; endif; ?>
</div><!--cs wrap-->

<?php get_template_part('footer')?>
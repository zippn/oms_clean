<?php
/*
 * Template Name: Scholarships
 * @package WordPress
 * @subpackage oms
 */?>

<?php get_template_part('header')?>
<div id="content-inner-wrap">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <h1><?php the_title(); ?></h1>
    <div class="post" id="post-<?php the_ID(); ?>">
        <div class="entry">
            <div class="content-left-side">
                <span class="featured"><?php the_post_thumbnail('large');?></span>
                <?php the_content(); ?>
            </div>
            <?php get_template_part("sidebar","students");?>

        </div><!--entry-->
    </div><!--post-->
    <?php endwhile; endif; ?>
</div><!--cs wrap-->

<?php get_template_part('footer')?>
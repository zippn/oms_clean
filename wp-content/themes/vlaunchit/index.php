<?php get_template_part('header')?>

<div class="left_column float_left" >
    <?php
        query_posts('cat=3');
        if (have_posts()) : while (have_posts()) : the_post(); ?>

            <h1><?php the_title(); ?></h1>
            <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                  the_post_thumbnail('home-page');
                } ?>
            <div class="post" id="post-<?php the_ID(); ?>">
                <div class="entry">
                    <?php the_content(); ?>
                </div><!--entry-->
            </div><!--post-->
   <?php endwhile; endif; ?>
</div>
 <div class="right_column float_right ">
     <h2>
         Follow Us On Twitters
     </h2>
        <div class="twitter-wrap horizontal-divider news" id="title-twitter">

            <script src="http://widgets.twimg.com/j/2/widget.js"></script>
            <script>
            new TWTR.Widget({
              version: 2,
              type: 'profile',
              rpp: 2,
              interval: 30000,
              width: 'auto',
              height: 100,
              theme: {
                shell: {
                  background: '#ffffff',
                  color: '#6eb306'
                },
                tweets: {
                  background: '#faf7fa',
                  color: '#4d4b4d',
                  links: '#c51372'
                }
              },
              features: {
                scrollbar: false,
                loop: true,
                live: false,
                behavior: 'default'
              }
            }).render().setUser('lephong').start();
            </script>
        </div>

        <div class="facebook-wrap news">
            <div class="facebook">
                <h2>Follow Us On Facebook</h2>

                <iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Foutonbayst&amp;width=293&amp;colorscheme=light&amp;show_faces=false&amp;border_color=%23ffffff&amp;stream=true&amp;header=false&amp;height=337" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:293px; height:337px;" allowTransparency="true"></iframe>

            </div>
        </div>
       
</div><!--right_column-->



<?php get_template_part('footer')?>



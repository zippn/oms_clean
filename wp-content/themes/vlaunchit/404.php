<?php get_template_part('header-section')?>
<div id="content-section-wrap">
    <div id="content-section" >

        <div class="post" id="post-<?php the_ID(); ?>">

            <div class="entry">
               <h3>We cannot find the page you are looking for.  Please use our navigation menu to locate your page.</h3>
            </div>
        </div>
       
    </div>
</div><!--cs wrap-->
<?php get_template_part('footer-section')?>

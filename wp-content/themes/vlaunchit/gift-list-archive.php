<?php $year = get_post_meta($post->ID,'year',true);
$items = new WP_Query('post_type=item&fundraising_year='.$year);

?>
<?php if($items->have_posts()):?>
<h3><?php _e("[:en]The Gifts[:vi]Các món Quà"); ?> </h3>
      	<div id="archive_gift_list_wrapper">
        	<!--<div class="gift_list_padding"></div>-->

            	<?php
                        $itemCount=0;
                        while($items->have_posts()):
                            $items->the_post();
                            $post_meta = get_post_custom(get_the_ID());
                            ?>
                                <div class="item">
                                    <div class="item_content_wrapper" id="item-<?php the_ID(); ?>">
                                        <a href="<?php the_permalink(); ?>" id="<?php the_ID(); ?>" class="giftlist" style="display: block" rel="history">
                                            <h4><?php the_title(); ?></h4>

                                            <!--<div class="float_left"><p class="gift_item_content">$<?php /*echo $post_meta['price'][0] */?></p></div>
                                              <div class="float_right"><p class="gift_item_content"><?php /*_e("[:en]Qty[:vi]Số Lượng"); */?>: <?php /*echo $post_meta['quantity'][0] ;*/?></p>
                                            </div>-->
                                            <div class="ItemImage">
                                                <?php the_post_thumbnail(); ?>
                                            </div>
                                        </a>

                                    </div>
                                    
								
                                </div>
   						
								<?php $itemCount++;?>
                                <?php endwhile; wp_reset_query();?>



        </div>
        
<?php endif;?>
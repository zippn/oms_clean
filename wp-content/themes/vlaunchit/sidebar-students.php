<div id="inner-sidebar" class="content-right-side">
    <?php
        $current_page_id = $post->ID;
        $students = new WP_Query(array('post_type'=>'student' ));
        if($students->have_posts()):?>
            <ul id="student-list">
                <?php while($students->have_posts()):$students->the_post();?>
                    <li>
                        <a href="<?php the_permalink();?>" title="<?php the_title();?>" <?php echo $current_page_id==$post->ID?'class="active"':''?>>
                            <?php if(has_post_thumbnail()):?>
                                <?php the_post_thumbnail('thumbnail');?>
                            <?php else:?>
                                <div class="place-holder">
                                    <?php  _e("[:en]".get_the_title()."'s photo coming soon[:vi]Hình của ".get_the_title()."sẽ được tải lên");?>
                                </div>
                            <?php endif;?>
                        </a>
                    </li>

                <?php endwhile; wp_reset_query();?>
            </ul>
        <?php endif;?>
</div>

<?php

// The Query
$args = $args = array(
    'post_type' => 'product',
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field'    => 'slug',
            'terms'    => '2015',
        ),
    ),
);
//$the_query =  new WP_Query( $args);
$the_query =  new WP_Query( $args);

//  var_dump($the_query);

// The Loop
if ( $the_query->have_posts() ) {?>


<h3><?php _e("[:en]The Gifts[:vi]Các món Quà"); ?> </h3>
<div id="archive_gift_list_wrapper">
            <?php

    while ( $the_query->have_posts() ) {
        $the_query->the_post();
        $post_meta = get_post_custom(get_the_ID());
        ?>
        <div class="item">
            <div class="item_content_wrapper" id="item-<?php the_ID(); ?>">
                <a href="/gift/?gift=<?php the_ID(); ?>" id="<?php the_ID(); ?>" class="giftlist" style="display: block" rel="history">
                    <h4><?php the_title(); ?></h4>

                    <!--<div class="float_left"><p class="gift_item_content">$<?php /*echo $post_meta['price'][0] */?></p></div>
                                              <div class="float_right"><p class="gift_item_content"><?php /*_e("[:en]Qty[:vi]Số Lượng"); */?>: <?php /*echo $post_meta['quantity'][0] ;*/?></p>
                                            </div>-->
                    <div class="ItemImage">
                        <img style="width:100px;height:auto;" src="<?php echo get_field('thumbnail') ?>"/>
                    </div>
                </a>

            </div>


        </div>

        <?php $itemCount++;?>

        <?php

    }
    wp_reset_query();
    ?>
</div>
<?    }?>

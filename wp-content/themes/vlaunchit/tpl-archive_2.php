<?php
/*
 * Template Name: Archive
 * @package WordPress
 * @subpackage citi
 */?>
<?php
    //query_posts( array ('post_type' => 'products') );

?>

<?php get_header(); ?>
<?php
    wp_reset_postdata();
    $term = wp_get_post_terms(get_the_ID(), 'fundraising_year', array("fields" => "names"));
    $year = $term[0];
   // var_dump($year);
?>
<div id="content-section-wrap">



    <div id="content-section" class="regular">
		<?php get_template_part('font-size-buttons');?>
        <h1>Archive | <?php echo $year.' Gift Drive'?></h1>

        <div class="entry">
            <?php

            $post_id = get_the_ID();// example post id
        //    var_dump($post_id);
                  $post_content = get_post($post_id);
                  $content = $post_content->post_content;
                  the_content(__('Read the rest of this entry &rarr;','yashfa')); 
            ?>
        </div>
        <?php

        // The Query
        $args = $args = array(
            'post_type' => 'product',
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_cat',
                    'field'    => 'slug',
                    'terms'    => $year,
                ),
            ),
        );
        $the_query =  new WP_Query( $args);

      //  var_dump($the_query);

        // The Loop
        if ( $the_query->have_posts() ) {?>


            <h3><?php _e("[:en]The Gifts[:vi]Các món Quà"); ?> </h3>
        <div id="archive_gift_list_wrapper">
            <?php

            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                $post_meta = get_post_custom(get_the_ID());
                ?>
                <div class="item">
                    <div class="item_content_wrapper" id="item-<?php the_ID(); ?>">
                        <a href="<?php the_permalink(); ?>" id="<?php the_ID(); ?>" class="giftlist" style="display: block" rel="history">
                            <h4><?php the_title(); ?></h4>

                            <!--<div class="float_left"><p class="gift_item_content">$<?php /*echo $post_meta['price'][0] */?></p></div>
                                              <div class="float_right"><p class="gift_item_content"><?php /*_e("[:en]Qty[:vi]Số Lượng"); */?>: <?php /*echo $post_meta['quantity'][0] ;*/?></p>
                                            </div>-->
                            <div class="ItemImage">
                                <img style="width:100px;height:auto;" src="<?php echo get_field('thumbnail') ?>"/>
                            </div>
                        </a>

                    </div>


                </div>

                <?php $itemCount++;?>

                <?php

            }
            wp_reset_query();

            /* Restore original Post Data */
            //wp_reset_postdata();
        }?>
            </div>

	 </div>

</div><!--content-section-wrap-->



<?php get_footer(); ?>
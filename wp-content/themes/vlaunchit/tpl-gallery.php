<?php
/*
 * Template Name: Gallery
 * @package WordPress
 * @subpackage citi
 */?>
<?php get_template_part('header')?>
<div id="content-inner-wrap">
     <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div id="breadcrums">
                <a href="<?php bloginfo("url");?>">Home</a> &rsaquo;
                <?php $ancestor_IDs = get_post_ancestors(get_the_ID());
                    $num_of_ancestors = sizeof($ancestor_IDs);?>

                    <?php for($i = 0;$i<$num_of_ancestors;$i++ ):?>
                        <a href="<?php echo get_post_permalink($ancestor_IDs[$num_of_ancestors-1-$i]);?>"><?php echo get_the_title($ancestor_IDs[$num_of_ancestors-1-$i]);?></a> &rsaquo;
                    <?php endfor;?>

            <?php the_title(); ?>
            </div>
            <h1><?php the_title(); ?></h1>
            <?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                  the_post_thumbnail('sectionpage');
                } ?>
            <div class="post" id="post-<?php the_ID(); ?>">
                <div class="entry">

                    <?php the_content(); ?>
                </div><!--entry-->
            </div><!--post-->
       <?php endwhile; endif; ?>
</div><!--cs wrap-->

<?php get_template_part('footer')?>



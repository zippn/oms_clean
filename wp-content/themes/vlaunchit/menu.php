<?php $page = get_top_ancestor($post->ID);?>
<?php if(!is_404()&&!($page==$post->ID && !has_children($page)))://don't display page if this page is a single post page or not found page?>

    <div id="section-menu" class="horizontal-divider">

        <?php
            if(get_post_meta($page ,'main_page',true))://if this page or its ancestor is one of the 5 main section pages?>
                <h2 <?php echo $page==$post->ID?'class="current_page_item"':'';?>><a href="<?php echo get_permalink($page);?>"><?php echo get_the_title($page)?></a></h2>
        <?php endif;?>
        <ul>
            <?php
                echo  wp_list_pages('child_of='. $page . '&title_li=');
            ?>
        </ul>
    </div>
<?php endif;?>
    <div id="bottom-menu" class="horizontal-divider">
        <?php
            //$page_ID = $post->ID;
            $the_query = new WP_Query( 'post_type=page&orderby=menu_order&order=ASC&meta_key=main_page&meta_value=yes' ); //get main section pages?>
        <?php if($the_query->have_posts()):?>
            <ul>
                <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>

                    <?php if($post->ID!=$page)://dont display the current page in this menu list?>
                        <li><a href="<?php the_permalink()?>" title="<?php the_title();?>" alt="<?php the_title();?>"><?php the_title();?></a></li>
                <?php endif;endwhile; ?>
            </ul>
        <?php endif;?>
        <?php wp_reset_postdata();// Reset Post Data?>
    </div>


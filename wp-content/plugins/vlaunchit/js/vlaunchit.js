

(function($){
    var isIE6 = (navigator.appVersion.indexOf("MSIE 6.")==-1) ? false : true;
    var isIE7 = (navigator.appVersion.indexOf("MSIE 7.")==-1) ? false : true;

   /*Global error catching*/
   $.ajaxSetup({
       error:function(x,e){
           if(x.status==0){
           //alert('You are offline!!\n Please Check Your Network.');
           }else if(x.status==404){
           //alert('Requested URL not found.');
           }else if(x.status==500){
           //alert('Internel Server Error.');
           }else if(e=='parsererror'){
           //alert('Error.\nParsing JSON Request failed.');
           }else if(e=='timeout'){
           //alert('Request Time out.');
           }else {
           //alert('Unknow Error.\n'+x.responseText);
           }
       }
   });

    /*Change the font size of a given element.  Used to increase reading content font size*/
    $.fn.changeFontSize = function(options) {
        var defaults = {
           sizeChange: 3, //can be negative or positive int
          targetDiv: "li" //the div containing text for font size change

        };

        var options = $.extend(defaults, options);

        var currentFontSize =$(options.targetDiv).css('fontSize');
        var currentFontSizeNum = parseFloat(currentFontSize, 10);
        var newFontSize = currentFontSizeNum+options.sizeChange;

        if((newFontSize>14) && (newFontSize<30)){
           $.post(getBaseURL()+"../session.php", {text_size: newFontSize}); //update the session to save it across pages
           $(options.targetDiv).css('fontSize',newFontSize);
        }
    }


    /*Retrieve the base URL of the website*/
   function getBaseURL() {
       var url = location.href;  // entire url including querystring - also: window.location.href;
       var baseURL = url.substring(0, url.indexOf('/', 14));
	
	
       if (baseURL.indexOf('http://localhost') != -1) {
           // Base Url for localhost
           var url = location.href;  // window.location.href;
           var pathname = location.pathname;  // window.location.pathname;
           var index1 = url.indexOf(pathname);
           var index2 = url.indexOf("/", index1 + 1);
           var baseLocalUrl = url.substr(0, index2);
	
           return baseLocalUrl + "/";
       }else if (url.indexOf('/oobs_v1') != -1) {
           return baseURL + "/oobs_v1/";
       }
       else {
           // Root Url for domain name
           return baseURL + "/";
       }
	
   }
	

		
   /*
    *Input a date and return a relative "days ago" date
    */
    function relative_time(time_value) {
     var newtime = time_value.replace(',','');
     var values = newtime.split(" ");
     time_value = values[2] + " " + values[1] + ", " + values[3] + " " + values[4];
     var parsed_date = Date.parse(time_value);
     var relative_to = (arguments.length > 1) ? arguments[1] : new Date();
     var delta = parseInt((relative_to.getTime() - parsed_date) / 1000);
     delta = delta + (relative_to.getTimezoneOffset() * 60);
	
     if (delta < 60) {
       return 'less than a minute ago';
     } else if(delta < 120) {
       return 'about a minute ago';
     } else if(delta < (60*60)) {
       return (parseInt(delta / 60)).toString() + ' minutes ago';
     } else if(delta < (120*60)) {
       return 'about an hour ago';
     } else if(delta < (24*60*60)) {
       return 'about ' + (parseInt(delta / 3600)).toString() + ' hours ago';
     } else if(delta < (48*60*60)) {
       return '1 day ago';
     } else {
       return (parseInt(delta / 86400)).toString() + ' days ago';
     }
   }

/*
 *Getting Tweets from Twitter.  Using a proxy file to make sure retweets show up
 */
   $.fn.printTweets = function(options) {
       var defaults = {
           twitterName: "vlaunchit",	//the Twitter user name
           visibleName: "vlaunchit",    //the name to be displayed to viewers
           rotationInterval: 10000,	//inverval between each tweet display
           rotateSpeed:500,
           numOfTweets:4,
           direction:"up",
           easing:"easeOutElastic",
           width:$(this).width(),
           height:$(this).height(),
           currentSlide:0
       };
		
  
       var options = $.extend(defaults, options);
       var currentVisibleTweet=0;
       var feedURL = getBaseURL()+'wp-content/plugins/vlaunchit/proxy.php?url=twitter.com/statuses/user_timeline/'+options.twitterName+'.rss?count='+options.numOfTweets;

       $(this).css({overflow:'hidden', position:'relative'}); //make sure the target div acts as a viewing screen
       $(this).append('<div id="image_real"></div>');
       $image_real=$("#image_real"); //the div that contains the strip of element
       $image_real.css({position:'absolute',top:0,left:0});
		
		
        $.get(feedURL, function(response){
             showTweets(response);
             rotateSwitchTweets("#image_real", "#image_real div",options.numOfTweets,options.height,options.rotationInterval, options.rotateSpeed, options.direction, options.easing, options.currentSlide); //Run function on launch
			
       });
       function showTweets(tweets){
           var tweetID=0;
			
           $(tweets).find('item').each(function() {
               var tweet = $(this);
               var pattern = new RegExp("^"+options.twitterName+": ","g");
               var status = tweet.find('description').text().replace(/((https?|s?ftp|ssh)\:\/\/[^"\s\<\>]*[^.,;'">\:\s\<\>\)\]\!])/g, function(url) {
                   return '<a href="'+url+'">'+url+'</a>';
               }).replace(/\B@([_a-z0-9]+)/ig, function(reply) {
                   return  reply.charAt(0)+'<a href="http://twitter.com/'+reply.substring(1)+'">'+reply.substring(1)+'</a>';
               }).replace(pattern,'');
               var tweetDate = relative_time(tweet.find('pubDate').text());
               $image_real.append('<div class="tweets" id="tweet'+tweetID+'"><a href="http://twitter.com/#!/'+options.twitterName+'" title="Follow Us on Twitter" alt="Follow Us on Twitter" class="name">'+options.visibleName+'</a>&ldquo;'+status+'&rdquo; <small>- <span class="date">'+tweetDate+'</span></small></div>');
               tweetID++;
           });//end of $(tweets).find
            $image_real.find('.tweets').css({width:options.width,height:options.height,display:'block', overflow:'hidden'});
            $("#tweet0").addClass("active"); //make the first element active for the rotation
       };//end of $showTweets
   };//end printTweet function
		
   //Paging  and Slider Function
   rotate = function(options){
       var defaults = {
           objectName: "#image_real",
           pagingName: "#paging a",
           slideDistance:1000,
           speed:3000,
           slideNum:5,
           direction:"left",
           easing:"easeOutElastic",
           activeElement:''
       };
       var options = $.extend(defaults, options);
		
       var image_reelPosition = options.slideNum * options.slideDistance; //Determines the distance the image reel needs to slide
		
       $(options.pagingName).removeClass('active'); //Remove all active class
       if(options.activeElement!="")
       {
           $(options.activeElement).addClass('active'); //Add active class (the $active is declared in the rotateSwitch function)
       }
       //Slider Animation

       switch(options.direction)
       {
           case "up":
               $(options.objectName).animate({top: -image_reelPosition},  options.speed);
               break
           case "left":
               $(options.objectName).animate({left:-image_reelPosition}, options.speed);
               break;
           case "right":
               $(options.objectName).animate({left:+image_reelPosition}, options.speed);
               break;
           case "down":
               $(options.objectName).animate({top:+image_reelPosition}, options.speed);
               break;
           default:
               $(options.objectName).animate({left:-image_reelPosition}, options.speed);
               break;
			
       }

		
   };


   /****Rotation  and Timing Event
    *@objectName the ID of the element (image_reel) containing all the slides
    *@pagingName the ID of the element containing each slide
    *@numOfSlides the count of how many slides are in the reel
    *@slideDistance the length each slide (ie the width of the viewing window)
    *@interval the interval by which the slides are toggled
    *@speed the animation speed of the slide change
    *@direction direction of the animation
    */
   rotateSwitch =  function(objectName, pagingName, numOfSlides, slideDistance, interval, speed, direction, easing, current){
       currentSlide=current;

       var play = setInterval(function(){ //Set timer - this will repeat itself every 7 seconds
           var active = $(pagingName+'.active').next(); //Move to the next paging
           if ( active.length === 0) { //If paging reaches the end...
               active = $(pagingName+':first'); //go back to first
               currentSlide=0;
           }
           else
           {
               currentSlide++;
           }
           rotate({
               objectName: objectName,
               pagingName: pagingName,
               slideDistance:slideDistance,
               speed:speed,
               slideNum:currentSlide,
               direction:direction,
               easing:"easeOutElastic",
               activeElement:active}) //Trigger the paging and slider function
       }, interval); //Timer speed in milliseconds (7 seconds)


      // setTimeout(rotateSwitch(), interval); //Timer speed in milliseconds


       return play;
   };

     /****Fading  and Timing Event
    *@objectName the ID of the element (image_reel) containing all the slides
    *@pagingName the ID of the element containing each slide
    *@numOfSlides the count of how many slides are in the reel
    *@slideDistance the length each slide (ie the width of the viewing window)
    *@interval the interval by which the slides are toggled
    *@speed the animation speed of the slide change
    *@direction direction of the animation
    */
   fadeSwitch =  function(objectName, pagingName, numOfSlides, interval, speed, easing, current, next){
       currentSlide=current;
       nextSlide = next;
       var play = setInterval(function(){ //Set timer - this will repeat itself every 7 seconds
           var active = $(pagingName+'.active').next(); //Move to the next paging
           if ( nextSlide == numOfSlides) { //If paging reaches the end...
               nextSlide=0;
               active = $(pagingName+':first'); //go back to first

           }else if(currentSlide == numOfSlides){
               currentSlide=0;
               nextSlide = currentSlide+1;

           }
            //console.log("fadeout, fadein:"+currentSlide+", "+nextSlide);
           $(objectName+' #slide'+currentSlide).fadeOut(speed,function(){
               $(pagingName).removeClass('active');
               active.addClass('active');
               $(objectName+' #slide'+nextSlide).fadeIn(speed, function(){
                   if(isIE6||isIE7){//hack to sharpen text after fading for IE6 & IE7
                    this.style.removeAttribute("filter");
                   }
                     currentSlide++;
                     nextSlide ++;
               });

           });

       }, interval); //Timer speed in milliseconds (7 seconds)


      // setTimeout(rotateSwitch(), interval); //Timer speed in milliseconds
       return play;
   };

      //Paging  and Slider Function
   fade = function(options){
       var defaults = {
           objectName: "#image_real",
           pagingName: "#paging li",
           speed:3000,
           fadeOutSlide:0,
           fadeInSlide:1,
           easing:"easeOutElastic"
       };

		 //console.log("fadeout, fadein:"+options.fadeOutSlide+", "+options.fadeInSlide);
           $(options.objectName+' #slide'+options.fadeOutSlide).fadeOut(options.speed,function(){
               $(options.pagingName).removeClass('active');
               $(options.pagingName +'#page-'+options.fadeInSlide).addClass('active');
               $(options.objectName+' #slide'+options.fadeInSlide).fadeIn(options.speed, function(){

                   if(isIE6||isIE7){//hack to sharpen text after fading for IE6 & IE7
                    this.style.removeAttribute("filter");
                   }
               });

           });


   };

    /****Rotation for Tweets
        *@objectName the ID of the element (image_reel) containing all the slides
        *@pagingName the ID of the element containing each slide
        *@numOfSlides the count of how many slides are in the reel
        *@slideDistance the length each slide (ie the width of the viewing window)
        *@interval the interval by which the slides are toggled
        *@speed the animation speed of the slide change
        *@direction direction of the animation
        */
       rotateSwitchTweets =  function(objectName, pagingName, numOfSlides, slideDistance, interval, speed, direction, easing, current){
           var currentTweet=current;
            setInterval(function(){ //Set timer - this will repeat itself every 7 seconds
               var activeTweet = $(pagingName+'.active').next(); //Move to the next paging
               if ( activeTweet.length === 0) { //If paging reaches the end...
                   activeTweet = $(pagingName+':first'); //go back to first
                   currentTweet=0;
               }
               else
               {
                   currentTweet++;
               }
               rotate({
                   objectName: objectName,
                   pagingName: pagingName,
                   slideDistance:slideDistance,
                   speed:speed,
                   slideNum:currentTweet,
                   direction:direction,
                   easing:"easeOutElastic",
                   activeElement:activeTweet}) //Trigger the paging and slider function
           }, interval); //Timer speed in milliseconds (7 seconds)
       };

   $.fn.slideShow = function(options) {
       var defaults = {
           rotationInterval: 10000,	//inverval between each tweet display
           rotateSpeed:500,
           numOfSlides:4,
           direction:"left",
           width:$(this).width(),
           height:$(this).height()
       };
		
       rotateSwitch("#image_real", "#paging a",options.numOfSlides,options.width,options.rotationInterval, options.rotateSpeed, options.direction); //Run function on launch
   }
/*
 * jQuery cursorHover
 * Copyright 2010 Jake Lauer with Clarity Design (isthatclear.com)
 * Released under the MIT and GPL licenses.
 */
	
           $.fn.cursorHover = function(options) {
				
               var defaults = {
                   outerImage: "",
                   innerImage: "",
               fadeSpeed: 300,
               align: "bottom",
               zalign: "bottom",
               elementWidth:$(this).css('width'),	//default to the this div's width
               elementHeight:$(this).css('height'),//default to the this div's height
               textLineHeight:$(this).css('height'), //the default line height of the text is the same as the height of its parent div
               textAlignment:"center",	//the default aligment of text is set to center
               textWidth:$(this).css('width'),//the default text width
               textPaddingVert:10,
               textPaddingHorz:5,
               horizontalElementsFloat:"none", //set the float to a default value for multiple elements
               cornerWidth:10,
               selected:''	//if this is set, then use this to set the current active element
           };
           var options = $.extend(defaults, options);
           return this.each(function() {
               //Set the layers
               var linkObject;
               var linkExists;
               var linkText;
               var thisHeight = $(this).css('height');
               var thisHeight = $(this).css('height');
               var thisTextWidth = $(this).width()-options.textPaddingHorz*2; //the width of the text is the width of the element minus the paddings
               if ( $(this).find('a') ) {
                   linkObject = $(this).find('a');
                   linkText = linkObject.text();
                   linkObject.html('<div class="topLayer"></div>'); //first set the top layer to be the link text
                   linkExists = true;
               }
               else
               {
                   linkObject=$(this);
               }
               linkObject.append('<div class="aInner"/>');//the middle layer will be the inner image
               linkObject.append('<div class="aTop"/>');//the top layer will be the outer image
				
               $(this).css({position:"relative"});
               var aInner =$(this).find('.aInner');
               var aTop = $(this).find('.aTop');
               var toplayer = $(this).find('.topLayer');
				
               switch (options.zalign)
               {
               case "bottom":
                   var innerImageZ = 2;
                   var outerImageZ = 1;
                   break;
               case "top":
                   var innerImageZ = 1;
                   var outerImageZ = 2;
                   break;
               }
				
				
               //aInner.wrap('<div class="border">');
               //$(this).find('.border').css({position:"absolute",top: options.innerBorder,left:options.innerBorder,width:$(this).css('width'), height:$(this).css('height'),overflow: "hidden", zIndex:innerImageZ});
               aInner.css({position:"absolute",top: 0,left:0,width:$(this).css('width'), height:thisHeight,overflow: "hidden", zIndex:innerImageZ,opacity:0});
               aTop.css({position:"absolute", top:0, left:0, zIndex:outerImageZ});
				
               //forming round corners for the outer element
               var middleWidth = $(this).width()-(options.cornerWidth*2);
               aTop.html('<span style="display:block; float:left; height:'+thisHeight+'; width:'+options.cornerWidth+'px; background:#480a60 url('+options.outerImage+') no-repeat left -13px"></span>');//left corners
               aTop.append('<span style="display:block; float:left; height:'+thisHeight+'; width:'+middleWidth+'px; background:#480a60 url('+options.outerImage+') repeat-x -'+options.cornerWidth+'px -13px"></span>');//middle section of element
               aTop.append('<span style="display:block; float:left; height:'+thisHeight+'; width:'+options.cornerWidth+'px; background:#480a60 url('+options.outerImage+') no-repeat right -13px"></span>');//right corners
				
               //forming round corners for the inner element
               aInner.html('<span style="display:block; float:left; height:'+thisHeight+'; width:'+options.cornerWidth+'px; background:url('+options.innerImage+') no-repeat left -13px"></span>');//left corners
               aInner.append('<span style="display:block; float:left; height:'+thisHeight+'; width:'+middleWidth+'px; background:url('+options.innerImage+') repeat-x -'+options.cornerWidth+'px -13px"></span>');//middle section of element
               aInner.append('<span style="display:block; float:left; height:'+thisHeight+'; width:'+options.cornerWidth+'px; background:url('+options.innerImage+') no-repeat right -13px"></span>');//right corners
				
				
               //add css to the top layer(the text layer)
               if ( linkExists == true ) {
                   toplayer.text(linkText);
                   toplayer.css({zIndex:outerImageZ+3, position:"absolute",  width:thisTextWidth, textAlign:options.textAlignment, paddingTop:options.textPaddingVert, paddingBottom:options.textPaddingVert, paddingLeft:options.textPaddingHorz,paddingRight:options.textPaddingHorz});//set the z-index to be the top layer
					
               }
				
               //set the main section page to be active if one of its children page is current being displayed
               if(options.selected!='' && $(this).hasClass(options.selected))
               {
                   $(this).addClass("current_page_item");
               }
				
               //check if the item is a currently selected, then highlight it
               if($(this).hasClass("current_page_item"))
               {
                   aInner.css({opacity:1});
               }
				
				
               if(!$(this).hasClass("current_page_item")) //do nothing if this element is currently the active element
               {
                   //Fadein on mouse over
                   $(this).mouseenter(function(){
						
                       aInner.stop().animate({
                           opacity: 1
                       }, options.fadeSpeed);
                   });
					
				
                   //Fade out on mouse leave
                   $(this).mouseleave(function(){
                       $(this).find('.aInner').stop().animate({
                           opacity: 0
                       }, options.fadeSpeed);
                   });
                }
           });
           function extractUrl(input) {
               return input.replace(/"/g,"").replace(/url\(|\)$/ig, "");
           }
       };
		
		
	
//console.log();
   $.fn.lavaLamp = function(o) {
   o = $.extend({
               'target': 'li',
               'container': '',
               'fx': 'swing',
               'speed': 500,
               'click': function(){return true},
               'startItem': '',
               'includeMargins': false,
               'autoReturn': true,
               'returnDelay': 0,
               'setOnClick': true,
               'homeTop':0,
               'homeLeft':0,
               'homeWidth':0,
               'homeHeight':0,
               'returnHome':false,
               'autoResize':false,
               'startElement':'',
               'bgImage':'images/side-menu-bg.png'
               },
           o || {});
   // parseInt for easy mathing
   function getInt(arg) {
       var myint = parseInt(arg);
       return (isNaN(myint)? 0: myint);
   }
   if (o.container == '')
       o.container = o.target;
   if (o.autoResize)
       $(window).resize(function(){
           $(o.target+'.selectedLava').trigger('mouseenter');
       });
   return this.each(function() {
       // ensures parent UL or OL element has some positioning
       if ($(this).css('position')=='static')
           $(this).css('position','relative');
       // create homeLava element if origin dimensions set
       if (o.homeTop || o.homeLeft) {
           var $home = $('<'+o.container+' class="homeLava"></'+o.container+'>').css({ 'left':o.homeLeft, 'top':o.homeTop, 'width':o.homeWidth, 'height':o.homeHeight, 'position':'absolute','display':'block' });
           $(this).prepend($home);
       }
       var path = location.pathname + location.search + location.hash, $selected, $back, $lt = $(o.target+'[class!=noLava]', this), delayTimer, bx=0, by=0, mh=0, mw=0, ml=0, mt=0;
       // start $selected default with CSS class 'selectedLava'
       $selected = $(o.target+'.selectedLava', this);
		
       // override $selected if startItem is set
       if (o.startItem != '')
           $selected = $lt.eq(o.startItem);
		
       $selected = $(o.target+'.current_page_item', this);
		
			
       // default to $home element
       if ((o.homeTop || o.homeLeft) && $selected.length<1)
           $selected = $home;
       // loop through all the target element a href tags and
       // the longest href to match the location path is deemed the most
       // accurate and selected as default
       if ($selected.length<1) {
           var pathmatch_len=0, $pathel;
	
           $lt.each(function(){
               var thishref = $('a:first',this).attr('href');
               //console.log(thishref+' size:'+thishref.length);
               if (path.indexOf(thishref)>-1 && thishref.length > pathmatch_len )
               {
                   $pathel = $(this);
                   pathmatch_len = thishref.length;
               }
	
           });
           if (pathmatch_len>0) {
               //console.log('found match:'+$('a:first',$pathel).attr('href'));
               $selected = $pathel;
           }
           //else
               //console.log('no match!');
       }
	
       // if still no matches, default to the first element
       if ( $selected.length<1 )
           $selected = $lt.eq(0);
       // make sure we only have one element as $selected and apply selectedLava class
       $selected = $($selected.eq(0).addClass('selectedLava'));
			
       // add mouseover event for every sub element
       $lt.bind('mouseenter', function() {
           //console.log('mouseenter');
           // help backLava behave if returnDelay is set
           if(delayTimer) {clearTimeout(delayTimer);delayTimer=null;}
           move($(this,'mouseenter'));
       }).click(function(e) {
           if (o.setOnClick) {
               $selected.removeClass('selectedLava');
               $selected = $(this).addClass('selectedLava');
           }
           return o.click.apply(this, [e, this]);
       });
		
       // creates and adds to the container a backLava element with absolute positioning
       $back = $('<'+o.container+' class="backLava"><div class="leftLava"></div></'+o.container+'>').css({'position':'absolute','display':'block','margin':0,'padding':0}).prependTo(this);
		
       $back.find('.leftLava, .cornerLava').css('background-image','url(./wp-content/themes/mediaco/'+o.bgImage+')');
		
       // setting css height and width actually sets the innerHeight and innerWidth, so
       // compute border and padding differences on styled backLava element to fit them in also.
       if (o.includeMargins) {
           mh = getInt($selected.css('marginTop')) + getInt($selected.css('marginBottom'));
           mw = getInt($selected.css('marginLeft')) + getInt($selected.css('marginRight'));
       }
       bx = getInt($back.css('borderLeftWidth'))+getInt($back.css('borderRightWidth'))+getInt($back.css('paddingLeft'))+getInt($back.css('paddingRight'))-mw;
       by = getInt($back.css('borderTopWidth'))+getInt($back.css('borderBottomWidth'))+getInt($back.css('paddingTop'))+getInt($back.css('paddingBottom'))-mh;
       // set the starting position for the lavalamp hover element: .back
       if (o.homeTop || o.homeLeft)
           $back.css({ 'left':o.homeLeft, 'top':o.homeTop, 'width':o.homeWidth, 'height':o.homeHeight });
       else
       {
           if (!o.includeMargins) {
               ml = (getInt($selected.css('marginLeft')));
               mt = (getInt($selected.css('marginTop')));
           }
           $back.css({ 'left': $selected.position().left+ml, 'top': $selected.position().top+mt, 'width': $selected.outerWidth()-bx, 'height': $selected.outerHeight()-by, 'overflow':'visible'});
		
       }
       // after we leave the container element, move back to default/last clicked element
       $(this).bind('mouseleave', function() {
           //console.log('mouseleave');
           var $returnEl = null;
           if (o.returnHome)
               $returnEl = $home;
           else if (!o.autoReturn)
               return true;
				
           if (o.returnDelay) {
               if(delayTimer) clearTimeout(delayTimer);
               delayTimer = setTimeout(function(){move($returnEl,'mouseleave');},o.returnDelay);
           }
           else {
               move($returnEl,'mouseleave');
           }
           return true;
       });
       function move($el,$state) {
			
           if (!$el) $el = $selected;
           if (!o.includeMargins) {
               ml = (getInt($el.css('marginLeft')));
               mt = (getInt($el.css('marginTop')));
           }
           var dims = {
               'left': $el.position().left+ml,
               'top': $el.position().top+mt
               /*'width': $el.outerWidth()-bx,
               'height': $el.outerHeight()-by*/
           };
			
           $back.stop().animate(dims, o.speed, o.fx, function(){
               //document.write(url);
               //var linkEl = $el.find('a');
               //linkEl.css('background-image',url);
			
           });
       };
   });
	
   };
   })(jQuery);

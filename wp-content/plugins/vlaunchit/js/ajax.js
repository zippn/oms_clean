
(function($){

    $.fn.preRecordPayPalPurchase = function(options) {
		var defaults = {
            name:'',
            email:'',
            list_name:1,
            payment:''
		};
		var options = $.extend(defaults, options);
		var html='';
        var return_message = '';
         $.ajaxSetup({async: false}); //set this to make sure $.post is done executing before returning return_message for validateGiftBag function
		$.post(
			// see tip #1 for how we declare global javascript variables
			MyAjax.ajaxurl,
			{
				// here we declare the parameters to send along with the request
				// this means the following action hooks will be fired:
				// wp_ajax_nopriv_myajax-submit and wp_ajax_myajax-submit
				action : 'pre_record_paypal_purchase',
 				// other parameters can be added along with "action"
				name:options.name,
                email:options.email,
                list_name:options.list_name,
                payment:options.payment
			},
			function( response ) {
                if(response.success){
                    return_message= 'success';
                }else{
                    return_message='fail';
                }
			}
		);
         $.ajaxSetup({async: true});
        return return_message;
	}
	
	$.fn.validateGiftBag = function(options) {
		var defaults = {
            lang:'en',
            purchaserName:'',
            purchaserEmail:'',
            list_name:1
		};
		var options = $.extend(defaults, options);
		var html='';
        var return_message = '';

         $.ajaxSetup({async: false}); //set this to make sure $.post is done executing before returning return_message for validateGiftBag function
		$.post(
			// see tip #1 for how we declare global javascript variables
			MyAjax.ajaxurl,
			{
				// here we declare the parameters to send along with the request
				// this means the following action hooks will be fired:
				// wp_ajax_nopriv_myajax-submit and wp_ajax_myajax-submit
				action : 'validate_gift_bag',
 				// other parameters can be added along with "action"
				language:options.lang,
                name:options.purchaserName,
                email:options.purchaserEmail,
                list_name:options.list_name
			},
			function( response ) {
                if(response.success){
                    return_message= response.purchaseId;
                }else{
                    return_message=response.message;

                }
			}
		);
         $.ajaxSetup({async: true});
        return return_message;
	}
    
	$.fn.addGiftToBasket = function(options) {
		var defaults = {
			divToChange: "",
            lang:'en',
			itemCode:1,
            itemURL:'',
            itemName:'',
            itemQty:0

		};
		var options = $.extend(defaults, options);
		var html='';
		$.post(
			// see tip #1 for how we declare global javascript variables
			MyAjax.ajaxurl,
			{
				// here we declare the parameters to send along with the request
				// this means the following action hooks will be fired:
				// wp_ajax_nopriv_myajax-submit and wp_ajax_myajax-submit
				action : 'add_to_gift_bag',
                language:options.lang,
		
				// other parameters can be added along with "action"
				ItemCode : options.itemCode,
                ItemName:options.itemName,
                ItemQty: options.itemQty
			},
			function( response ) {
				
				if(options.divToChange!=''){
                    if(response.success){
                        if(!$("#item-"+options.itemCode).find(".PurchasedBow").length){ //check if item has a bow already
                            $bow_html='<div id="purchased4" class="PurchasedBow">';
                            $bow_html+='<a rel="history" href="'+options.itemURL+'">';
                            $bow_html+='<img border="0" alt="purchased bow" src="http://onemorestep.ca/wp-content/themes/vlaunchit/images/purchased-bow.png"></a>';
                            $bow_html+='</div>';
                            $("#item-"+options.itemCode).append($bow_html);

                        }
                        $(options.divToChange).html('<div class="success">'+response.message+'</div>');
				    }else{
					    $(options.divToChange).html('<div class="error">'+response.message+'</div>');  //reponse is a json object
                    }
				}
			}
		);	
	}

	
	$.fn.removeGiftFromBasket = function(options) {
		var defaults = {
			divToChange: "",
			message:""

		};
		var options = $.extend(defaults, options);
		var html='';

		$.post(
			// see tip #1 for how we declare global javascript variables
			MyAjax.ajaxurl,
			{
				// here we declare the parameters to send along with the request
				// this means the following action hooks will be fired:
				// wp_ajax_nopriv_myajax-submit and wp_ajax_myajax-submit
				action : 'empty_gift_bag'
		
				// other parameters can be added along with "action"
			},
			function( response ) {
				
				if(options.divToChange==''&&response.success){
					$(this).html(response.postID); 
				}else{

					$html = '<p>'+options.message+'</p>' ;

					$(options.divToChange).html($html);  //reponse is a json object	
				}
			}
		);	
	}
})(jQuery);
	
<?php
    $path = $_SERVER['DOCUMENT_ROOT'];
    $product_year = $_GET['year']?$_GET['year']:'2016';
    include_once $path . '/wp-config.php';
    include_once $path . '/wp-load.php';


//get all shop orders
$shop_order_args = array(
    'date_query' => array(
        array(
            'after'     => array(
                'year'  => $product_year,
                'month' => 11,
                'day'   => 20,
            ),

            'inclusive' => true,
        ),
    ),
    'posts_per_page' => -1,
    'post_status' => 'publish',
    'post_type' => 'shop_order',
    'orderby' => 'date',
    'order' => 'DESC'
);
$shop_orders = new WP_Query($shop_order_args);

//get all shop products for this year only
$args = array('post_type'=>'product',
    'post_status'=>'publish',
    'posts_per_page=-1',
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field'    => 'name',
            'terms'    => $product_year,
        )
    ));

$products = new WP_Query($args);


function process_status($order_status){
    //cheque
    if($order_status=='pending'){
        return 'error processing';
    }else if($order_status=='cancelled'){
        return 'cancelled';
    }else{
        return 'completed';
    }
}

?>

<html>
<head>
    <link rel='stylesheet' id='oms-style-bs-css'  href='http://onemorestep.ca/wp-content/plugins/oms/js/vendor/bootstrap/css/bootstrap.min.css?ver=1.0' type='text/css' media='all' />
    <link rel='stylesheet' id='oms-style-bs-theme-css'  href='http://onemorestep.ca/wp-content/plugins/oms/js/vendor/bootstrap/css/bootstrap-theme.min.css?ver=1.0' type='text/css' media='all' />
    <link href="//cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
    <link href="//cdn.datatables.net/buttons/1.1.0/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">


    <script src="//code.jquery.com/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script type='text/javascript' src='http://onemorestep.ca/wp-content/plugins/oms/js/vendor/bootstrap/js/bootstrap.min.js?ver=1.0'></script>
    <script src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="//cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.print.min.js" type="text/javascript"></script>

    <script src="//cdn.datatables.net/plug-ins/1.10.10/api/sum().js" type="text/javascript"></script>

    <script>
        (function($){
            $(function(){
                var table = $("#orders").DataTable({
                    order: [[ 5, "desc" ]],
                    paging:false,
                    dom:"Bfrtip",
                    buttons: [
                        'csv','pdf','print', 'excel'
                    ],
                    drawCallback: function () {
                        var api = this.api();
                        var sum = api.column( 4, {page:'current'} ).data().sum();
                        $("#order-totals-num").html( sum.toFixed(0));
                    }
                });

                var itemsTable = $("#items").DataTable({
                    paging:false,
                    dom:"Bfrtip",
                    buttons: [
                        'csv','pdf','print', 'excel'
                    ],
                    drawCallback: function () {
                        var api = this.api();
                        var ed_inv_sum = parseInt($("#prod-1112-ordered-total").text().substr(2));
                        var ed_qty_sum = parseInt($("#prod-1112-qty-total").text().substr(2));

                        var gift_inv_sum = api.column( 3, {page:'current'} ).data().sum()-ed_inv_sum;
                        var gift_qty_sum = api.column( 5, {page:'current'} ).data().sum()-ed_qty_sum;
                        var ed_total_percent = 100*(ed_inv_sum/ed_qty_sum);
                        var gift_total_percent = 100*(gift_inv_sum/gift_qty_sum);

                        $("#items-inv-sum-gift").html( "("+gift_total_percent.toFixed(2)+"%) "+gift_inv_sum.toFixed(0));
                        $("#items-total-sum-gift").html( gift_qty_sum.toFixed(0));
                        $("#items-inv-sum-ed").html( "("+ed_total_percent.toFixed(2)+"%) "+ed_inv_sum.toFixed(0));
                        $("#items-total-sum-ed").html( ed_qty_sum.toFixed(0));
                        $("#items-inv-total").html((gift_inv_sum+ed_inv_sum).toFixed(0));
                        $("#items-total").html( (gift_qty_sum+ed_qty_sum).toFixed(0));
                    }
                });
                var orderItems = $("#order-items").DataTable({
                    order: [[ 5, "desc" ]],
                    paging:false,
                    dom:"Bfrtip",
                    buttons: [
                        'csv','pdf','print', 'excel'
                    ],
                    drawCallback: function () {
                        var api = this.api();
                        var sum = api.column( 4, {page:'current'} ).data().sum();
                        $("#item-order-totals-num").html( sum.toFixed(0));
                    }
                });
            });
        })(jQuery);
    </script>
    <style>
        #content{
            width:1280px;
            margin:0 auto;
            text-align: center;
            background-color: #8dc63f;
            padding:25px;
        }
        #orders{
            text-align: left;
        }
        .cheque{
            color: #05a0ff;
        }
        .error{
            color:red;
        }
        #message{
            width:300px;
            margin:20px auto;
        }
        #content h1{
            text-align: left;
            color:white;
        }
        #content h1 img{
            margin-right: 50px;
        }
        #orders_wrapper{
            background-color: white;
        }
        #orders_filter{
            margin:20px;
        }
        #content-body{
            background-color: white;
            padding: 25px;
            min-height: 300px;
        }
        .login{
            position: relative;
            top:-300px;
            margin-top: 50%;
        }
        .money{
            text-align: right;
        }
        div.dt-buttons{
            width: 100%;
            text-align: right;
        }
        #order-totals{
            text-align: right;
        }
    </style>
</head>

<body>
<div id="content">
    <header>
        <h1><img src="http://onemorestep.ca/wp-content/themes/vlaunchit/images/logo-white.png" border="0"> OMS Gift Drive <?php echo $product_year;?> Orders</h1>
    </header>
    <section id="content-body">
        <?php if($_POST):?>
            <?php if ($_POST['key']!='0425'):?>
                <div id="message" class="alert alert-danger">Nice Try! That's the wrong key!</div>
                <form action="" method="POST" class="login">
                    <label for="key">Key</label>
                    <input type="text" name="key">
                    <input type="submit" value="Go">
                </form>
            <?php else:?>
                <ul class="nav nav-tabs">
                    <li role="orders" class="active">
                        <a href="#tab-orders" data-toggle="tab" role="tab">Orders</a>
                    </li>
                    <li role="order-items">
                        <a href="#tab-order-items" data-toggle="tab" role="tab">Order Items</a>
                    </li>
                    <li role="items">
                        <a href="#tab-items" data-toggle="tab" role="tab">Inventory</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="tab-orders" class="tab-pane active">
                        <table id="orders">
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Payment Method</th>
                                <th>Gift Amount</th>
                                <th>Date Ordered</th>
                                <th>Show on Donor List</th>
                                <th>Include in Newsletter</th>
                                <th>Order Status</th>
                                <th>Notes</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <td>Total Amount</td>
                                <td colspan="3"></td>
                                <td id="order-totals">$<span id="order-totals-num"></span></td>
                                <td colspan="4"></td>
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php while($shop_orders->have_posts()):
                            $shop_orders->the_post();
                            $data = get_post_meta($post->ID,'order_data',true);
                            $is_donor_list = get_post_meta($post->ID,'billing_display_in_donor_list',true);
                            $is_newsletter = get_post_meta($post->ID,'billing_included_in_newsletter',true);
                            $ordder_status = get_the_terms($post->ID, 'shop_order_status' );
                            $ordder_status = process_status($ordder_status[0]->name);
                            //$data = unserialize($order_meta);
                            ?>
                            <?php if($ordder_status!= "cancelled" && $ordder_status!= "error processing"):?>
                            <tr>

                                <td><?php echo $data['billing_first_name'];?></td>
                                <td><?php echo $data['billing_last_name'];?></td>
                                <td><?php echo $data['billing_email']==""?'-':$data['billing_email'];?></td>
                                <td <?php echo $data['payment_method_title']=="Cheque Payment"?'class="cheque"':'';?>><?php echo $data['payment_method_title'];?></td>
                                <td class="money">$ <?php echo $data['order_total'];?></td>
                                <td><?php echo $post->post_date;?></td>
                                <td><?php echo $is_donor_list==NULL?'no':'yes';?></td>
                                <td><?php echo $is_newsletter==NULL?'no':'yes';?></td>
                                <td <?php echo $ordder_status=="error processing"?'class="error"':''?>><?php echo $ordder_status;?></td>
                                <td><?php the_excerpt();?></td>
                            </tr>

                        <?php endif;endwhile; ?>
                        </tbody>
                    </table>
                    </div>
                    <div id="tab-items" class="tab-pane">
                        <table id="items">
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Price per unit</th>
                                    <th>Ordered</th>
                                    <th>Total Ordered ($)</th>
                                    <th>Total Qty</th>
                                    <th>Total Amount ($)</th>
                                </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <td colspan="2">Totals</td>
                                <td>Gift Drive</td>
                                <td class="money">$<span id="items-inv-sum-gift"></span></td>
                                <td></td>
                                <td class="money">$<span id="items-total-sum-gift"></span></td>
                            </tr>
                            <tr>
                                <td colspan="2">Totals</td>
                                <td >Education Fund</td>
                                <td class="money">$<span id="items-inv-sum-ed"></span></td>
                                <td ></td>
                                <td class="money">$<span id="items-total-sum-ed"></span></td>
                            </tr>
                            <tr>
                                <td colspan="2">Totals</td>
                                <td ></td>
                                <td class="money">$<span id="items-inv-total"></span></td>
                                <td ></td>
                                <td class="money">$<span id="items-total"></span></td>
                            </tr>
                            </tfoot>
                            <tbody>
                            <?php while($products->have_posts()):
                                $products->the_post();
                                $total_qty = get_post_meta($post->ID,'target_quantity',true);
                                $price = get_post_meta($post->ID,'regular_price',true);
                                $ordered = $total_qty - get_post_meta($post->ID,'stock',true);
                                //$stock = get_post_meta($post->ID,'stock',true);
                                ?>
                                <?php if($ordder_status!= "cancelled"):?>
                                <tr>

                                    <td><?php the_title();?></td>
                                    <td class="money">$ <?php echo $price;?></td>
                                    <td class="money"> <?php echo $ordered;?></td>
                                    <td class="money" id="prod-<?php the_ID();?>-ordered-total">$ <?php echo $price*$ordered;?></td>
                                    <td class="money"><?php echo $total_qty;?></td>
                                    <td class="money" id="prod-<?php the_ID();?>-qty-total">$ <?php echo $price*$total_qty;?></td>
                                </tr>
                            <?php endif;endwhile;?>
                            </tbody>
                        </table>
                    </div>
                    <div id="tab-order-items" class="tab-pane">
                        <table id="order-items">
                            <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Item Name</th>
                                <th>Item Qty</th>
                                <th>Item Total Amount</th>
                                <th>Date Ordered</th>
                                <th>Order Status</th>
                            </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td colspan="4" >Total Amount</td>
                                    <td id="item-order-totals" class="money">$<span id="item-order-totals-num"></span></td>
                                    <td colspan="2"></td>
                                </tr>
                            </tfoot>
                            <tbody>
                            <?php while($shop_orders->have_posts()):
                                $shop_orders->the_post();
                                $data = get_post_meta($post->ID,'order_data',true);
                                $order_items = get_post_meta($post->ID,'order_items',true);
                                $ordder_status = get_the_terms($post->ID, 'shop_order_status' );
                                $ordder_status = process_status($ordder_status[0]->name);
                                ?>
                                <?php if($ordder_status!= "cancelled" && $ordder_status!= "error processing"):?>
                                    <?php foreach ($order_items as $item):?>
                                        <tr>
                                            <td><?php echo $data['billing_first_name'];?></td>
                                            <td><?php echo $data['billing_last_name'];?></td>
                                            <td><?php echo $item['name'];?></td>
                                            <td><?php echo $item['qty'];?></td>
                                            <td class="money">$ <?php echo $item['cost']*$item['qty'];?></td>
                                            <td><?php echo $post->post_date;?></td>
                                            <td <?php echo $ordder_status=="error processing"?'class="error"':''?>><?php echo $ordder_status;?></td>
                                        </tr>
                                    <?php endforeach;?>
                            <?php endif;endwhile; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php endif;?>
        <?php else:?>
            <form action="" method="POST" class="login">
                <label for="key">Key</label>
                <input type="text" name="key">
                <input type="submit" value="Go">
            </form>
        <?php endif;?>
    </section>

</div>


</body>
</html>
<?php
/*
Plugin Name: VLaunchIT
Description: VLaunchIT functions
Version: 1.0
Author: VLanch Infotech INC
Author URI: http://www.vlaunchit.com
License: GPL2
*/

/**---------------------------
 DEFINE GLOBAL VARS HERE
 ----------------------------*/

/**---------------------------
 ADD THUMBNAIL SUPPORT AND DEFINE THUMBNAIL SIZES
 ----------------------------*/
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 100, 90, false ); // Defaul size
add_image_size( 'homepage', 390, 216 ); // Other size
add_image_size( 'sectionpage', 690, 250 ); // Other size

/**---------------------------
 ADD MENU SUPPORT FOR THEME
 ----------------------------*/
add_theme_support( 'menus' );/*Add custom menu support*/

/**---------------------------
 CUSTOMIZE POST MANAGEMENT PAGE
 ----------------------------*/
/*Customize the post list page in the admin area for the custom type "member", notice "member" in the actually hook name*/
add_action("manage_posts_custom_column", "vl_custom_columns", 10, 2); /* Customize the value to be shown for each column in post list summary display.*/
add_filter("manage_edit-member_columns", "vl_edit_columns");/* Modify which columns to show/hide in post list summary display.*/

/**---------------------------
 DEFINE HOOKS
 ----------------------------*/
/***Add ajax action***/
add_action( 'wp_ajax_nopriv_add_to_gift_bag', 'add_to_gift_bag_callback' );
add_action( 'wp_ajax_add_to_gift_bag', 'add_to_gift_bag_callback' );
add_action( 'wp_ajax_nopriv_empty_gift_bag', 'empty_gift_bag_callback' );
add_action( 'wp_ajax_empty_gift_bag', 'empty_gift_bag_callback' );
add_action( 'wp_ajax_nopriv_validate_gift_bag', 'validate_gift_bag_callback' );
add_action( 'wp_ajax_validate_gift_bag', 'validate_gift_bag_callback' );
add_action( 'wp_ajax_nopriv_pre_record_paypal_purchase', 'pre_record_paypal_purchase_callback' );
add_action( 'wp_ajax_pre_record_paypal_purchase', 'pre_record_paypal_purchase_callback' );
/**Session Hook*/
add_action('init', 'valaunch_init_session', 1);

add_action('wp_enqueue_scripts', 'vlaunchit_init'); /*turns on styles and js scripts*/
//add_action('admin_init', 'remove_sub_menus'); /*Remove sub menus*/
//add_action('admin_menu', 'remove_menus');/*Remove main admin menus*/
//add_action('admin_init', 'vl_admin_init'); /*Add more custome fields to several post types*/
//add_action('save_post', 'vl_save_company_custom_fields',10,2);/*save custom fields for admin panel */
//add_action("save_post","vl_action_modify_title",10,2); /*Edit the title of a custom post type right after saving/updating*/
add_action('paypal-ipn', 'my_process_ipn',10,1);/*Paypal Framework plugin hook - Process IPN messages from paypal*/
//add_action('admin_head', 'remove_areas');/*Remove certain areas on teh admin page via display:hiden in the admin header*/
//add_action('admin_init','customize_meta_boxes');/*remove certain meta boxes for Edit and Add posts and pages*/

/**Adds a jQuery datepicker script to Event pages.
 * http://jqueryui.com/demos/datepicker/
 */
//add_action('admin_init', 'pbd_events_jquery_datepicker');


/**---------------------------
 DEFINE FILTERS
 ----------------------------*/
//add_filter( 'request', 'vl_post_list_column_orderby' );/*Order the display of the list of posts in the admin post summary page*/
add_filter('the_generator', 'vl_wpbeginner_remove_version');/*Remove wordpress vesrion number*/
//add_filter('query_vars', 'vl_parameter_queryvars' );/*Add Get parameters to WP urls*/

/**---------------------------
 DEFINE WIDGET AREAS
 ----------------------------*/
if ( function_exists('register_sidebar') ){
    /*
    register_sidebar(array(
         'name' => 'Main Page Column 2',
         'id' => 'main-col-2',
         'before_widget' => '',
         'after_widget' => "",
         'before_title' => "<h4>",
         'after_title' => "</h4>",
    ));*/
    /*
    register_sidebar(array(
     'name' => 'Side Bar',
     'id' => 'sidebar',
     'before_widget' => '',
     'after_widget' => "",
     'before_title' => "<h4>",
     'after_title' => "</h4>",
     ));*/

    register_sidebar(array(
     'name' => 'Footer',
     'id' => 'footer2',
     'before_widget' => '',
     'after_widget' => "",
     'before_title' => "<h4>",
     'after_title' => "</h4>",
     ));
}


/**---------------------------
 GENERAL HOOK FUNCTIONS
 ----------------------------*/
/**Session start
 * (Done during init() and after call to wp-settings to make sure session vars are not wiped out by wp-settings.  Give priority "1" so this hook is called before any plugin or widget initializations to make sure if they need the session it's available
 */
function valaunch_init_session(){
    $one_month_in_minute = 43829;

    session_cache_expire($one_month_in_minute);
    $cache_expire = session_cache_expire();

    session_set_cookie_params($one_month_in_minute * 60, '/');
    session_start();
    setcookie(ini_get("session.name"), $sid=session_id(), time() + ini_get("session.cookie_lifetime"), '/');
}


/**Paypal Framework plugin hook callback*/
function my_process_ipn( $data ) {
    //write_to_log($data['payment_status'].', '.$data['custom']."\r\n");
    if($data['payment_status']=='Completed'){//check if paypal returns a completed transcation
        $purchase_array = vl_get_purchase_data($data['custom']);

        if($purchase_array){
            $purchase_obj = $purchase_array[0];
            if(empty($purchase_obj->payment_type)){ //check to make sure this transtraction has been process by finding out if the payment type is registered
                vl_sql_update_purchase_record($data['custom'],'paypal');
                vl_update_items_qty_remain($data['custom'],$purchase_array);
                vl_email_purchase($data['custom'],$purchase_array);
            }
        }


    }
}
function process_cheque_payment($purchaseID ) {
    vl_sql_update_purchase_record($purchaseID,'cheque');
    vl_update_items_qty_remain($purchaseID);
    vl_email_purchase($purchaseID);
}
/**Enqueue scripts and styles*/
function vlaunchit_init() {

	/*the default jquery version 1.6 that comes with wordpress 3.2.1 does not work with the main slideshow, so switching back to version 1.6*/
    /*wp_deregister_script('jquery');
    wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js"), false, '1.6');
    wp_enqueue_script('jquery');*/

    /*adding ajax functionality*/
	wp_enqueue_script( 'my-ajax-request',  plugins_url( 'js/ajax.js',__FILE__), array( 'jquery'), '1.0' ); /*embed the javascript file that makes the AJAX request*/
	wp_localize_script( 'my-ajax-request', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) ); /*declare the URL to the file that handles the AJAX request (wp-admin/admin-ajax.php)*/

    /*Other scripts*/
	wp_enqueue_script( 'easing', plugins_url('js/jqueryeasing1.3.js',__FILE__), array( 'jquery'), '1.0' );
    wp_enqueue_script( 'corner', plugins_url('js/corner.min.js',__FILE__), array( 'jquery'), '1.0' );
    wp_enqueue_script( 'progress', plugins_url('js/jquery.progressbar.min.js',__FILE__), array( 'jquery'), '1.0' );
    //wp_enqueue_script( 'reflection',plugins_url('js/reflection-jquery/src/reflection.js',__FILE__), array( 'jquery'), '1.0' );
	wp_enqueue_script( 'vlaunch',plugins_url('js/vlaunchit.js',__FILE__), array( 'jquery', 'easing'), '1.0' );
    wp_enqueue_script( 'common_jquery',get_bloginfo('stylesheet_directory').'/js/common_jquery.js', array( 'jquery','vlaunch'), '1.1' );
}

/**
 * Adds a jQuery datepicker script to Event pages.
 * http://jqueryui.com/demos/datepicker/
 */
function pbd_events_jquery_datepicker() {
     wp_enqueue_style(
		'anytimne-datepicker-style',
		get_bloginfo('stylesheet_directory').'/js/anytimec.css'
	);
    wp_enqueue_script(
		'anytimne-datepicker',
		get_bloginfo('stylesheet_directory').'/js/anytimec.js',
		array('jquery')
	);

	wp_enqueue_script(
		'pbd-datepicker',
		get_bloginfo('stylesheet_directory').'/js/vlaunchit_admin.js',
		array('jquery', 'anytimne-datepicker')
	);
}

/**---------------------------
 ADMIN PANEL CUSTOMIZATION HOOK FUNCTIONS
 ----------------------------*/
/**Remove meta boxes from Add and Edit posts & pages**/
function customize_meta_boxes() {
    /* Removes meta boxes from Posts */
    //remove_meta_box('postcustom','post','normal');
    //remove_meta_box('trackbacksdiv','post','normal');
    //remove_meta_box('commentstatusdiv','post','normal');
    //remove_meta_box('commentsdiv','post','normal');
    //remove_meta_box('tagsdiv-post_tag','post','normal');
    //remove_meta_box('postexcerpt','post','normal');
    //remove_meta_box('multi_content','post','normal');  //remove the multi content plugin block from posts
    //remove_meta_box('multi_content','member','normal');  //remove the multi content plugin block from member custom type

    /* Removes meta boxes from pages */
    //remove_meta_box('postcustom','page','normal');
    //remove_meta_box('trackbacksdiv','page','normal');
    //remove_meta_box('commentstatusdiv','page','normal');
    //remove_meta_box('commentsdiv','page','normal');
}

/**Removes menu panels for certain user roles*/
function remove_menus () {
    global $menu;

    if( !current_user_can('administrator') ) {//check if user is admin
        $restricted = array(__('Dashboard'), __('Posts'), __('Media'), __('Links'), __('Pages'), __('Appearance'), __('Tools'), __('Users'), __('Settings'), __('Comments'), __('Plugins'));
        /*if(!current_user_can('artist')){
            array_push ($restricted,__('Songs')); //custom type widget
        }*/
     }
    else{
        $restricted = array( __('Comments'),__('Links'));
    }

    end ($menu);
    while (prev($menu)){
        $value = explode(' ',$menu[key($menu)][0]);
        if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
    }

}

/**Removes certain areas on the admin page via display:hidden in admin header*/
function remove_areas() {
  echo '<style type="text/css">
    #cpt_info_box {display:none}
    </style>';
}

/**Remove WP default Media-tag submenu item*/
function remove_sub_menus () {
	global $menu, $submenu;
	remove_submenu_page( 'upload.php','edit-tags.php?taxonomy=media-tags');
}

/**Modify the columns in post list summary display.
 *Register the new column and remove all the default ones
 *Make the column name sortable by using the hook "manage_edit-posttype_sortable_column*/
function vl_edit_columns($columns)
{
	/*$columns = array(
		'first_name' =>'First Name',
        'last_name' => 'Last Name',
        'member_type' =>  'Memeber Of'
    );*/
    $columns['member_type']='Memeber Of';
    $columns['title']='Name'; //change the lable of the default column "title"
    unset($columns['author']);
    unset($columns['date']);
	return $columns;
}

function vl_custom_columns( $column_name, $post_id)
{
    if ( 'first_name' == $column_name ){
        $col_val = get_post_meta($post_id, 'first_name', true);
    }
    else if('last_name' == $column_name ){
        $col_val = get_post_meta($post_id, 'last_name', true);

    }else if('member_type' == $column_name ){
        //$col_val = implode(', ',get_post_meta($post_id, 'exec_type', false));

        $col_val = vl_get_exec_type_string($post_id);
    }else{
       return;
    }
    $col_val = $col_val?$col_val:'N/A';
	echo '<a href="'.get_bloginfo('url').'/wp-admin/post.php?post='.$post_id.'&action=edit">'.$col_val.'</a>';
}

/**Modify a post title when saved using meta data(custom fields) passed in the edit POST form in the admin section*/
function vl_action_modify_title($post_ID){
    if(get_post_type($post_ID)=='member'){
        /***get the meta data using Custom Field Form plugin
        $first_name =$_POST['first_name'];
        $last_name = $_POST['last_name'];

        //check if the meta data was passed in.  In this particular case it's a 2 dimensional array(check the custom field form) so have to read the array
        $first_name =$first_name?$first_name[0][0]:'';
        $last_name = $last_name?$last_name[1][0]:'';

        $name =$last_name;
        $name .= empty($first_name)?'':'-'.$first_name;
        $title = $first_name.' '.$last_name;
        $postarr = array();
        $postarr['ID'] = $post_ID;
        $postarr['post_title'] = trim($title)==''?'None':trim($title);
        $postarr['post_name'] = trim($name)==''?'member'.$post_ID: strtolower(vl_remove_all_whitespace($name));
        */
        /**get the meta data using SImple Field Plugin*/
        //check if the meta data was passed in.  In this particular case it's a 2 dimensional array(check the custom field form) so have to read the array
        $first_name =$_POST['simple_fields_fieldgroups'][6][1][0]?$_POST['simple_fields_fieldgroups'][6][1][0]:''; //first name is in field groupID 6, fieldID 1
        $last_name = $_POST['simple_fields_fieldgroups'][6][2][0]?$_POST['simple_fields_fieldgroups'][6][2][0]:''; //last name is in field groupID 6, fieldID 2

        $name =$last_name;
        $name .= empty($first_name)?'':'-'.$first_name;
        $exec_types = vl_get_exec_type_string($post_ID);
        $exec_types = empty($exec_types)?'':' - '.$exec_types;
        $title = $first_name.' '.$last_name.$exec_types;
        $postarr = array();
        $postarr['ID'] = $post_ID;
        $postarr['post_title'] = trim($title)==''?'None':trim($title);
        $postarr['post_name'] = trim($name)==''?'member'.$post_ID: strtolower(vl_remove_all_whitespace($name));
        sql_update_post($postarr);
    }
}

/** Add meta boxes and other functions during admin init*/
function vl_admin_init() {
    /*add meta boxes for "company" custom type*/
    add_meta_box('event-types', 'Event', 'vl_event_meta_box', 'company', 'side', 'low');
    add_meta_box('company-extra-info', 'Extra Info', 'vl_company_details_meta_box', 'company', 'normal', 'low');
}

/**Call back to add events*/
function vl_event_meta_box($the_post) {
    //Nonce for verification.
    wp_nonce_field( plugin_basename(__FILE__), 'pbd_events_nonce');
    ?>
    <p>Note: Multiple Events selections is possible</p>
    <?php echo print_option_list('events_name_array',$the_post->ID );?>

<?php }

/**Call back to add events*/
function vl_company_details_meta_box($the_post) {
    //Nonce for verification.
    $url = get_post_meta($the_post->ID,'vl_meta_box_company_url',true);
    $is_pride_at_work = get_post_meta($the_post->ID,'vl_meta_box_pride_at_work',true);
    $is_footer_logo = get_post_meta($the_post->ID,'vl_meta_box_company_in_footer',true);

    wp_nonce_field( plugin_basename(__FILE__), 'pbd_events_nonce');
    ?>
    <ul>
        <li>Website URL <input type="text" id="vl_meta_box_company_url" name="vl_meta_box_company_url" size="43" <?php echo $url?'value="'.$url.'"':'';?>> (eg. http://companylink.com)</li>
        <li>Is a Sponsor of Pride at Work? <input type="checkbox" id="vl_meta_box_pride_at_work" name="vl_meta_box_pride_at_work" <?php echo empty($is_pride_at_work)?'':'checked="yes"'?> value="Yes"> Yes</li>
        <!--<li>Place this logo in the footer? <input type="checkbox" id="vl_meta_box_company_in_footer" name="vl_meta_box_company_in_footer" <?php /*echo empty($is_footer_logo)?'':'checked="yes"'*/?> value="Yes"> Yes.
            <p>***Please Note: If the company is to be shown in the footer, please upload an identical black and white image of the logo and name it like the following example:</p>
            <p>Color Logo: logo_name.jpg</p>
            <p>Black and White Logo: logo_name_bw.jpg</p>
        </li>-->
    </ul>

<?php }

/**Save custom data from custom meta boxes*/
function vl_save_company_custom_fields($post_id) {
    // Verification check.
    global $vl_company_meta;

	if ( !wp_verify_nonce( $_POST['pbd_events_nonce'], plugin_basename(__FILE__) ) )
	      return;
	// And they're of the right level?
	if(!current_user_can('edit_posts',$post_id) )
		return;


    /*save the following for company type*/
    if(get_post_type($post_id)=='company'){
        //save event type
        foreach($vl_company_meta as $meta_box){
             foreach($meta_box as $key=>$value){
                $event_meta_value= $_POST[$key];
                $event_meta = get_post_meta($post_id,$key,true);
                //make sure from date is not empty
                if($event_meta|| !empty($event_meta_value)){
                    update_post_meta($post_id, $key,$event_meta_value);
                }
             }
        }
    }
}


/**---------------------------
 AJAX HOOK FUNCTIONS
 ----------------------------*/
/*add ajax action
 if both logged in and not logged in users can send this AJAX request,
add both of these actions, otherwise add only the appropriate one*/

/*remove item from basket call back*/
function empty_gift_bag_callback() {
	
	try{
		session_unset('basket');
    	$response = json_encode( array( 'success' => true) );
	}catch(Exception $e){
		$response = json_encode( array( 'success' => false) );
	}

	// response output
	header( "Content-Type: application/json" );
	echo $response;
	// IMPORTANT: don't forget to "exit"
	exit;
}
/*check if gift bag's items are available */

function validate_gift_bag_callback() {

    $lang = $_POST['language'];
    $purchaser_name = $_POST['name'];
    $purchaser_email = $_POST['email'];
    $purchaser_list_name = $_POST['list_name'];

    $total=0;
    $unavailable_items = array();
    $available_items = array();
    $error = false;

    if (empty($_SESSION['basket'])){
         if($lang=='en'){
            $response = json_encode( array( 'success' => false, 'message'=> "Error - Your gift bag is empty") );
        }else{
            $response = json_encode( array( 'success' => false, 'message'=> "Trục trặc - Giỏ quà của quý vị không chứa đựng quà.") );
        }
    }else{
       foreach($_SESSION['basket'] as $item => $details){
            $item = get_post($item,OBJECT);
            if ($item){//check if item exists in db
                $remaining_str=get_post_meta($item->ID, 'item_remaining',true);
                $item_price=get_post_meta($item->ID, 'item_price',true);
                $remaining = intval($remaining_str);
                $item_qty = intval($details['ItemQty']);
                 if ( $item_qty>$remaining){//if basket is empty then make sure there is enough remaining qty before adding to basket
                     array_push($unavailable_items,$details['ItemName'] );
                     $error=true;
                 }else{
                     //array_push($available_items,$details['ItemQty']." ".$details['ItemName']);
                     $available_items[$item->ID] = array('qty'=>$item_qty, 'name'=>$details['ItemName']);
                     $total=$total+$item_price*$item_qty;
                 }
            }else{//item doesn't exist in db
                $error=true;
                if($lang=='en'){
                    $response = json_encode( array( 'success' => false, 'message'=> "Error - One of the item you selected is no longer available.  Please clear your gift bag and try again.") );
                }else{
                    $response = json_encode( array( 'success' => false, 'message'=> "Trục trặc - Một trong những món quà qúy vị chọn không còn tồn tại, xin vui chọn lại các món quạ") );
                }
                break;
            }
       }

       if(count($unavailable_items)>0){//if there are items that do not have quantities available
            $unavailable_items_str = implode(",",$unavailable_items);
           if($lang=='en'){
                $response = json_encode( array( 'success' => false, 'message'=> "Error - The following items no longer have the quantities of your choice: ".$unavailable_items_str." . Please modify your purchases.") );
            }else{
                $response = json_encode( array( 'success' => false, 'message'=> "Trục trặc - Những món quà sau đây còn số lượng ít hơn qúy vị chọn: ".$unavailable_items_str.".  Xin vui lòng thay đổi số lượng của các món nàỵ") );
            }
       }


      if(!$error){
          //add purchase record into db
          $available_items_serial = serialize($available_items);
          $purchase_id = vl_sql_insert_purchase($purchaser_name,$purchaser_email,$available_items_serial,$total,$purchaser_list_name);

          $response = json_encode( array( 'success' => true, 'purchaseId'=>$purchase_id) );
       }
    }

    // response output
    header( "Content-Type: application/json" );
    echo $response;
    // IMPORTANT: don't forget to "exit"
    exit;
}
/*add to gift basket*/
function add_to_gift_bag_callback() {
	// get the submitted parameters
    /*
	$postID = $_POST['postID'];
	$post = get_post($postID);
	$post_content = $post->post_content;
	$post_title = $post->post_title;
    */

    ignore_user_abort(true);

    if (empty($_SESSION['basket'])){
        $_SESSION['basket'] = array();
    }
    $lang = $_POST['language'];
    $item_name =  $_POST['ItemName'];
    $item_code = $_POST['ItemCode'];
    $item = get_post($item_code,OBJECT);
    if ($item){//check if item exists in db
        $remaining_str=get_post_meta($item_code, 'item_remaining',true);
        $remaining = intval($remaining_str);
        $item_qty = intval($_POST['ItemQty']);
         if (empty($_SESSION['basket'][$item_code]) && ($item_qty<=$remaining)){//if basket is empty then make sure there is enough remaining qty before adding to basket
            $_SESSION['basket'][$item_code] = $_POST; //add item to basket if hasn't already existed
              if($lang=='en'){
                $response = json_encode( array( 'success' => true, 'message'=>$item_qty." units of ".$item_name." have been added to your basket") );
            }else{
                $response = json_encode( array( 'success' => true, 'message'=>$item_qty." đơn vị ".$item_name. " đã được cho vào giỏ quà") );
            }
        }
        else if (!empty($_SESSION['basket'][$item_code]) && (($_SESSION['basket'][$item_code]['ItemQty']+$item_qty)<=$remaining)) { //if basket already contains item, then make sure adding more qty still less or equal the qty remaining
            $_SESSION['basket'][$item_code]['ItemQty'] += intval($_POST['ItemQty']);
            if($lang=='en'){
                $response = json_encode( array( 'success' => true, 'message'=>$item_qty." units of ".$item_name." has been added to your basket") );
            }else{
                $response = json_encode( array( 'success' => true, 'message'=>$item_qty." đơn vị ".$item_name. " đã được cho vào giỏ quà.  Quý vị có thể vào <a href='".get_post_permalink(99)."'>đây</a> để xem giỏ quà.") );
            }

        }else {
            if($lang=='en'){
                $response = json_encode( array( 'success' => false, 'message'=> "There is not enough remaining quantity, please reduce the quantity purchase") );
            }else{
                $response = json_encode( array( 'success' => false, 'message'=> "Xin vui lòng giảm số lượng") );
            }


        }
    }
    else{
        $response = json_encode( array( 'success' => false, 'message'=>"") );

    }
	// response output
	header( "Content-Type: application/json" );
	echo $response;
	// IMPORTANT: don't forget to "exit"
	exit;
}


/**---------------------------
 FILTER FUNCTIONS
 ----------------------------*/
/**Security-remove wordpress version number*/
function vl_wpbeginner_remove_version() {
    return '';
}

/**Add Get parameters to WP urls*/
function vl_parameter_queryvars( $qvars )
{
    $qvars[] = 'url';
    return $qvars;
}

/**Retrieve params defined above*/
function get_event_param()
{
    global $wp_query;
    if (isset($wp_query->query_vars['url']))
    {
        return $wp_query->query_vars['url'];
    }else{
        return null;
    }
}

/**Filter the result to sort by meta value of our chosing*/
function vl_post_list_column_orderby( $vars ) {
	if ( isset( $vars['orderby'] ) ) {
        if ( 'first_name' == $vars['orderby'] ){
            $vars = array_merge( $vars, array(
                'meta_key' => 'first_name',
                'orderby' => 'meta_value_num'
            ) );
        }
        else if('last_name' == $vars['orderby'] ){
            $vars = array_merge( $vars, array(
                'meta_key' => 'last_name',
                'orderby' => 'meta_value_num'
            ) );
        }else if('member_type' == $vars['orderby']){
           $vars = array_merge( $vars, array(
                'meta_key' => 'member_type',
                'orderby' => 'meta_value_num'
            ) );
        }
	}

	return $vars;
}


/**---------------------------
 COMMON FUNCTIONS
 ----------------------------*/
/*Return the hover url of an image given that image's url
 *@url the url of the image
 */
 function get_hover_url($url, $hover_extension = '-hover')
 {
	$patterns = array('/.jpg/', '/.jpeg/', '/.gif/', '/.png/', '/.bmp/');
	$replacements = array( $hover_extension.'.jpg',$hover_extension.'.jpeg',$hover_extension.'.gif',$hover_extension.'.png',$hover_extension.'.bmp');
	$hover_url = preg_replace($patterns, $replacements,$url);
	return $hover_url;
 }

/*Remove the "title" attribute from any element
*@param $list the string containing elements with the title attributes
*/
function vl_clean_list( $list ) {
		return preg_replace('/title=\"(.*?)\"/','',$list);
}

/*Regular Expression to validate teh date
 *@param date a date string
 */
function vl_validate_date($date){
    return preg_match('(^\w{3,9}\s{1}\d{2}\s{1}\d{4}$)', $date);
}

/*Redirect a link
 @param link The destination link*/
function vl_redirect($link){
// grab the 'naked' link
	//$link	= strip_tags($contents);
	$link	= preg_replace('/\s/', '', $link);

 // work out
	if(!preg_match('%^http://%', $link)){
		$host	= $_SERVER['HTTP_HOST'];
		$dir	= dirname($_SERVER['PHP_SELF']);
		$link	= "http://$host$dir/$link";
	}
// do the link
	header("Location: $link");
	die('');

}

/*Sort an associative array
 *eg. $people = array(
 *		12345 => array(
 *			'id' => 12345,
 *			'first_name' => 'Joe',
 *			'surname' => 'Bloggs',
 *			'age' => 23,
 *			'sex' => 'm'
 *		 )
 *	   );
 *	   print_r(array_sort($people, 'age', SORT_DESC)); // Sort by oldest first
*/
function array_sort($array, $on, $order=SORT_ASC)
{
    $new_array = array();
    $sortable_array = array();
    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }
        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
            break;
            case SORT_DESC:
                arsort($sortable_array);
            break;
        }
        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }
    return $new_array;
}

/*Return the shorten string
  @string the string to be shorten
  @num_words_limit the length to shorten
 */
function shorten_string($string, $num_words_limit=5){
	preg_match('/^\s*+(?:\S++\s*+){1,' .$num_words_limit . '}/', $string, $matches);
    return $matches[0];
}

/*Echos the custom excerpt function allows control of excerpt length
 @length the length of the excerpt
 @post_ID the ID of the post to draws the excerpt, if doesn't exist, then get the current global  post object
 @end_char the ending char to be attached to the except, the default is "..."
 */
function custom_the_excerpt($length,$post_ID=null, $end_char = '&#8230;'){
   global $post;
    if($post_ID){
        $the_post = get_post($post_ID,OBJECT);
    }else{
        $the_post=$post;
    }

    $excerpt = apply_filters('the_excerpt', $the_post->post_excerpt);

	if(!empty($excerpt)){
	  echo $excerpt;
	}
	else{
        $content = apply_filters('the_content', $the_post->post_content);
          $content = strip_tags($content);
          preg_match('/^\s*+(?:\S++\s*+){1,' .$length . '}/', $content, $matches);
        //return the end character as well if the content is cut short
        if(strlen($matches[0])< strlen($content)){
            echo "<p>" . $matches[0] .$end_char. "</p>";
        }else{
            echo "<p>" . $matches[0]."</p>";
        }
	}
}

/*Get the custom excerpt function allows control of excerpt length
 @length the length of the excerpt
 @post_ID the ID of the post to draws the excerpt, if doesn't exist, then get the current global  post object
 @end_char the ending char to be attached to the except, the default is "..."
 */
function get_custom_the_excerpt($length,$post_ID=null, $end_char = '&#8230;'){
   global $post;
    if($post_ID){
        $the_post = get_post($post_ID,OBJECT);
    }else{
        $the_post=$post;
    }

    $excerpt = apply_filters('the_excerpt', $the_post->post_excerpt);

	if(!empty($excerpt)){
	  echo $excerpt;
	}
	else{
        $content = apply_filters('the_content', $the_post->post_content);
	  $content = strip_tags($content);

	  preg_match('/^\s*+(?:\S++\s*+){1,' .$length . '}/', $content, $matches);
        //return the end character as well if the content is cut short
        if(strlen($matches[0])< strlen($content)){
            return $matches[0].$end_char;
        }else{
            return $matches[0];
        }
	}
}

/*
 * Test if a page has children sub pages
*/
function has_children($post_id) {
    $children = get_pages("child_of=$post_id");
    if( count( $children ) != 0 ) { return true; } // Has Children
    else { return false; } // No children
}

/*Get the ID of the top level ancestor of a post/page
 *@id the ID of the current post
 *@returns the ancestor post ID
 */
function get_top_ancestor($id){
	$current = get_post($id);
	if(!$current->post_parent){
	    return $current->ID;
	} else {
	    return get_top_ancestor($current->post_parent);
	}
}

function vl_remove_all_whitespace($text){
    return str_replace(array("\n", "\r", "\t", " ", "\o", "\xOB"), '', $text);
}

/*For Testing*/
function write_to_log($message,$file='log.txt'){
    $myFile = $file;
    $fh = fopen($myFile, 'a') or die("can't open file");
    fwrite($fh, $message);
    fclose($fh);
}

/*Update qty remaining of all items in basket*/
function vl_update_items_qty_remain($purchaseID, $purchase_object=null){
    if($purchase_object==null)
        $purchase_array = vl_get_purchase_data($purchaseID);
    else
        $purchase_array = $purchase_object;

    if($purchase_array ){
        $purchase = $purchase_array[0];
        if($purchase){
            $gift_drive_amount = 0;
            $scholarship_amount = 0;
            try{
                $items_array = unserialize($purchase->items);
                foreach($items_array as $item => $values){
                    //write_to_log("item: $item, values: ".print_r($values,true));
                    $remaining_str = get_post_meta($item,'item_remaining',true);
                    $item_price = intval(get_post_meta($item,'item_price',true));
                    $remaining = intval($remaining_str);
                    $qty = intval($values['qty']);
                    update_post_meta($item,'item_remaining', $remaining-$qty);
                    if($item==SCHOLARSHIP_DONATE)//if is a scholarship item
                    {
                        $scholarship_amount += $qty*$item_price;
                    }else{
                        $gift_drive_amount += $qty*$item_price;
                    }

                }
            }catch(Exception $ext){

            }
            if($scholarship_amount>0){ //update scholarship total
                //update total amount
                $amount = get_post_meta(HOME2011_PAGE_ID,'education_amount_raised', true);
                $amount =  $amount + $scholarship_amount;
                update_post_meta(HOME2011_PAGE_ID,'education_amount_raised',$amount);
            }
            if($gift_drive_amount>0){// update gift drive total
                //update total amount
                $amount = get_post_meta(HOME2011_PAGE_ID,'gift_amount_raised', true);
                $amount =  $amount +$gift_drive_amount;
                update_post_meta(HOME2011_PAGE_ID,'gift_amount_raised',$amount);

            }

            clear_basket();
            //empty_gift_bag_callback();
        }
    }


}
/*Email function*/
function vl_email_purchase($purchaseID, $purchase_object=null){
     if($purchase_object==null)
        $purchase_array = vl_get_purchase_data($purchaseID);
    else
        $purchase_array = $purchase_object;

    if($purchase_array ){
        $purchase = $purchase_array[0];
        $subject = "A New Purchase from ".$purchase->name;
        $body = "Purchaser Name: ".$purchase->name."<br>";
        $body .= "Purchaser Email: ".$purchase->email."<br>";

        $items_array = unserialize($purchase->items);
        $item_str = '';
        foreach($items_array as $item => $values){
            $item_str .= $values['qty'].' '.$values['name'].',';
        }
        if(empty($purchase->payment_type)){
            $payment_type = "paypal";//paypal return payment type is not working, we just add it manually
        }else{
            $payment_type = $purchase->payment_type;
        }
        $body .= "Items:".$item_str.'<br>';
        $body .= "Total: $".$purchase->total."<br>";
        $body .= "List Name on Donors Page: ".$purchase->list_name."<br>";
        $body .= "Payment Type: ".$payment_type."<br>";
        $body .= "Date Purchased: ".$purchase->date."<br>";


        sendNotification($subject, $body);

        if($purchase->email!=''){
            $items_array = unserialize($purchase->items);
            $item_str = '';
            foreach($items_array as $item => $values){
                $item_str .= $values['qty'].' '.$values['name'].'<br>';
            }
            sendThankYouEmail($purchase->name,$purchase->email,$item_str, $purchase->total  );
        }




    }
    //echo $body
}
function clear_basket(){
    //unset ($_SESSION['basket']);
session_unset('basket');
}

function sendNotification($subject, $body){
	try {
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: OneMoreStep <info@onemorestep.ca>' . "\r\n";
        $headers .= 'Reply-To: info@onemorestep.ca'. "\r\n";
        $to       = "info@onemorestep.ca,onemorestepvietnam@gmail.com";
        //$to = "phongl@gmail.com";

		mail($to, $subject, $body,$headers);
	}
	catch (Exception $xpt){
		file_put_contents('email_error.txt', $xpt->getMessage());
	}
}

function sendThankYouEmail($name, $email, $items, $amount){
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: OneMoreStep <info@onemorestep.ca>' . "\r\n";
    $headers .= 'Reply-To: info@onemorestep.ca'. "\r\n";
	$subject = ' Gift confirmation - Thank you for your donation';
    $item_list = str_replace(",", "<br>", $items);
	$message = "
<p>Dear $name,</p>

<p>Thank you for your donation to One More Step.</p>

<p>We have recorded the following purchase:</p>
<p>
$item_list<br>
Total: $$amount
</p>
<p>
Your money will go a long way in Viet Nam to support the purchase of these necessary supplies, which will be delivered shortly before Christmas.  We will send you another email once supplies have reached the orphanages.</p>
<p>
If your donation is in honour of a friend or family member as a holiday gift, we will be happy to notify them of your gift via email and a card on a date and with the message of your choice.</p>
<p style='color:#9f1f63'>
Please follow us on <a href='http://www.facebook.com/pages/OneMoreStep/141055129331797'>Facebook</a> and <a href='https://twitter.com/#!/takeonemorestep'>Twitter</a>.</p>
<p>
Thanks again and have a very happy season,</p>
<p>
The OneMoreStep team</p> ";

	mail($email, $subject, $message, $headers);
}

/**---------------------------
 CUSTOM SQL Querries Samples
 ----------------------------*/
/***CForm Queries***/
/*insert a record into purchase table*/
function vl_sql_insert_purchase($purchaser_name,$purchaser_email,$available_items_str,$total,$purchaser_list_name){
    global $wpdb;

 	$wpdb->insert(
        'd23oms2011_purchases',
        array(
            'name' => $purchaser_name,
            'email' => $purchaser_email,
            'items' => $available_items_str,
            'total' => $total,
            'list_name' => $purchaser_list_name
        ),
        array(
            '%s',
            '%s',
            '%s',
            '%d',
            '%d'
        )
    );
    return $wpdb->insert_id;//the id of the inserted row
}

function vl_sql_update_purchase_record($purchaseID, $payment_type){
    global $wpdb;
    $result = $wpdb->update(
        'd23oms2011_purchases',
        array(
            'payment_type' => $payment_type
        ),
        array( 'ID' => $purchaseID ),
        array(
            '%s'	
        ),
        array( '%d' )
    );
    if($result==false){
        write_to_log("update_purchase_record, ID: $purchaseID, payment type: $payment_type");
    }
}
/*Retrieve the cFOrm ID from the submission ID*/
function vl_get_purchase_data($purchaseID){
    global $wpdb;
	$querystr = "
		SELECT *
		FROM d23oms2011_purchases
        WHERE ID = %d
        ";

    $querystr = $wpdb->prepare( $querystr,$purchaseID);
    write_to_log($querystr);
 	$purchase = $wpdb->get_results ($querystr, OBJECT);
	return $purchase;
}

/*Get the cForm data by submission ID*/
function vl_get_cForm_data($submission_ID){
    global $wpdb;
	$querystr = "
		SELECT cform.field_name, cform.field_val
		FROM $wpdb->cformsdata cform
        WHERE cform.sub_id = %s
        ";

    $querystr = $wpdb->prepare( $querystr,$submission_ID);

 	$myposts = $wpdb->get_results($querystr, OBJECT);
	return $myposts;
}
/*Get the bio by the specified type of bio
 */

function get_post_by_meta_data($post_type,$meta_key, $meta_value){
    global $wpdb;
	$querystr = "
		SELECT wposts.ID, wposts.post_title, wposts.post_name
		FROM $wpdb->posts wposts, $wpdb->postmeta wmeta
        WHERE wposts.ID = wmeta.post_id
        AND wposts.post_type='%s'
        AND wposts.post_status = 'publish'
        AND wmeta.meta_key = '%s'
        AND wmeta.meta_value = '%s'
        ORDER BY wposts.post_name ASC
        ";

    $querystr = $wpdb->prepare( $querystr,$post_type,$meta_key,$meta_value);

 	$myposts = $wpdb->get_results($querystr, OBJECT);
	return $myposts;
}

function get_query_with_two_meta_keys(){
    global $wpdb;
    
    $query = "   SELECT wposts.ID
		FROM $wpdb->posts wposts,
		(SELECT wmeta.post_id
		 FROM $wpdb->postmeta wmeta
		 WHERE wmeta.meta_key = '%s'
         AND wmeta.meta_value !='') wpmeta1,
		(SELECT wmeta.post_id, wmeta.meta_value as slot_number
		 FROM $wpdb->postmeta wmeta
		 WHERE wmeta.meta_key = '%s'
         AND wmeta.meta_value !='') wometa2
        WHERE wposts.ID = wpmeta1.post_id
        AND wpmeta1.post_id = wometa2.post_id
        AND wposts.post_type='%s'
        AND wposts.post_status = 'publish'
        ";
    return $query;
}

function get_query_with_meta_key_and_value(){
    global $wpdb;

    $query = "  SELECT wposts.ID, wposts.post_title, wposts.post_name
		FROM $wpdb->posts wposts, $wpdb->postmeta wmeta
        WHERE wposts.ID = wmeta.post_id
        AND wposts.post_type='%s'
        AND wposts.post_status = 'publish'
        AND wmeta.meta_key = '%s'
        AND wmeta.meta_value = '%s'
        ";
    return $query;
}

/*Get custom post with the given meta keys (custom fields)*/
function sql_update_post($postattr) {
    global $wpdb;
	$querystr = "
		UPDATE $wpdb->posts
		SET post_title=%s, post_name=%s
        WHERE ID=%d
        AND post_status = 'publish'
        ";

    $querystr = $wpdb->prepare( $querystr,$postattr['post_title'],$postattr['post_name'],$postattr['ID']);

    //error_log( $querystr );

 	$myposts = $wpdb->query($querystr);
}

/*Get custom post with the given meta keys (custom fields)*/
function get_events_sorted_by_date($num_of_posts=3,$month=null) {
	global $wpdb;
    global $month_names;
	$querystr = "
		SELECT *
		FROM $wpdb->posts wposts
		JOIN
		  ( SELECT wmeta.post_id,STR_TO_DATE(wmeta.meta_value,'%s') AS 'from_date'
            FROM $wpdb->postmeta wmeta
            WHERE wmeta.meta_key='from_date'
            GROUP BY wmeta.post_id) meta
        ON wposts.ID = meta.post_id
        WHERE wposts.post_type='event'
        AND wposts.post_status = 'publish'
        ";

        if($month=="past_events"){//if a month is specified, then only get upcoming events
             $querystr .= "
            AND  meta.from_date < NOW()
            ORDER BY meta.from_date DESC
            LIMIT %d";
            $querystr = $wpdb->prepare( $querystr,'%M %d %Y',$num_of_posts);

        }elseif($month!=null&&array_key_exists($month,$month_names)){
            $querystr .= "
            AND  meta.from_date BETWEEN TIMESTAMPADD(MONTH,-11,NOW()) AND NOW()
            AND  MONTHNAME(meta.from_date) = %s
            ORDER BY meta.from_date DESC
            LIMIT %d";
            $querystr = $wpdb->prepare( $querystr,'%M %d %Y',$month,$num_of_posts);

        }else{//if no month is specified, then get upcoming events
            $querystr .= "
            AND meta.from_date >= NOW()
            ORDER BY meta.from_date ASC
            LIMIT %d";

            $querystr = $wpdb->prepare( $querystr,'%M %d %Y',$num_of_posts);
        }
 	$myposts = $wpdb->get_results($querystr, OBJECT);

	return $myposts;
}

/*Get the months of past events, up to the specified month
 */
function get_past_event_months($num_of_months){
    global $wpdb;
	$querystr = "
		SELECT meta.month
		FROM $wpdb->posts wposts
		JOIN
		  ( SELECT wmeta.post_id,STR_TO_DATE(wmeta.meta_value,'%s') AS 'from_date', MONTHNAME(STR_TO_DATE(wmeta.meta_value,'%s')) AS 'month'
            FROM $wpdb->postmeta wmeta
            WHERE wmeta.meta_key='from_date') meta
        ON wposts.ID = meta.post_id
        WHERE wposts.post_type='event'
        AND wposts.post_status = 'publish'
        AND  meta.from_date BETWEEN TIMESTAMPADD(MONTH,%d,NOW()) AND NOW()
        ORDER BY meta.from_date DESC
        ";

    $querystr = $wpdb->prepare( $querystr,'%M %d %Y','%M %d %Y',-1*$num_of_months);

 	$myposts = $wpdb->get_results($querystr, OBJECT);
    //print_r($myposts);
	return $myposts;
}

/*Get posts with the thumbnail and its hovered form urls
 *@post_type the type of post eg. 'page'
 *@taxonomy_name the name of the taxonomy eg. 'category', or 'media_tag'
 *@term_ID the ID of the term eg. ID of testimonials (-1 if there's not term, ie. retreive all posts from the given taxonomy)
*/
function get_posts_with_attachments_by_taxonomy($post_type,$taxonomy_name , $term_ID=-1) {
	global $wpdb;
	$querystr ="SELECT DISTINCT *
				FROM (".get_taxonomy_query($post_type,$taxonomy_name,$term_ID).") the_posts
				LEFT JOIN (".get_attachment_query().") wthumbs
				ON the_posts.ID = wthumbs.thumb_parent
				WHERE the_posts.post_status='publish'
				ORDER BY the_posts.menu_order";
	$querystr = $wpdb->prepare( $querystr);
	$myposts = $wpdb->get_results($querystr, OBJECT);

	return $myposts;
}

/*custom query string to get pages and their associating thumbnail (in this case it's the page icon)
*First select statement is to join all posts w/ parent ID $postID and a table of all the posts w/ their thumbnails urls
*/
function get_posts_with_attachments($postID, $get_children=true, $post_type='page') {
	global $wpdb;
	$querystr = "";

	if($get_children){
		$querystr.="
				SELECT DISTINCT *
				FROM $wpdb->posts the_posts
				LEFT JOIN (".get_attachment_query().") wthumbs
				ON the_posts.ID = wthumbs.thumb_parent
				WHERE the_posts.post_type='%s'
				AND the_posts.post_parent='%d'
				AND the_posts.post_status='publish'
				ORDER BY the_posts.menu_order";
		$querystr = $wpdb->prepare( $querystr, $post_type, $postID);
	 //get the attachments of the $postID's children, in this case it's the page icons

	}else{//get the attachments of the $postID

		$querystr .= "
			SELECT *			FROM $wpdb->posts wchildren
			LEFT JOIN
			  ( SELECT * FROM
					(SELECT wposts.post_parent AS thumb_parent, wposts.guid AS thumb_url
						FROM $wpdb->posts wposts, $wpdb->term_relationships wterm_rel, $wpdb->term_taxonomy wterm_tax, $wpdb->terms wterms
						WHERE wposts.ID = wterm_rel.object_id
						AND wterm_rel.term_taxonomy_id = wterm_tax.term_taxonomy_id
						AND wterm_tax.term_id = wterms.term_id
						AND wposts.post_type = 'attachment'
						AND wposts.post_parent = '%d'
						AND wterm_tax.taxonomy ='media-tags'
						AND wterms.term_id =65) wthumbs1,
					(SELECT wposts.post_parent AS thumb_hover_parent, wposts.guid AS thumb_hover_url
						FROM $wpdb->posts wposts, $wpdb->term_relationships wterm_rel, $wpdb->term_taxonomy wterm_tax, $wpdb->terms wterms
						WHERE wposts.ID = wterm_rel.object_id
						AND wterm_rel.term_taxonomy_id = wterm_tax.term_taxonomy_id
						AND wterm_tax.term_id = wterms.term_id
						AND wposts.post_type = 'attachment'
						AND wposts.post_parent = '%d'
						AND wterm_tax.taxonomy ='media-tags'
						AND wterms.term_id=66) wthumbs2
					WHERE wthumbs1.thumb_parent= wthumbs2.thumb_hover_parent) wthumbs
			ON wchildren.ID = wthumbs.thumb_parent
			WHERE wchildren.ID='%d'
			AND wchildren.post_type='%s'
			AND wchildren.post_status='publish'";
		$querystr = $wpdb->prepare( $querystr, $postID, $postID,  $postID, $post_type);
	}

 	$myposts = $wpdb->get_results($querystr, OBJECT);

	return $myposts;
}

/*
 *Return the query for getting posts given a taxonomy name and the term
 *post_type the type of post eg. page
 *taxonomy_name the name of the taxonomy eg. 'category', 'media_tag'
 *term_id the id of the term eg. 'testimonial' for taxonomy 'category'
*/
function get_taxonomy_query($post_type, $taxonomy_name, $term_ID=-1)
{
	global $wpdb;
	$querrystr = "SELECT wposts.ID, wposts.post_title, wposts.post_type, wposts.guid, wterms.slug, wposts.post_status, wposts.menu_order, wposts.post_date
		FROM $wpdb->posts wposts, $wpdb->term_relationships wterm_rel, $wpdb->term_taxonomy wterm_tax, $wpdb->terms wterms
		WHERE wposts.ID = wterm_rel.object_id
		AND wterm_rel.term_taxonomy_id = wterm_tax.term_taxonomy_id
		AND wterm_tax.term_id = wterms.term_id
		AND wposts.post_type = '".$post_type."'
		AND wterm_tax.taxonomy ='".$taxonomy_name."'";
	if($term_ID >=0)
	{
		$querrystr .= "AND wterms.term_id =".$term_ID." ";
	}
	return $querrystr;
}

function get_attachment_query()
{	global $wpdb;
	return  "SELECT wthumbs1.thumb_parent, wthumbs1.thumb_url, wthumbs2.thumb_hover_url  FROM
			(SELECT wposts.post_parent AS thumb_parent, wposts.guid AS thumb_url
				FROM $wpdb->posts wposts, $wpdb->term_relationships wterm_rel, $wpdb->term_taxonomy wterm_tax, $wpdb->terms wterms
				WHERE wposts.ID = wterm_rel.object_id
				AND wterm_rel.term_taxonomy_id = wterm_tax.term_taxonomy_id
				AND wterm_tax.term_id = wterms.term_id
				AND wposts.post_type = 'attachment'
				AND wterm_tax.taxonomy ='media-tags'
				AND wterms.term_id =65
				GROUP BY wposts.post_parent
				ORDER BY wposts.post_date DESC) wthumbs1,
			(SELECT wposts.post_parent AS thumb_hover_parent, wposts.guid AS thumb_hover_url
				FROM $wpdb->posts wposts, $wpdb->term_relationships wterm_rel, $wpdb->term_taxonomy wterm_tax, $wpdb->terms wterms
				WHERE wposts.ID = wterm_rel.object_id
				AND wterm_rel.term_taxonomy_id = wterm_tax.term_taxonomy_id
				AND wterm_tax.term_id = wterms.term_id
				AND wposts.post_type = 'attachment'
				AND wterm_tax.taxonomy ='media-tags'
				AND wterms.term_id=66
				GROUP BY wposts.post_parent
				ORDER BY wposts.post_date DESC) wthumbs2
			WHERE wthumbs1.thumb_parent= wthumbs2.thumb_hover_parent";
}

/*Custom query to determin if a post belong to a category or any of its decendants
*/
function belong_to_ancestor_categories($ancestor_cateogryID, $postID)
{
	global $wpdb;
	$querystr = "SELECT wposts.ID, wposts.guid, wterms.slug
		FROM $wpdb->posts wposts, $wpdb->term_relationships wterm_rel, $wpdb->term_taxonomy wterm_tax, $wpdb->terms wterms
		WHERE wposts.ID = wterm_rel.object_id
		AND wterm_rel.term_taxonomy_id = wterm_tax.term_taxonomy_id
		AND wterm_tax.term_id = wterms.term_id
		AND wposts.post_type = 'post'
		AND wterm_tax.taxonomy ='category'
		AND wterm_tax.parent ='%d'";

	$myancestors = $wpdb->get_results($querystr, OBJECT);

	if(count($myancestors)>0)
		return true;
	else
		return false;
}
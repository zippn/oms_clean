<?php
/**
 * Created by PhpStorm.
 * User: ddang
 * Date: 5/14/14
 * Time: 4:30 PM
 */

class DB
{
    /**
     * @param $time_str - the date string
     * @param $input_format - the format of the date string
     * @return string
     */
    public static function str_to_db_date($time_str, $input_format)
    {
        return self::str_to_db_time($time_str, $input_format, 'Y-m-d');
    }

    /**
     * @param $time_str
     * @param $input_format
     * @return string
     */
    public static function to_db_datetime($time_str, $input_format)
    {
        return self::str_to_db_time($time_str, $input_format, 'Y-m-d G:i:s');
    }

    /**
     * @param $time_str
     * @param $input_format
     * @param $output_format
     * @return string
     */
    private static function str_to_db_time($time_str, $input_format, $output_format)
    {
        if (empty($time_str) || empty($input_format)){
            return '';
        }

        $time = DateTime::createFromFormat($input_format, $time_str);
        if ($time === false){
            return '';
        }
        return $time->format($output_format);
    }
} 
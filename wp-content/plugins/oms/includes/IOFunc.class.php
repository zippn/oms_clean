<?php
/**
 * Created by PhpStorm.
 * User: ddang
 * Date: 5/23/14
 * Time: 12:18 PM
 */

class IOFunc
{

    /**
     * Convert array into csv file
     * @param array $data - array (0 => array (col, col, col), 1 => array(...))
     * @param bool|string $file_path - if filename == false return content instead of writng to file
     * @param string $header - if emptied => do not write header to the file
     * @param string $delimiter
     * @param string $enclosure
     */
    public static function array_to_csv($data, $file_path = false, $header='', $footer='', $delimiter=',', $enclosure ='"'){

        $csv_file = $file_path === false ? self::get_temp_file_path('csv_file', '.txt') : $file_path;

        if (($fp = fopen($csv_file, 'w')) !== false){
            if (!empty($header)){
                fputs($fp, $header . PHP_EOL);
            }

            foreach($data as $row)
            {
                fputcsv($fp, $row, $delimiter, $enclosure);
            }

            if (!empty($footer)){
                fputs($fp, $footer . PHP_EOL);
            }
            fclose($fp);
        }
    }

    /**
     * @param $csv_file
     * @param bool $skip_first_row - if your csv has a header, use this to skip it
     * @param string $delimiter - separator between values
     * @param string $enclosure - the chars surrounds the value
     * @param string $escape_char - char to use as escape default is like c# or java syntax \" \'
     * @return array|bool
     */
    public static function csv_to_array($csv_file, $skip_first_row=true, $delimiter=',', $enclosure ='"', $escape_char='\\')
    {
        $records = array();
        $first_row = true;

        if (($fp = fopen($csv_file, "r")) !== false)
        {
            while (($data = fgetcsv($fp, 0, $delimiter, $enclosure, $escape_char)) !== false) {
                if ($first_row && $skip_first_row)
                {
                    $first_row = false;
                    continue;
                }

                $records[] = $data;
            }
            fclose($fp);

            return $records;
        }
        else {
            return false;
        }
    }


    /**
     * Get a temp file path.
     * @param string $prefix - string before the filename
     * @param string $extension - eg. '.txt', or '.tmp'
     * @param string $dir - where the temp going to be. If $dir is emptied or not exist then use the system temp
     * @return string - full path of the temp file
     */
    public static function get_temp_file_path($prefix='', $extension='', $dir='')
    {
        if (empty($dir) || !file_exists($dir)){
            $dir = self::add_slash(sys_get_temp_dir());
        }

        $dir = realpath($dir);

        $temp_path = $dir . $prefix . '_' . time() . $extension;
        while(file_exists($temp_path)){
            $temp_path = $dir . $prefix . '_' . microtime() . $extension;
        }

        return $temp_path;
    }

    /**
     * Add a forward slash to the end of the path.
     * @param $dir_path - path with or without ended slash
     * @return string - return the path with trailing slash
     */
    public static function add_slash($dir_path)
    {
        $dir_path = rtrim($dir_path, '/\\');
        $dir_path .= '/';
        return $dir_path;
    }

    /**
     * If dir doesn't exist create it.
     * @param $dir_path - fullpath
     * @param int $permission
     * @return bool
     */
    public static function create_folder($dir_path, $permission = 0777)
    {
        if (!file_exists($dir_path))
        {
            if (!mkdir($dir_path, $permission, true))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Move file from one path to another
     * @param $src - full path
     * @param $dest - full path
     * @return bool
     */
    public static function move_file($src, $dest)
    {
        self::create_folder( dirname($dest) );

        if (copy($src, $dest)){
            if (!unlink($src)){
                return true;
            }
        }
        return false;
    }

    /**
     * @param $file_path
     * @param $new_name - no path just name ony
     * @return bool
     */
    public static function rename($file_path, $new_name){
        $new_file_path = dirname($file_path) . '/' . $new_name;

        return rename($file_path, $new_file_path);
    }

    /**
     * Move file to a directory
     * @param $src - source full path or relative path
     * @param $dest_dir - dir path with ended slash
     * @return bool
     */
    public static function move_file_to_dir($src, $dest_dir)
    {
        if (!self::dir_exist($dest_dir)){
            self::create_folder($dest_dir);
        }

        $dest_dir = self::treat_dir_path($dest_dir);

        if (!Str::ends_with($dest_dir, '/') && !Str::ends_with($dest_dir, '\\')){
            $dest_dir .= DIRECTORY_SEPARATOR;
        }

        $dest = $dest_dir . basename($src);
        return self::move_file($src, $dest);
    }









    /**
     * Gzip string into a file.
     *
     * @param string $file_path remember to add .gz at the end of the file.
     * @param string $content
     */
    public static function gzip($file_path, $content){
        // Open the gz file (w9 is the highest compression)
        $fp = gzopen ($file_path, 'w9');

        // Compress the file
        gzwrite ($fp, $content);

        gzclose($fp);
    }

    public static function dir_exist($path){
        return is_dir($path) && file_exists($path);
    }

    /**
     * For use with folder path only, not filepath
     *
     * @param $path
     * @return string
     */
    public static function treat_dir_path($path){
        $path = str_replace('\\', '/', $path);
        $path = substr($path, -1) == '/' ? $path : $path . '/';
        return $path;
    }

    /**
     * Get list of all directories in a folder.
     *
     * @param $path - must have ended slash
     * @param $match_regex
     * @param $include_path
     * @return array (with ended slash if $include_path = true, slashes is not included otherwise)
     */
    public static function get_dir_list($path, $match_regex = false, $include_path = true){
        $path = self::treat_dir_path($path);
        $dir_list = array();

        if (!self::dir_exist($path)){
            return $dir_list;
        }

        if ($dh = opendir($path)) {
            while (($folder = readdir($dh)) !== false) {
                if (($folder != '.') && ($folder != '..') && (is_dir($path . $folder))){

                    if ($match_regex){
                        if (preg_match($match_regex, $folder) == false){
                            continue;
                        }
                    }

                    if ($include_path){
                        $dir_list[] = $path . $folder . '/';
                    }
                    else {
                        $dir_list[] = $folder;
                    }
                }
            }
            closedir($dh);
        }
        return $dir_list;
    }

    /**
     * Get directory by the last mod date.
     * @param $path
     * @param $age_in_second
     * @return array
     */
    public static function get_dir_file_by_age($path, $age_in_second){
        $file_list = self::get_file_list($path);
        $expired_time = time() - $age_in_second;

        for($i=0; $i<count($file_list); $i++){
            // if less the age then don't delete
            if (filemtime($file_list[$i]) > $expired_time){
                unset($file_list[$i]);
            }
        }

        return $file_list;
    }

    /**
     * Delete file(s) if it is older than the age limit
     * @param $folder_path
     * @param $age_in_second
     */
    public static function delete_by_age($folder_path, $age_in_second){
        $filelist = self::get_dir_file_by_age($folder_path, $age_in_second);

        foreach ($filelist as $filepath){
            unlink($filepath);
        }
    }

    /**
     * Get all the files in the directory, not including subdirectory
     *
     * @param $path - full path
     * @param $match_regex - bool|regex
     * @param $include_path - prefix file name with path
     * @return array of file paths
     */
    public static function get_file_list($path, $match_regex = false, $include_path = true){
        $path = self::treat_dir_path($path);
        $file_list = array();

        if (!self::dir_exist($path)){
            return $file_list;
        }

        if ($dh = opendir($path)) {
            while (($file = readdir($dh)) !== false) {
                if (is_file($path . $file)){
                    if ($match_regex){
                        if (preg_match($match_regex, $file) == false){
                            continue;
                        }
                    }

                    if ($include_path){
                        $file_list[] = $path . $file;
                    }
                    else {
                        $file_list[] = $file;
                    }
                }
            }
            closedir($dh);
        }
        return $file_list;
    }


    /**
     * Recursively get all the file path in the directory
     *
     * @param $path
     * @param $match_regex
     * @param $include_path
     * @return array
     */
    public static function get_subfile_list ($path, $match_regex = false, $include_path = true){
        $path = self::treat_dir_path($path);

        $dir_list = self::get_dir_list($path, false, true);
        $file_list = self::get_file_list($path, $match_regex, $include_path);

        foreach ($dir_list as $subdir){
            $sub_files = self::get_subfile_list($subdir, $match_regex, $include_path);

            if (!empty($sub_files)){
                $file_list = array_merge($file_list, $sub_files);
            }
        }

        return $file_list;
    }

    public static function file_delete ($path){
        unlink($path);
    }

    public static function dir_delete ($path){
        self::rmdirtree($path);
    }

    public static function subdir_delete ($path, $match_regex = false, $file_and_dir = true, $file_only = false, $dir_only = false){
        $path = self::treat_dir_path($path);

        if (!self::dir_exist($path)){
            return false;
        }

        if ($dh = opendir($path)) {
            while (($file = readdir($dh)) !== false) {
                if (($file_and_dir || $file_only) && is_file($path . $file)){
                    if ($match_regex){
                        if (preg_match($match_regex, $file) == true){
                            self::file_delete($path . $file);
                        }
                    }
                    else {
                        self::file_delete($path . $file);
                    }
                }
                else if (($file_and_dir || $dir_only) && ($file != '..' || $file != '.')) {
                    if ($match_regex){
                        if (preg_match($match_regex, $file) == true){
                            self::dir_delete($path . $file);
                        }
                    }
                    else {
                        self::dir_delete($path . $file);
                    }
                }

            }
            closedir($dh);
        }
        return true;
    }

    private static function rmdirtree($dirname) {
        if (is_dir($dirname)) {    //Operate on dirs only
            $result=array();
            if (substr($dirname,-1)!='/') {$dirname.='/';}    //Append slash if necessary
            $handle = opendir($dirname);
            while (false !== ($file = readdir($handle))) {
                if ($file!='.' && $file!= '..') {    //Ignore . and ..
                    $path = $dirname.$file;
                    if (is_dir($path)) {    //Recurse if subdir, Delete if file
                        $result = array_merge($result, self::rmdirtree($path));
                    }else{
                        unlink($path);
                        $result[].=$path;
                    }
                }
            }
            closedir($handle);
            rmdir($dirname);    //Remove dir
            $result[].=$dirname;
            return $result;    //Return array of deleted items
        }else{
            return false;    //Return false if attempting to operate on a file
        }
    }

    /**
     * Run cmd in shell
     * @param $cmd
     * @param $redirect_error
     * @param $nice - Ranging from -20(highest) to 19(lowest) default is 10
     * @return string
     */
    public static function exec_cmd ($cmd, $redirect_error = true, $nice = '+10'){
        if ($redirect_error){
            $cmd .= " 2>&1";
        }

        if ($nice != '+10'){
            $cmd = "nice $nice $cmd";
        }

        exec($cmd, $output);
        return implode("\n", $output);
    }

    /**
     * Run cmd in background
     *
     * @param string $cmd
     * @param bool $return_data
     * @param string $nice
     * @return int|array
     */
    public static function exec_bg ($cmd, $return_data = false, $nice = '+10'){
        $nice = $nice == '+10' ? '' : " nice $nice";
        $pid = exec("nohup$nice $cmd > /dev/null 2>&1 &", $results, $status);

        if ($return_data){
            return array (
                'pid' => $pid,
                'result' => implode("\n", $results),
                'status' => $status
            );
        }
        return $pid;
    }

    public static function byte_to_mb ($byte){
        return round($byte/1048576);
    }

    public static function file_size_in_mb($filepath){
        return intval(self::byte_to_mb(filesize($filepath)));
    }
} // end class
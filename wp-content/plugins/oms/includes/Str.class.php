<?php
/**
 * Created by PhpStorm.
 * User: ddang
 * Date: 5/14/14
 * Time: 9:53 AM
 */

class Str
{
    //region [ Equals ]
    /**
     * @param $str1
     * @param $str2
     * @param bool $case_sensitive
     * @return bool
     */
    public static function equals ($str1, $str2, $case_sensitive = false){
        if ($case_sensitive){
            return $str1 == $str2;
        }

        return strcasecmp($str1, $str2) === 0;
    }

    /**
     * @param $str
     * @param array $str_array
     * @param bool $case_sensitive
     * @return bool
     */
    public static function equals_any ($str, $str_array, $case_sensitive = false)
    {
        return self::any( array(self, 'equals'), $str, $str_array, $case_sensitive );
    }
    //endregion

    //region [ Starts With ]
    /**
     * Determine if the string starts with the specified needle
     *
     * @param $haystack - the big string
     * @param $needle - the string you want to find
     * @param bool $case_sensitive
     * @return bool
     */
    public static function starts_with($haystack, $needle, $case_sensitive = false)
    {
        if ($case_sensitive)
        {
            return $needle === "" || strpos($haystack, $needle) === 0;
        }
        return $needle === "" || stripos($haystack, $needle) === 0;
    }

    /**
     * $haystack starts with any of the string in the needles array.
     * @param $haystack
     * @param array $needles
     * @param bool $case_sensitive
     * @return bool
     */
    public static function starts_with_any($haystack, $needles, $case_sensitive = false){
        return self::any( array(self, 'starts_with'), $haystack, $needles, $case_sensitive );
    }

    /**
     * $haystack starts with all of the string in the needles array.
     * eg. starts_with_all('Hello', array('He', 'Hell')) => return true
     * @param $haystack
     * @param array $needles
     * @param bool $case_sensitive
     * @return bool
     */
    public static function starts_with_all($haystack, $needles, $case_sensitive = false){
        return self::all( array(self, 'starts_with'), $haystack, $needles, $case_sensitive );
    }
    //endregion

    //region [ Ends With ]
    /**
     * @param $haystack - the big string
     * @param $needle - the string you want to find
     * @param bool $case_sensitive
     * @return bool
     */
    public static function ends_with($haystack, $needle, $case_sensitive = false)
    {
        $expected_position = strlen($haystack) - strlen($needle);

        if ($case_sensitive)
        {
            return strrpos($haystack, $needle, 0) === $expected_position;
        }
        return strripos($haystack, $needle, 0) === $expected_position;
    }

    /**
     * @param $haystack
     * @param array $needles
     * @param bool $case_sensitive
     * @return bool
     */
    public static function ends_with_any($haystack, $needles, $case_sensitive = false)
    {
        return self::any( array(self, 'ends_with'), $haystack, $needles, $case_sensitive );
    }

    /**
     * @param $haystack
     * @param array $needles
     * @param bool $case_sensitive
     * @return bool
     */
    public static function ends_with_all($haystack, $needles, $case_sensitive = false)
    {
        return self::all( array(self, 'ends_with'), $haystack, $needles, $case_sensitive );
    }
    //endregion

    //region [ Contains ]
    /**
     * @param $haystack
     * @param $needle
     * @param bool $case_sensitive
     * @return bool
     */
    public static function contains ($haystack, $needle, $case_sensitive = false){

        if ($case_sensitive){
            return strpos($haystack, $needle) !== false;
        }
        return stripos($haystack, $needle) !== false;
    }

    /**
     * @param $haystack
     * @param array $needles
     * @param bool $case_sensitive
     * @return bool
     */
    public static function contains_any($haystack, $needles, $case_sensitive = false){
        return self::any( array(self, 'contains'), $haystack, $needles, $case_sensitive );
    }

    /**
     * @param $haystack
     * @param array $needles
     * @param bool $case_sensitive
     * @return bool
     */
    public static function contains_all($haystack, $needles, $case_sensitive = false){
        return self::all( array(self, 'contains'), $haystack, $needles, $case_sensitive );
    }
    //endregion

    //region [ Get Match ]
    /**
     * Regex extraction
     *
     * @param string $regex
     * @param string $text
     * @param int $match_index -1 to return the matching array, else return value at $match_index
     * @param bool $auto_fetch - fetch data from url in $text $auto_fetch
     * @param int $auto_fetch_timeout
     * @return string
     * @throws Exception
     */
    public static function get_match($regex, $text, $match_index = 0, $auto_fetch = true, $auto_fetch_timeout = 60){
        if ($auto_fetch === true){
            throw new Exception('Not implemented');
        }

        if (preg_match($regex, $text, $match)){
            if ($match_index == -1){
                return $match;
            }

            if ($match_index < count($match)){
                return $match[$match_index];
            }
            else {
                return false;
            }
        }
        return false;
    }
    //endregion

    //region [ Generic Functions ]
    /**
     * Return true if any is true.
     * @param callable $callback
     * @param $string
     * @param array $string_array
     * @param bool $case_sensitive
     * @return bool
     */
    private static function any($callback, $string, $string_array, $case_sensitive = false)
    {
        if (!is_array($string_array)){
            $string_array = array($string_array);
        }

        foreach($string_array as $item){
            if (call_user_func($callback, $string, $item, $case_sensitive))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Return true if all is true.
     * @param callable $callback
     * @param $string
     * @param array $string_array
     * @param bool $case_sensitive
     * @return bool
     */
    private static function all($callback, $string, $string_array, $case_sensitive = false){
        if (!is_array($string_array)){
            $string_array = array($string_array);
        }

        foreach($string_array as $item){
            if (!call_user_func($callback, $string, $item, $case_sensitive))
            {
                return false;
            }
        }

        return true;
    }
    //endregion
} 
<?php
/**
 * Created by PhpStorm.
 * User: ddang
 * Date: 5/21/14
 * Time: 10:09 AM
 */

class CirUtils {

    public static $date_format = 'Y-m-d';


    public static $is_ajax = false;
    private static $user = null;

    /**
     * @param $capability
     * @param bool $log_attempt
     * @param bool|\Wp_User $wp_user
     * @return bool
     */
    public static function user_can($capability, $log_attempt = false, $wp_user = false){
        if ($wp_user === false)
        {
            if (current_user_can($capability))
            {
                return true;
            }
        }
        else {
            if ($wp_user->has_cap($capability)){
                return true;
            }
        }


        $no_perm_msg = __('You do not have permission to view this page.', 'cir-pm');

        if (self::$is_ajax){
            self::return_json_msg($no_perm_msg, 'danger');
        }
        else {
            echo "<div class=\"alert alert-danger margin-top-10\">$no_perm_msg</div>";
        }
        return false;
    }


    /**
     * @param $msg - display message
     * @param string $msg_type  - success, info, warning, danger
     * @param string $btn_link
     * @param string $btn_text
     */
    public static function echo_message_box($msg, $msg_type = 'success', $btn_link = '', $btn_text = ''){
        $msg_type = strtolower($msg_type);
        echo '<div class="alert alert-' . $msg_type . ' margin-top-20">'. $msg .'</div>';
        if (!empty($btn_link)){
            echo '<div class="row"><div class="col-md-12 cir-right-align"><a class="btn btn-primary" href="' . $btn_link . ($_GET['lang'] ? "&lang={$_GET['lang']}" : '') . '">' . $btn_text . '</a></div></div>';
        }
    }

    public static function array_to_string($array){
        if (empty($array)){
            return '';
        }

        $result = array();
        foreach($array as $key => $value){
            if ($value == null || $value == ''){
                $value = "''";
            }
            else if (is_array($value)){
                $value = self::array_to_string($value);
                $value = "[ $value ]";
            }

            if (empty($key)){
                $result[] = $value;
            }
            else {
                $result[] = "$key=$value";
            }
        }

        return '[ ' . implode(', ', $result) . ' ]';
    }

    /**
     * @param $category
     * @param $action
     * @param $value
     * @param $note
     * @param string $type - Verbose, Info, Warning, Error
     */
    public static function log($category, $action, $value, $note = '', $type = 'Verbose')
    {
        if (!empty($value) && is_array($value))
        {
            $value = self::array_to_string($value);
        }

        if (!empty($note) && is_array($note))
        {
            $note = self::array_to_string($note);
        }

        cir_db_insert_audit_trail($category, $action, $value, $note, $type);
    }

    public static function get_publish_meta_box($add_new_button_label, $update_button_label, $return_output = false){
        $context = Timber::get_context();
        $context['__'] = TimberHelper::function_wrapper( '__' );
        $context['_e'] = TimberHelper::function_wrapper( '_e' );
        $context['date_format'] = self::$date_format;

        $screen = get_current_screen();
        $context['hiddens'] = array(
            'post_status'   => 'Publish',
            'action'        => 'editpost'
        );
        $context['button_text'] = $screen->action == 'add' ? $add_new_button_label : $update_button_label;

        Timber::$locations = CIR_PM_ADMIN_DIR;
        if ($return_output){
            return Timber::compile('templates/cir-pm-meta-box.twig', $context);
        }
        else {
            Timber::render('templates/cir-pm-meta-box.twig', $context);
        }
    }

    /**
     * @param $array - data
     * exit() after function is called
     */
    public static function return_json($array){
        //header('Content-Type: application/json');
        echo json_encode( $array );
        exit();
    }

    /**
     * @param $msg
     * @param string $type - success, info, warning, danger
     */
    public static function return_json_msg($msg, $type = 'success'){
        self::return_json( array( 'msgType' => $type, 'msg' => $msg ) );
    }

    public static function return_json_error($msg){
        self::return_json_msg($msg, 'danger');
    }

    public static function return_json_forward($url, $msg = '', $type = 'success')
    {
        if (empty($msg)){
            self::return_json( array('cmd' => 'forward', 'url' => $url) );
        }
        else {
            self::return_json( array('cmd' => 'forward', 'url' => $url,
                'msgType' => $type, 'msg' => $msg
            ) );
        }
    }

    public static function return_json_refresh($msg = '', $type = 'success')
    {
        if (empty($msg)){
            self::return_json( array('cmd' => 'refresh') );
        }
        else {
            self::return_json( array('cmd' => 'refresh',
                'msgType' => $type, 'msg' => $msg
            ) );
        }
    }

    public static function return_invalid_auth_json(){
        self::return_json(array(
            'cmd'           => 'auth',
            'msgType'       => 'danger',
            'msg'           => __('Invalid username or password.', 'cir-pm')
        ));
    }

    public static function auth_user($username, $password)
    {
        /** @var WP_User $user */
        $cur_user = wp_get_current_user();
        $user_login = $cur_user->data->user_login;

        /** @var WP_User $user */
        $user = get_user_by( 'login', $username );
        if ( $user && wp_check_password( $password, $user->data->user_pass, $user->ID) )
        {
            self::log('Approval', 'Authenticate', "CurrentUser={$user_login}, AuthUser=$username", 'Success', 'Info');
            return true;
        }
        else {
            self::log('Approval', 'Authenticate', "CurrentUser={$user_login}, AuthUser=$username", 'Failed', 'Warning');
            self::return_invalid_auth_json();
        }
    }
} // end class

function format_wp_date($string, $return_date = false){
    if (empty($string)){
        return '';
    }
    else if (strlen($string) == 8)
    {
        if ($return_date)
        {
            return DateTime::createFromFormat('Ymd', $string);
        }

        // wp_date=20140528
        return DateTime::createFromFormat('Ymd', $string)->format(CirUtils::$date_format);
    }

    return '';
}
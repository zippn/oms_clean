<?php
/**
 * Created by PhpStorm.
 * User: ddang
 * Date: 6/13/14
 * Time: 9:36 AM
 */

class Email {

    /**
     * @param string|array $emails - comma separated or array
     * @param $subject
     * @param $title
     * @param $body
     * @param string|array $attachments
     * @return bool
     */
    public static function send_email($emails, $subject, $title, $body, $attachments = array()){
        $output = self::get_email_content($title, $body);
        //echo $output;
        $headers = array('Content-Type: text/html; charset=UTF-8');

        return wp_mail($emails, $subject, $output, $headers, $attachments);
    }

    public static function send_order_decline_email($order_id){
        $domain = home_url('/');
        $body = self::get_title(__('Your order has been modified by the administrator, the quantity ordered has been reduced to meet the maximum limit.', 'cir-pm'));
        $body .= self::get_link("{$domain}?page_id=8&view-order=$order_id", "View Order# $order_id");

        $customer_id = get_post_meta($order_id, '_customer_user', true);
        $user = new WP_User($customer_id);

        self::send_email($user->data->user_email, __('Your Order Has Been Modified Order#', 'cir-pm') . " $order_id", __('Order Modified', 'cir-pm'), $body);
    }

    /**
     * @param $email_to_material array( email => mm, email mm }
     */
    public static function send_low_inventory_email($email_to_material)
    {
        foreach($email_to_material as $email => $material_rows){
            $context = Timber::get_context();

            $context['__'] = TimberHelper::function_wrapper( '__' );
            $context['_e'] = TimberHelper::function_wrapper( '_e' );
            $context['date_format'] = CirUtils::$date_format;

            $materials = array();
            foreach($material_rows as $m){
                $m['link'] = home_url('/') . "wp-admin/post.php?post={$m['post_id']}&action=edit";
                $materials[] = $m;
            }

            $context['materials'] = $materials;

            Timber::$locations = array( __DIR__ . '/../admin/templates/' );
            $body = Timber::compile( 'cir-pm-low-inventory-email.twig', $context );

            self::send_email($email, __('Low Inventory Notification', 'cir-pm'), __('Low Inventory Notification', 'cir-pm'), $body);
        }


    }

    //region [ Helper ]
    public static function get_title($text){
        return '<p class="article-title" align="left"><singleline label="Title">' . $text . '</singleline></p>';
    }

    public static function get_link($link, $text){
        return  "<h3><a href='$link' target='_blank'>$text</a></h3>";
    }

    /**
     * @param $title
     * @param $body
     * @return bool|string
     */
    public static function get_email_content($title, $body)
    {
        $context = Timber::get_context();

        $context['__'] = TimberHelper::function_wrapper('__');
        $context['_e'] = TimberHelper::function_wrapper('_e');
        $context['date_format'] = CirUtils::$date_format;

        $context['d'] = array(
            'title' => $title,
            'body' => $body
        );

        Timber::$locations = array(__DIR__ . '/../admin/templates/');
        $output = Timber::compile('cir-pm-email.twig', $context);
        return $output;
    }
    //endregion

} 
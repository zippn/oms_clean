# Plugins Structure
* oms-setup.php (plugins init)
    * OmsSetup Class
        * Load JigoshopMod()-> frontend/JigoshopMod.class.php
            * This contains all the Jigoshop mod for the frontend
        * If Admin
            * Load OmsAdmin() -> admin/OmsAdmin.class.php 
        * Not Admin
            * Load OmsFrontend() -> frontend/OmsFrontend.class.php

### Setup Gift Page For The Shop
1. Create a page called "Gift"
1. In the body insert `[oms-item-desc]` (make sure for both language tab)
1. Take the page id and put it in the JigoshopMod::GIFT_PAGE_ID


### Change Product Listing Year
* Change JigoshopShortcode::PRODUCT_YEAR
    * Change to the category name for the year
    

---

# Checkout Fields
* Name
* Email (Optional)
* Include my email in newsletter
* Display Name on the donor page

# Shop Shortcode
* [oms-shopping-list] - the list of item to purchase
* [oms-item-desc] - the item description

# Add To Cart
* Url http://onemorestep.ca/vi/product/school-supplies/?add-to-cart=27&_n=fd75d77f1f

# Cart Products
* jigoshop_cart::$cart_contents
* Product is the "data" key in the array

# Pay Pal Dev Sandbox
* https://developer.paypal.com/developer/accounts
* hmlinks-facilitator@gmail.com     =>      B>5k,7tW
* hmlinks-buyer@gmail.com           =>      C=H4s6!z

# Checkout
```php
add_filter( 'jigoshop_shipping_fields', 'egmont_shipping_fields' );

function(){
    return array(
        array(
            'name'          => jigoshop::jigoshop_version() < '1.10' ? 'billing-first_name' : 'billing_first_name',
            'label'         => __('First Name', 'jigoshop'),
            'placeholder'   => __('First Name', 'jigoshop'),
            'required'      => true,
            'class'         => array('form-row-first') ),
}
```

# Jigoshop Cache
* https://www.jigoshop.com/documentation/using-caching-plugins-jigoshop/
* Add these to the exception list. WP Super Cache: Important DO NOT put the slash at the end
```
    cart
    checkout
    thanks
    my-account
    my-account/change-password
    shop
    order-tracking
```

<?php
require '../includes/kint/Kint.class.php';
require '../includes/IOFunc.class.php';

$admin = IOFunc::get_file_list('../admin/templates', '@\.twig$@i');
$frontend = IOFunc::get_file_list('../frontend/templates', '@\.twig$@i');

$list = array_merge($admin, $frontend);

d($list);
$result = '';

foreach($list as $file){

    $txt = file_get_contents($file);
    preg_match_all('@\{\{\s*((_e|__).+?\))@i', $txt, $matches);

    $result .= "\n\n// " . $file . "\n// ==========================================================\n";
    foreach ($matches[1] as $m){
        $result .= $m . ";\n";
    }
}

$result = "<?php\n$result\n";
d($result);

file_put_contents(__DIR__ . '/po-result.php', $result);

<?php
/*
Plugin Name: OMS Ecommerce Module
Plugin URI: http://onemorestep.ca/
Description: One More Step
Version: 1.0
Author: onemorestep.ca
Author URI: http://onemorestep.ca/
*/

require_once('includes/kint/Kint.class.php');

// Security: disable file edit inside wordpress
define('DISALLOW_FILE_EDIT', true);


class OmsSetup {

    public $admin = null;
    public $frontend = null;
    public $jigoshop_mod = null;

    public function __construct(){
        // Order Of Execution: "plugins_loaded" goes b4 "init"
        add_action('plugins_loaded', array(&$this, 'plugins_loaded'));
        add_action('init', array(&$this, 'init_plugin'));

    }

    public function plugins_loaded()
    {
        define( 'OMS_PLUGIN_DIR',    trailingslashit( plugin_dir_path( __FILE__ ) ) );
        define( 'OMS_PLUGIN_URL',    trailingslashit( plugin_dir_url( __FILE__ ) ) );

        define( 'OMS_INCLUDES_DIR',      OMS_PLUGIN_DIR . trailingslashit( 'includes' ) );
        define( 'OMS_ADMIN_DIR',         OMS_PLUGIN_DIR.  trailingslashit( 'admin' ) );

        define( 'OMS_ADMIN_URL',         OMS_PLUGIN_URL . trailingslashit( 'admin' ) );

        Timber::$locations = array(
            OMS_PLUGIN_DIR . '/templates/',
            OMS_ADMIN_DIR . '/templates/'
        );

        $this->include_css_js();

        require_once('frontend/OmsFrontend.class.php');
        require_once('includes/IOFunc.class.php');
        $this->frontend = new OmsFrontend();
    }

    public function init_plugin()
    {
        $this->secure_wp_installation();

        // hide admin bar from front end
        add_filter('show_admin_bar', '__return_false');

        require_once('frontend/JigoshopMod.class.php');
        $this->jigoshop_mod = new JigoshopMod();

        if (is_admin()){
            require_once('admin/OmsAdmin.class.php');
            $this->admin = new OmsAdmin();
        }
    }

    private function secure_wp_installation()
    {
        // disable xml-rpc (allow blog to interface with wp)
        add_filter('xmlrpc_enabled', '__return_false');

        // remove wp version from meta tag
        remove_action('wp_head', 'wp_generator');

        // REMOVE WP EMOJI
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
        remove_action( 'admin_print_styles', 'print_emoji_styles' );
    }

    public function include_css_js(){
        $oms_url = trailingslashit(OMS_PLUGIN_URL);

        wp_enqueue_script( 'oms-bs',            $oms_url . 'js/vendor/bootstrap/js/bootstrap.min.js', array(), '1.0', true );
        wp_enqueue_script( 'oms-bs-dialog',     $oms_url . 'js/vendor/bootstrap-dialog/bootstrap-dialog.js', array('oms-bs'), '1.0', true );
        wp_enqueue_script( 'oms-bs-toggle',     $oms_url . 'js/vendor/bootstrap-toggle/js/bootstrap-toggle.js', array('oms-bs'), '1.0', true );

        wp_enqueue_script( 'oms-jq-blockui',    $oms_url . 'js/vendor/block-ui/jquery.blockUI.js', array('jquery'), '1.0', true );
        wp_enqueue_script( 'oms-jq-form',       $oms_url . 'js/vendor/jquery-form/jquery.form.js', array('jquery'), '1.0', true );
        wp_enqueue_script( 'oms-jq-fancy-box',  $oms_url . 'js/vendor/fancybox/jquery.fancybox.js', array('jquery'), '1.0', true );

        wp_enqueue_script( 'oms-uri',           $oms_url . 'js/vendor/uri/URI.js', array('jquery'), '1.0', true );
        wp_enqueue_script( 'oms-history',       $oms_url . 'js/vendor/history.js/jquery.history.js', array('jquery'), '1.0', true );

        wp_enqueue_script( 'oms-util',          $oms_url . 'js/vendor/util.min.js', array('oms-bs-dialog', 'oms-jq-blockui', 'oms-jq-form', 'oms-bs', 'jquery'), '1.0', true );

        // Don't include bs css for acf edit screen (bs conflict)
        if (empty($_GET['action']) || $_GET['action'] != 'edit' || get_post_type($_GET['post']) != 'acf') {
            wp_enqueue_style('oms-style-bs', $oms_url . 'js/vendor/bootstrap/css/bootstrap.min.css', array(), '1.0');
            wp_enqueue_style('oms-style-bs-theme', $oms_url . 'js/vendor/bootstrap/css/bootstrap-theme.min.css', array('oms-style-bs'), '1.0');
            wp_enqueue_style('oms-style-bs-dialog', $oms_url . 'js/vendor/bootstrap-dialog/bootstrap-dialog.css', array('oms-style-bs'), '1.0');
            wp_enqueue_style('oms-style-bs-toggle', $oms_url . 'js/vendor/bootstrap-toggle/css/bootstrap-toggle.min.css', array('oms-style-bs'), '1.0');
            wp_enqueue_style('oms-style-jq-fancy-box', $oms_url . 'js/vendor/fancybox/jquery.fancybox.css', array('oms-style-bs'), '1.0');
        }

        if (is_admin()){
            wp_enqueue_script( 'oms-admin-main',        $oms_url . 'js/admin-main.js', array('oms-util'), '1.0', true );
            wp_enqueue_style( 'oms-style-admin-main',   $oms_url . 'css/admin-main.css', array('oms-style-bs'), '1.0' );

            wp_localize_script( 'oms-admin-main', 'OMS', array( 'wpAjaxUrl' => admin_url( 'admin-ajax.php' )) );
        }
        else {
            wp_enqueue_script( 'oms-frontend-main',        $oms_url . 'js/frontend-main.js', array('oms-util'), '1.0', true );
            wp_enqueue_style( 'oms-style-frontend-main',   $oms_url . 'css/frontend-main.css', array('oms-style-bs'), '1.0' );

            wp_localize_script( 'oms-frontend-main', 'OMS', array( 'wpAjaxUrl' => admin_url( 'admin-ajax.php' )) );

            wp_localize_script(
                'oms-frontend-main',
                'omsLocale',
                array(
                    'addedToCart'       => __('The item you seleted has been added to the cart.', 'oms'),
                    'goToCart'       => __('Go To Cart.', 'oms')
                )
            );
        }
    }
}


$OMS = new OmsSetup();

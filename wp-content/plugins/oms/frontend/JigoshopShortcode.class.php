<?php

class JigoshopShortcode {

    const PRODUCT_YEAR = '2017';

    public function __construct(){
        add_shortcode( 'oms-shopping-list', array(&$this, 'oms_shopping_list_shortcode'));
        add_shortcode( 'oms-item-desc', array(&$this, 'oms_item_desc_shortcode'));

        add_action('wp_ajax_get_item_desc', array(&$this, 'ajax_get_item_desc'));
    }

    /**
     * Shortcode for shop item list [oms-shopping-list]
     *
     * @param $atts
     */
    public function oms_shopping_list_shortcode($atts) {
        $context = Timber::get_context();
        $context['__'] = TimberHelper::function_wrapper( '__' );
        $context['_e'] = TimberHelper::function_wrapper( '_e' );
        $context['lang'] = qtranxf_getLanguage();

        $products = array();
        $cart_items = array();
        $current_drive = get_field('current_drive_year', 13);
        $year =  isset($atts['year'])?$atts['year']:$current_drive;
        foreach (jigoshop_cart::$cart_contents as $k => $cart_item){
            $cart_items[] = $cart_item['product_id'];
        }

        $the_query = new WP_Query(array(
            'post_type' => 'product',
            'posts_per_page'=>35,
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_cat',
                    'field'    => 'name',
                    'terms'    => $year,
                )
            )
        ));

        while($the_query->have_posts()){
            $the_query->the_post();
            $the_post = $the_query->post;

            $post_data = $the_post->to_array();
            $post_meta = get_post_custom($the_post->ID);

            $post_data = array_merge($post_data, $post_meta);
            $post_data['post'] = $the_post;
            $post_data['timber_post'] = new TimberPost();

            $post_data['gift_thumbnail_url'] = get_field('thumbnail', $the_post->ID);
            $post_data['gift_stock'] = intval($post_data['stock'][0]);
            $products[] = $post_data;

//            d($post_data);
        }
        $context['items'] = $products;
        $context['cart_items'] = $cart_items;
        $context['current_drive'] = $year==$current_drive;

        // Remove all previous jigo notifications
        jigoshop::clear_messages();

        Timber::render('templates/jigoshop-item-list.twig', $context);
    }

    function get_post_by_slug($slug){
        $posts = get_posts(array(
            'name' => $slug,
            'posts_per_page' => 1,
            'post_type' => 'product',
            'post_status' => 'publish'
        ));

        if(!$posts) {
            return null;
        }

        return $posts[0];
    }

    /**
     * Shortcode for shop item list [oms-shopping-list]
     *
     * @param $atts
     */
    public function oms_item_desc_shortcode($atts) {
        $context = Timber::get_context();
        $context['__'] = TimberHelper::function_wrapper( '__' );
        $context['_e'] = TimberHelper::function_wrapper( '_e' );
        $context['lang'] = qtranxf_getLanguage();

        $product = null;

        $gift_id = $_REQUEST['gift'];

        if (!empty($gift_id)){
            $post_id = intval($gift_id);
            $cart_purchased_qty = 0;            // get the quantity already in the cart so that it could be subtracted from the max order qty
            foreach (jigoshop_cart::$cart_contents as $k => $cart_item){
                if ($post_id == intval($cart_item['product_id'])){
                    // qty selected already in the cart
                    $cart_purchased_qty = intval($cart_item['quantity']);
                }
            }

            if ($post_id == 0){
                $the_post = $this->get_post_by_slug($gift_id);
            }
            else {
                $the_post = get_post($post_id);
            }

            $post_data = $the_post->to_array();
            $post_meta = get_post_custom($the_post->ID);

            $post_data = array_merge($post_data, $post_meta);
            $post_data['post'] = $the_post;
            $post_data['timber_post'] = new TimberPost();

            $post_data['post_title'] = apply_filters('the_content', $post_data['post_title']);
            $post_data['post_content'] = apply_filters('the_content', $post_data['post_content']);

            $thumb_attrs = array(
                'title' => strip_tags($post_data['post_title'])
            );
            $post_data['gift_thumbnail'] = get_the_post_thumbnail($the_post->ID, 'full', $thumb_attrs);
            $post_data['gift_stock'] = intval($post_data['stock'][0]);
            $post_data['max_purchase_qty'] = $post_data['gift_stock'] - $cart_purchased_qty;
            $post_data['purchase_disabled'] = $post_data['max_purchase_qty'] <= 0 ? ' disabled="disabled"' : '';

            $jg_product = new jigoshop_product($post_id);
            $post_data['purchase_url'] = $jg_product->add_to_cart_url();

            $product = $post_data;

//                d($post_data);

            $context['itm'] = $product;

//            $start = TimberHelper::start_timer();
            Timber::render('templates/jigoshop-item-desc.twig', $context);
            //do_shortcode('[oms-shopping-list]');
//            echo '<h1>' . TimberHelper::stop_timer($start) . '</h1>';
        }
        else {
            $referer = $_SERVER["HTTP_REFERER"];
            if (preg_match('/gift=(\d+)/i', $referer, $match)){
                $gift_id = intval($match[1]);
                $link = get_permalink(JigoshopMod::GIFT_PAGE_ID) . '?gift=' . $gift_id;
                wp_redirect($link);
            }
            else {
                /// TODO: needs pretty message
                $msg = __('No item found', 'oms');
                echo "<div class=\"ItemDesc\"><h1>$msg</h1></div>";
            }
        }
    }

    public function ajax_get_item_desc(){
        $this->oms_item_desc_shortcode(array());
        wp_die();
    }
}
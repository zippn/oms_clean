<?php
/**
 * Created by PhpStorm.
 * User: ddang
 * Date: 6/19/2015
 * Time: 12:15 PM
 */

class JigoshopMod {

    const GIFT_PAGE_ID = 1114;
    const SCHOLARSHIP_ITEM_ID = 1112;

    public function __construct(){
        $this->billing_fields = Array();
        $this->init_billing_fields();
        $this->create_jigo_js_global_object();

        add_filter('jigoshop_billing_fields', array(&$this, 'billing_fields'));
        add_action('jigoshop_display_checkout_field', array(&$this, 'display_checkout_field'), 10, 3);
        add_action('wp', array(&$this, 'jigoshop_billing_fields_override'));

        add_action( 'jigoshop_new_order', array(&$this, 'jigoshop_new_order'), 10, 1 );
        add_action( 'order_status_pending_to_processing', array(&$this, 'jigoshop_register_successful_order'), 10, 1 );
        add_action( 'order_status_pending_to_completed', array(&$this, 'jigoshop_register_successful_order'), 10, 1 );
    }

    function create_jigo_js_global_object(){
        $gift_link = get_permalink(self::GIFT_PAGE_ID);

        // php to js language (globalize js var)
        wp_localize_script(
            'oms-frontend-main',
            'OmsJigo',
            array(
                'ajaxUrl' => admin_url('admin-ajax.php'),
                'giftUrl' => $gift_link,
                'pluginsUrl' => WP_PLUGIN_URL
            )
        );
    }

    public function billing_fields(){
        // Need this to prevent email validation (ie allow email as non-required field)
        return $this->billing_fields;
    }

    /**
     * Render non-native jigo field types (in our case is hidden and toggle field)
     *
     * @param $type
     * @param $args
     * @param $name
     */
    public function display_checkout_field($type, $args, $name){
        $context = Timber::get_context();
        $context['__'] = TimberHelper::function_wrapper( '__' );
        $context['_e'] = TimberHelper::function_wrapper( '_e' );
        $context['lang'] = qtranxf_getLanguage();

        $context['field'] = $args;

        Timber::render('templates/jigoshop-checkout-field-render.twig', $context);
    }

    public function jigoshop_billing_fields_override() {
        //// http://www.myaakriti.com/web/modify-checkout-field-jigoshop/
        if ( class_exists( 'jigoshop_checkout' ) ) {
            jigoshop_checkout::instance()->billing_fields = $this->billing_fields;
            jigoshop_checkout::instance()->shipping_fields = array();
        }
    }

    public function jigoshop_new_order ($order_id)
    {
        // Checkboxes are present if checked, absent if not.
        if (isset($_REQUEST['billing_included_in_newsletter'])) {
            update_field('field_559032bdbcc95', 'yes', $order_id);
        }

        // Checkboxes are present if checked, absent if not.
        if (isset($_REQUEST['billing_display_in_donor_list'])) {
            update_field('field_5590329fbcc94', 'yes', $order_id);
        }

        $jigo_order = new jigoshop_order($order_id);
        if ($jigo_order->payment_method == 'cheque') {
            $jigo_order->reduce_order_stock();
            $this->update_total_purchased($jigo_order);
            $this->send_admin_notification($jigo_order);
        }
    }
    public function jigoshop_register_successful_order($order_id, $status){
        $jigo_order = new jigoshop_order($order_id);
        $this->update_total_purchased($jigo_order);
        $this->send_admin_notification($jigo_order);
    }

    private function send_admin_notification($jigo_order){
        $gift_purchased = 0;
        $to = "phongl@gmail.com, arun.rajalingam@gmail.com, lerosework@gmail.com, darcyhiggins@gmail.com, fortneem@gmail.com, nguyenhuuvu@gmail.com, tamkaivo@gmail.com";
        $subject =  "New Order from ".$jigo_order->billing_first_name." ".$jigo_order->billing_last_name;
        $headers = array(
            'MIME-Version: 1.0',
            'Content-Type: text/html; charset=UTF-8',
            'From: OneMoreStep <info@onemorestep.ca>',
        );
        $body = '<table style="border:1px solid #d3d3d3">';
        $body .= "<tr><td>Order Date: </td><td>".$jigo_order->order_date."</td></tr>";
        $body .= "<tr><td>Name: </td><td>".$jigo_order->billing_first_name." ".$jigo_order->billing_last_name."</td></tr>";
        $body .= "<tr><td>Payment Method: </td><td>".$jigo_order->payment_method."</td></tr>";
        $body .= "<tr><td>Items: </td><td><table><tr><th>Item Name</th><th>Qty</th><th>Unit Cost</th><th>Total Cost</th></tr>";
        foreach ($jigo_order->items as $itm) {
            $item_total = $itm['cost'] * $itm['qty'];
            $body .= "<tr><td>".$itm['name'] ." </td><td>".$itm['qty']."</td><td>$ ".$itm['cost']."</td><td>$ ".$item_total."</td></tr>";
            $gift_purchased += $item_total;

        }
        $body .= '<tr><td colspan="3" style="font-weight:800">Order Total</td><td style="font-weight:800">$ '.$gift_purchased.'</td></tr>';
        $body .= "</table></td></tr>";
        $body .= '</table>';

        //$this->write_to_log($body);
        wp_mail($to,$subject, $body, $headers);
    }
    private function write_to_log($message,$file='G:\dev\oms_2017_wp\log.txt'){
        $myFile = $file;
        $fh = fopen($myFile, 'a') or die("can't open file");
        fwrite($fh, $message);
        fclose($fh);
    }
    /**
     * @param $jigo_order
     */
    private function update_total_purchased($jigo_order)
    {
        $education_purchased = 0;
        $gift_purchased = 0;

        foreach ($jigo_order->items as $itm) {
            $item_total = $itm['cost'] * $itm['qty'];
            if ($itm['id'] == self::SCHOLARSHIP_ITEM_ID) {
                $education_purchased += $item_total;
            } else {
                $gift_purchased += $item_total;
            }
        }

        if ($education_purchased > 0) {
            $amount = get_post_meta(HOME2011_PAGE_ID, 'education_amount_raised', true);
            update_post_meta(HOME2011_PAGE_ID, 'education_amount_raised', floatval($amount) + $education_purchased);
        }

        if ($gift_purchased > 0) {
            $amount = get_post_meta(HOME2011_PAGE_ID, 'gift_amount_raised', true);
            update_post_meta(HOME2011_PAGE_ID, 'gift_amount_raised', floatval($amount) + $gift_purchased);
        }

//        s($education_purchased);
//        s($gift_purchased);
    }

    private function init_billing_fields()
    {
        $this->billing_fields = array(
            array('name' => 'billing_first_name', 'label' => __('First Name', 'oms'), 'placeholder' => __('First Name', 'oms'), 'required' => true, 'class' => array('form-row-first')),
            array('name' => 'billing_last_name', 'label' => __('Last Name', 'oms'), 'placeholder' => __('Last Name', 'oms'), 'required' => true, 'class' => array('form-row-last')),
            array('name' => 'billing_email', 'required' => false, 'validate' => 'email', 'label' => __('Email Address', 'oms'), 'placeholder' => __('you@yourdomain.com', 'oms')),

            array('name' => 'billing_included_in_newsletter', 'type' => 'toggle', 'value' => 'yes', 'checked' => true, 'required' => false,
                'label' => __('Include my email in newsletter', 'oms'),
                'attrs' => array(
                    'data-toggle' => 'toggle',
                    'data-on' => __('Include my email in newsletter', 'oms'),
                    'data-off' => __('Don\'t include my email in newsletter', 'oms'),
                    'data-width' => '295px',
                    'data-onstyle' => 'info',
                    'data-offstyle' => 'warning'
                )
            ),
            array('name' => 'billing_display_in_donor_list', 'type' => 'toggle', 'value' => 'yes', 'checked' => true, 'required' => false,
                'label' => __('Display my name in donor list', 'oms'),
                'attrs' => array(
                    'data-toggle' => 'toggle',
                    'data-on' => __('Display my name in donor list', 'oms'),
                    'data-off' => __('Don\'t include my name in donor list', 'oms'),
                    'data-width' => '295px',
                    'data-onstyle' => 'info',
                    'data-offstyle' => 'warning'
                )
            ),

            array('name' => 'billing_company', 'type' => 'hidden', 'value' => ''),
            array('name' => 'billing_address_1', 'type' => 'hidden', 'value' => ''),
            array('name' => 'billing_address_2', 'type' => 'hidden', 'value' => ''),
            array('name' => 'billing_city', 'type' => 'hidden', 'value' => ''),
            array('name' => 'billing_state', 'type' => 'hidden', 'value' => ''),
            array('name' => 'billing_postcode', 'type' => 'hidden', 'value' => ''),
            array('name' => 'billing_phone', 'type' => 'hidden', 'value' => ''),
            array('name' => 'billing_country', 'type' => 'hidden', 'value' => 'Ca'),
        );
    }
}
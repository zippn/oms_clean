/*global jQuery, URI, Str, History, UI, BootstrapDialog, omsLocale, OMS */

// STANDALONE

(function (global, $, undefined) {
    "use strict";

    $(function(){
        var $body = $('body');

        function callWpAjax(data){
            var $resultTextBox = $('#ta-result');
            $resultTextBox.val('');

            $.ajax(OMS.wpAjaxUrl, {
                data: data
            }).done(function(result){
                $resultTextBox.val(result);
            }).fail(function(jqXHR, textStatus, errorThrown){
                BootstrapDialog.show({
                    title: 'Wordpress Ajax Error',
                    message: errorThrown
                });
            });
        }

        $body.on('click', '#btn-clear-orders', function(){
            UI.Bs.confirmYesNo('Are you sure you want to clear all the orders?',
                function(result){
                    if (result === true){
                        callWpAjax({
                            action: 'clear_orders'
                        });
                    }
                });
        });

        $body.on('click', '#btn-refill-qty', function(){
            UI.Bs.confirmYesNo('Are you sure you want to refill the quantity for all items?',
                function(result){
                    if (result === true){
                        callWpAjax({
                            action: 'refill_qty'
                        });
                    }
                });
        });
    });

}(typeof window !== 'undefined' ? window : this, jQuery));
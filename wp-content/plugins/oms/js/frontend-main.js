/*global jQuery, URI, Str, History, UI, BootstrapDialog, omsLocale, OmsJigo */

// STANDALONE

(function (global, $, undefined) {
    "use strict";

    var GIFT_CONTENT_SELECTOR = '.ItemDesc';

    $(function(){
        var $body = $('body'), baseUrl = global.location.href;

        UI.darkOverlayCSS.background = '#eee url("' + OmsJigo.pluginsUrl + '/oms/images/ajax-loader.gif") no-repeat center';
        UI.defaultBlockOpts.overlayCSS = UI.darkOverlayCSS;

        function callWpAjax(funcName, data, callBack){
            data = data || {};
            data.action = funcName;

            console.log(data);
            $.post(OmsJigo.ajaxUrl, data, function(res){
                callBack.call(data, res);
            }, 'html');
        }

        $body.on('click', '#gift-list a.list-item-details', function(e){
            e.preventDefault();

            var $link = $(this), id = $link.data('item-id'), data = {
                gift: id
            }, title = $link.attr('title') + ' | One More Step';

            History.pushState(data, title, OmsJigo.giftUrl + '?gift=' + id);
            $('html, body').animate({
                scrollTop: $("#primary").offset().top
            }, 300);
            return false;
        });

        // Bind to StateChange Event
        History.Adapter.bind(global, 'statechange', function(){
            var state = History.getState();

            if (Str.startsWith(baseUrl, OmsJigo.giftUrl)){
                //var giftId = URI(state.url).getSearch('gift');
                //callWpAjax('get_item_desc', {
                //        gift: giftId
                //    }, function(data){
                //        $(GIFT_CONTENT_SELECTOR).replaceWith($(data));
                //    });
                UI.Patterns.ajaxGet(state.url, null, GIFT_CONTENT_SELECTOR, undefined, '.right_column.float_right');
            }
            else{
                global.location.href = state.url;
            }
            //console.debug(state);
        });

        $body.on('submit', '#frm-purchase', function(){
            return false;
        });

        $body.on('click', '#add-to-cart', function(){
            var $form = $(this).parents('form'), url = $form.attr('action'), itemId = $form.data('item-id');

            UI.block(GIFT_CONTENT_SELECTOR);

            $.ajax(url, {
                method: 'POST',
                data: $form.serialize()
            }).fail(function(jqXHR, textStatus, errorThrown){
                BootstrapDialog.show({
                    title: 'Error',
                    message: errorThrown
                });
            }).done(function(data){
                var $msg, $itemOnTheList = $('#item-' + itemId),
                    $stamp = $itemOnTheList.find('.stamp');
                if (!$stamp.length){
                    $itemOnTheList.find('a').append('<div class="purchased-stamp stamp"></div>');
                }

                $msg = $(Str.format('<div class="alert alert-success" role="alert">{0}</div><a href="/cart/" class="btn btn-success btn-lg"><span class="glyphicon glyphicon-shopping-cart"></span> {1}</a>',
                    omsLocale.addedToCart, omsLocale.goToCart));
                $('#frm-purchase').replaceWith($msg);

                $('.jigoshop_cart').replaceWith($(data).find('.jigoshop_cart'));

            }).always(function(){
                UI.unblock(GIFT_CONTENT_SELECTOR);
            });
        });
    });

}(typeof window !== 'undefined' ? window : this, jQuery));
<?php
/**
 * Created by PhpStorm.
 * User: ddang
 * Date: 9/22/2014
 * Time: 12:24 PM
 */

require '../../includes/kint/Kint.class.php';
require '../../includes/IOFunc.class.php';

$list = IOFunc::get_file_list('./', '@\.twig$@i');

d($list);

$result = '';

foreach($list as $file){

    $txt = file_get_contents($file);
    preg_match_all('@\{\{\s*((_e|__).+?\))@i', $txt, $matches);

    $result .= "\n\n// " . $file . "\n// ==========================================================\n";
    foreach ($matches[1] as $m){
        $result .= $m . ";\n";
    }
}

$result = "<?php\n$result\n";
d($result);

file_put_contents(__DIR__ . '/po-result.php', $result);

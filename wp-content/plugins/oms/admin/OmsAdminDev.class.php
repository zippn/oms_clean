<?php

class OmsAdminDev{

    public function __construct()
    {
        add_action( 'admin_menu', array(&$this, 'setup_menu') );
        add_action( 'wp_ajax_clear_orders', array(&$this, 'ajax_clear_orders') );
        add_action( 'wp_ajax_refill_qty', array(&$this, 'ajax_refill_qty') );

    }

    public function setup_menu(){
        $capability = 'activate_plugins';
       /* add_menu_page(esc_attr__( 'Oms Dev', 'oms' ), esc_attr__( 'Oms Dev', 'oms' ), $capability, 'oms-admin-dev',
            array(&$this, 'admin_dev_page'), 'none', 3);*/

        add_menu_page(esc_attr__( 'Inventory Report', 'oms' ), esc_attr__( 'Inventory Report', 'oms' ), $capability, 'shop-inv-report',
            array(&$this, 'admin_inv_page'), 'none', 3);
    }

    public function admin_inv_page(){
        $args = array(
            'posts_per_page' => -1,
            'post_type' => 'product',
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_cat',
                    'field'    => 'term_id',
                    'terms'    => '36',
                ),
            )
        );
        $query = new WP_Query($args);
        ?>
        <h1>Inventory Report </h1>
        <?php
        if($query->have_posts()){
            $total_inv_amount = 0;
            $total_purchased_amount = 0;
            ?>
            <table id="admin-inv-report">
                <tr>
                    <th>Item</th>
                    <th>Unit Price</th>
                    <th>Purchased Qty</th>
                    <th>Target Qty</th>
                    <th>Total Purchased $</th>
                    <th>Total Target $</th>
                </tr>
            <?php
            while($query->have_posts()){
                $query->the_post();
                $post_fields = get_post_custom(get_the_ID());
                $item_total_amount = intval($post_fields['regular_price'][0])*intval($post_fields['target_quantity'][0]);
                $purchased_qty = intval($post_fields['target_quantity'][0]) - intval($post_fields['stock'][0]);
                $item_total_purchased = $purchased_qty*intval($post_fields['regular_price'][0]);
                if(get_the_title()!="Scholarship Fund"){
                    $total_inv_amount += $item_total_amount;
                    $total_purchased_amount += $item_total_purchased;
                }else{
                    $scholarhip_total =$item_total_amount;
                    $scholarhip_purchased_total =$item_total_purchased;
                }
                ?>
                <tr>
                    <td><?php the_title();?></td>
                    <td >$ <?php echo $post_fields['regular_price'][0];?></td>
                    <td ><?php echo $purchased_qty;?></td>
                    <td ><?php echo $post_fields['target_quantity'][0];?></td>
                    <td >$ <?php echo $item_total_purchased?></td>
                    <td >$ <?php echo $item_total_amount;?></td>
                </tr>
            <?php

            }?>
                <tr class="totals">
                    <td colspan="4">Gift Drive Totals</td>
                    <td >$ <?php echo $total_purchased_amount;?></td>
                    <td >$ <?php echo $total_inv_amount;?></td>
                </tr>
                <tr class="totals">
                    <td colspan="4">Scholarship Totals</td>
                    <td >$ <?php echo $scholarhip_purchased_total;?></td>
                    <td >$ <?php echo $scholarhip_total;?></td>
                </tr>
            </table>
        <?php
        }
        self::render_orders_report();
    }
    public function render_product_orders_report(){
        $orders = self::get_product_orders();
        ?>

        <h1>Inventory Orders Report</h1>
        <table id="admin-orders-report">
            <tr>
                <th>Item Name</th>
                <th>Item Unit Price</th>
                <th>Qty Ordered</th>
                <th>Total Ordered</th>
            </tr>
            <?php
            $all_totals = ['paypal'=> 0, 'cheque'=>0];
            foreach($orders as $jigo_order):
                $order_total = 0;
                $item_dettails = [];
                foreach ($jigo_order->items as $itm) {
                    $item_total = $itm['cost'] * $itm['qty'];
                    $order_total +=$item_total;
                    $item_dettails[] =  $itm['name']."(". $itm['qty'].")".": $".$item_total;
                }
                if($jigo_order->status != "pending")
                    $all_totals[$jigo_order->payment_method] += $order_total

                ?>
                <tr class="<?php echo $jigo_order->status=="pending"?'alert':"";?>">
                    <td class="wide"><?php echo $jigo_order->billing_first_name." ".$jigo_order->billing_last_name;?></td>
                    <td class="wide"><?php echo $jigo_order->order_date;?></td>
                    <td><?php echo $jigo_order->payment_method;?></td>
                    <td class="wide"><?php echo $jigo_order->status;?></td>
                    <td><?php echo implode(",", $item_dettails);?></td>
                    <td class="right wide">$ <?php echo $order_total;?></td>
                </tr>
            <?php endforeach;?>
        </table>
    <?php
    }
    public function get_product_orders(){
        $orders = self::get_orders();
        $items_report = [];

        foreach($orders as $jigo_order){
            $order_total = 0;
            $item_dettails = [];
            if($jigo_order->status != "pending" and $jigo_order->status != "cancel"){
                foreach ($jigo_order->items as $itm) {
                    $item_total = $itm['cost'] * $itm['qty'];
                    $order_total +=$item_total;
                }
            }
            ?>
            <tr class="<?php echo $jigo_order->status=="pending"?'alert':"";?>">
                <td class="wide"><?php echo $jigo_order->billing_first_name." ".$jigo_order->billing_last_name;?></td>
                <td class="wide"><?php echo $jigo_order->order_date;?></td>
                <td><?php echo $jigo_order->payment_method;?></td>
                <td class="wide"><?php echo $jigo_order->status;?></td>
                <td><?php echo implode(",", $item_dettails);?></td>
                <td class="right wide">$ <?php echo $order_total;?></td>
            </tr>
   <?php
        }
    }
    public function render_orders_report(){
        $orders = self::get_orders();?>
        <style>
            table td, table th{
                padding: 3px 7px;
                border:solid 1px #bababa;
            }
            table td.right{
                text-align: right;
            }
            td.wide{
                white-space: nowrap;
            }
            ul.report li{
                margin-left: 20px;
                list-style:circle;
            }
            .alert{
                background-color: #ffc68a;
            }
        </style>
        <h1>Orders Report</h1>
        <table id="admin-orders-report">
            <tr>
                <th>Name</th>
                <th>Order Date</th>
                <th>Payment Method</th>
                <th>Order Status</th>
                <th>Items</th>
                <th>Total</th>
            </tr>
            <?php
            $all_totals = ['paypal'=> 0, 'cheque'=>0];

            foreach($orders as $jigo_order):
                $order_total = 0;
                $item_dettails = [];
                foreach ($jigo_order->items as $itm) {
                    $item_total = $itm['cost'] * $itm['qty'];
                    $order_total +=$item_total;
                    $item_dettails[] =  $itm['name']."(". $itm['qty'].")".": $".$item_total;
                }
                if($jigo_order->status != "pending")
                    $all_totals[$jigo_order->payment_method] += $order_total

            ?>
                <tr class="<?php echo $jigo_order->status=="pending"?'alert':"";?>">
                    <td class="wide"><?php echo $jigo_order->billing_first_name." ".$jigo_order->billing_last_name;?></td>
                    <td class="wide"><?php echo $jigo_order->order_date;?></td>
                    <td><?php echo $jigo_order->payment_method;?></td>
                    <td class="wide"><?php echo $jigo_order->status;?></td>
                    <td><?php echo implode(",", $item_dettails);?></td>
                    <td class="right wide">$ <?php echo $order_total;?></td>
                </tr>
            <?php endforeach;?>
        </table>
        <h1>Summary</h1>

        <ul class="report">
            <li>Total Successful Order Amount: $ <?php echo array_sum($all_totals);?>
                <ul>
                    <li>(Paypal): $ <?php echo $all_totals['paypal'];?></li>
                    <li>(Cheque Amount): $ <?php echo $all_totals['cheque'];?></li>
                </ul>
            </li>
            <li>Total Recorded Order Amount: $ <?php echo intval(GIFT_DRIVE_RAISED)+intval(SCHOLARSHIP_RAISED);?>
                <ul>
                    <li>(Gift Drive): $ <?php echo GIFT_DRIVE_RAISED;?></li>
                    <li>(Scholarship): $ <?php echo SCHOLARSHIP_RAISED;?></li>
                </ul>
            </li>
        </ul>
            <?php

    }
    public function get_orders(){
        $args = array(
            'numberposts' => -1,
            'post_type' => 'shop_order',
            'post_status' => 'publish',
            'fields' => 'ids',
            'date_query' => array(
                array(
                    'year'      => 2018,# date("Y"),
                    'compare'   => '>=',
                )
            )
        );

        $results = get_posts($args);

        $orders = array();
        if ($results) {
            foreach ($results as $result) {
                $order = new jigoshop_order($result);
                $orders[] = $order;
            }
        }
        return $orders;
    }

    public function admin_dev_page(){
        $context = Timber::get_context();
        $context['__'] = TimberHelper::function_wrapper( '__' );
        $context['_e'] = TimberHelper::function_wrapper( '_e' );
        $context['lang'] = qtranxf_getLanguage();

//        $jigoshop_orders = new jigoshop_orders();
        $orders = self::get_orders();
//        d($orders);

        $customer_data = array();
        $customer_data[] = array('First Name', 'Last Name', 'Email', 'Payment', 'Total', 'Newsletter', 'Donor List', 'Notes');
        foreach ($orders as $o){
            $newsletter = get_field('field_559032bdbcc95', $o->id);
            $display = get_field('field_5590329fbcc94', $o->id);
            $customer_data[] = array($o->billing_first_name, $o->billing_last_name, $o->billing_email,
                $o->payment_method_title, $o->order_total, $newsletter, $display, $o->customer_note);
        }
        $path = str_replace('\\', '/', ABSPATH . 'orders.csv');
        IOFunc::array_to_csv($customer_data, $path);

        Timber::render('templates/oms-admin-dev.twig', $context);
    }

    public function ajax_clear_orders(){
        if (OmsAdmin::$DEV_MODE){
            $the_query = new WP_Query('post_type=shop_order&posts_per_page=-1');

            while($the_query->have_posts()) {
                $the_query->the_post();
                $the_post = $the_query->post;
                wp_delete_post($the_post->ID, true);
            }
            echo "Order Deleted: {$the_query->found_posts}";
        }
        else {
            echo 'Sorry no cigar, dev mod must be enable to run this sucker.';
        }
        exit();
    }

    public function ajax_refill_qty(){
        if (OmsAdmin::$DEV_MODE){
            $the_query = new WP_Query(array(
                'post_type' => 'product',
                'posts_per_page'=>35,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_cat',
                        'field'    => 'name',
                        'terms'    => JigoshopShortcode::PRODUCT_YEAR,
                    )
                )
            ));

            while($the_query->have_posts()){
                $the_query->the_post();
                $post_id = get_the_ID();
                $target_quantity =  intval(get_post_meta($post_id, 'target_quantity', true));
                update_post_meta($post_id, 'stock', $target_quantity);
            }

            update_post_meta(HOME2011_PAGE_ID, 'education_amount_raised', 0);
            update_post_meta(HOME2011_PAGE_ID, 'gift_amount_raised', 0);

            echo "All Done!";
        }
        else {
            echo 'Sorry no cigar, dev mod must be enable to run this sucker.';
        }
        exit();
    }
}
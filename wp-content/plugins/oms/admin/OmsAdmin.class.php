<?php
/**
 * Created by PhpStorm.
 * User: ddang
 * Date: 6/30/2015
 * Time: 9:35 AM
 */

class OmsAdmin {

    // Set this to true to turn on dev menu and dev options
    public static $DEV_MODE = true;

    public function __construct(){

        if (self::$DEV_MODE){
            require_once('OmsAdminDev.class.php');
            $this->oms_dev = new OmsAdminDev();
        }
    }
}